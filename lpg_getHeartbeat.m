function lpg_getHeartbeat(file, output)
%LPG_GETHEARTBREAT Get heartbeat mask for file
%   This function allows the user to select a region to be used
%   for computing a heartbeat. The mask for the region is saved
%   to the output filename.


% check if output already exists
if exist(output, 'file')
    error 'output file already exists';
end

% read in first frame
header = ufmf_read_header(file);
im_first = ufmf_read_frame(header, 1);

% get the roi
fig = figure;
ax = axes(fig);
imshow(im_first, 'Parent', ax);

title(ax, 'Circle heartbeat region, then double-click');
roi_h = imellipse(ax);
wait(roi_h);

mask = roi_h.createMask;
close(fig);

save(output, 'mask');

end



