function is_done = lpg_SpinSlowStopCamerasIsDone(cams)
% LPG_SPINSLOWSTOPCAMERASISDONE Check if cameras have stopped
%   This function returns one if the cameras have all stopped, and zero
%   otherwise. When the cameras have stopped, the caller must invoke
%   lpg_SpinFinishSlowStopCameras.
%
%   This function accepts a single argument 'cams', which is a structure
%   produced by the function lpg_SpinStartCameras. 

is_done = lpg_spin_common('slow_stop_capture_is_done', cams.spin_context);

end