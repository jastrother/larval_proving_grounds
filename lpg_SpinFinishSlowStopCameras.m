function cams = lpg_SpinFinishSlowStopCameras(cams)
% LPG_SPINFINISHSLOWSTOPCAMERAS Finish stopping the cameras
%   This releases any and all resources associated with the cameras.
%
%   This function accepts a single argument 'cams', which is a structure
%   produced by the function lpg_SpinStartCameras. After this function
%   has been invoked, the 'spin_context' in 'cams' is invalid. But the
%   'cfg' and 'preview_figure' fields of 'cams' are unaltered.

lpg_spin_common('finish_slow_stop_capture', cams.spin_context);
lpg_spin_common('destroy_context', cams.spin_context);

end