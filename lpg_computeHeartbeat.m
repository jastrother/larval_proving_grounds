function lpg_computeHeartbeat(ufmf_file, heartm_fname, heartb_fname)
%LPG_COMPUTEHEARTBEAT Calculate the heartbeat
%   This function traverses the passed movie in order to calculate
%   the heartbeat for the movie. It accepts the following inputs:
%
%   ufmf_file - filename of video as string
%   heartm_fname - filename of heart mask as string
%   heartb_fname - filename of output heartbeat file

cfg.quantile = 0.9; % quantile used to calculate value

header = ufmf_read_header(ufmf_file);
load(heartm_fname, 'mask');

heartbeat = zeros(2, header.nframes);

for frame_idx=1:header.nframes
    im = ufmf_read_frame(header, frame_idx);
    heartbeat(2, frame_idx) = quantile(im(mask), cfg.quantile);
end

heartbeat(1,:) = header.timestamps;

fclose(header.fid);

save(heartb_fname, 'heartbeat');

end



