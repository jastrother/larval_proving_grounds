function lpg_getLarvaTrajectory2d(mov_file, masks_file, output)
%LPG_GETLARVATRAJECTORY3D Get 3D trajectory from two camera views
%   This function reads in two ufmf files and then saves and output
%   file that contains the position of the larva in 3D space.

error 'update this code to use background movie';

cfg.show_progress = true;
cfg.prog_num_bars = 30;
cfg.thresh_size = 800;
cfg.thresh_tol = 0.01;
cfg.parallel_info.show_progress = true;
cfg.parallel_info.in_parallel = true;
cfg.parallel_info.block_size = 50000;
cfg.parallel_info.chunk_size = 50;

% the following variables are set automatically below
%cfg.thresh_skip
%cfg.median_group_skip
%cfg.median_group_size

% check if output already exists
if exist(output, 'file')
    error 'output file already exists';
end

% read in movie headers
header = ufmf_read_header(mov_file);
num_frames = header.nframes;

% calculate reasonable values for median
cfg.thresh_skip = floor(num_frames/100); % use 1% of total

% read in the masks file
load(masks_file, 'masks', 'bkg_im');

% find the image type
switch header.dataclass
    case 'uint8'
        max_value = 255;
    otherwise
        error 'unrecognized data class';
end

% Calculate the image threshold
if cfg.show_progress
    fprintf(1, 'Computing image thresholds\n');
end

thresh_values = lpg_getThreshold(cfg, header, bkg_im, max_value, num_frames, masks);

% Find larva in each frame
if cfg.show_progress
    fprintf(1, 'Calculating larva positions\n');
end

if ~cfg.parallel_info.in_parallel
    udata.header = header;
else
    udata.header = mov_file;
end
    
udata.bkg_im = bkg_im;
udata.thresh_values = thresh_values;
udata.max_value = max_value;
udata.masks = masks;
    
raw_output = lpg_doForEach(cfg.parallel_info, ...
    @(start, num) processFrames(cfg, udata, start, num), ...
    1, num_frames);

raw_output = reshape(raw_output, 3, numel(masks), num_frames);
raw_pos = raw_output(1:2,:,:);
area = raw_output(3,:,:);
time = header.timestamps;

save(output, 'bkg_im', 'raw_pos', 'time', 'area');

end

function raw_output = processFrames(cfg, udata, start_frame, num_frames)
% This function processes the passed group of frames

have_fid = false;
if ischar(udata.header)
    udata.header = ufmf_read_header(udata.header);
    have_fid = true;
end

raw_output = zeros(3, numel(udata.masks), num_frames);

for frame_idx=start_frame:start_frame+num_frames-1
    
    % read frame
    im = ufmf_read_frame(udata.header, frame_idx);

    % subtract background and shift to positive values
    im = double(im)/udata.max_value - udata.bkg_im;
    im = 0.5*(im + 1);
    
    local_idx = frame_idx-start_frame+1;
    
    for mask_idx=1:numel(udata.masks)
        im_mask = udata.masks(mask_idx).im;
        th_val = udata.thresh_values(mask_idx);
        
        [raw_output(1:2,mask_idx,local_idx), raw_output(3,mask_idx,local_idx)] = ...
            lpg_findLarvaCentroid(cfg, im, th_val, im_mask);
    end

end

if have_fid
    fclose(udata.header.fid);
end

raw_output = reshape(raw_output, 3 * numel(udata.masks), num_frames);

end



