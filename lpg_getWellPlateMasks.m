function lpg_getWellPlateMasks(file, output, num_rows, num_cols)
%LPG_GETWELLPLATEMASKS Get masks for well plate
%   This function calculates a background image for the passed
%   ufmf movie, and then prompts the user to select the corners
%   of the well plate.


% check if output already exists
if exist(output, 'file')
    error 'output file already exists';
end

% read in first frame
header = ufmf_read_header(file);
im_first = ufmf_read_frame(header, 1);
fclose(header.fid);

masks = lpg_selectCircularMaskArray(im_first, num_rows, num_cols);

save(output, 'masks');

end



