function usable_mask = lpg_findUsableTrajectoryPoints(t, x, y, snr, cfg)
% This function scans through the input trajectory in order to find
% points that have enough confidence to be used for analysis.
% This is done in two stages. In the first stage, we look for points with
% high confidence (snr >= cfg.snr_high). These points are always deemed
% usable. In the second stage, we use interpolation to predict the value
% of the points with medium confidence (snr >= cfg.snr_med and
% snr < cfg.snr_high). If the distance between the predicted and found
% values is small, then the found object is probably the larva, so these
% points are also deemed usable. Any point with snr < cfg.snr_med is
% always deemed unusable.
%
% This function accepts the following inputs:
%    t - A Nx1 array representing time
%    x - A Nx1 array representing x position
%    y - A Nx1 array representing y position
%    snr - A Nx1 array representing the SNR
%    cfg - A configuration structure with these fields:
%          snr_high - High confidence threshold
%          snr_med - Medium confidence threshold
%          allow_dist_error - Allowable distance error
%
% This function returns:
%    usable_mask - A Nx1 boolean array that is true if corresponding
%       point is usable and false otherwise.


if ~iscolumn(t) || ~isfloat(t)
    error 't has invalid dimensions or type';
end

if ~iscolumn(x) || ~isfloat(x) || nnz(size(t) - size(x)) > 0
    error 'x has invalid dimensions or type';
end

if ~iscolumn(y) || ~isfloat(y) || nnz(size(t) - size(y)) > 0
    error 'y has invalid dimensions or type';
end

if ~iscolumn(snr) || ~isfloat(snr) || nnz(size(t) - size(snr)) > 0
    error 'snr has invalid dimensions or type';
end

% calculate splines based on high-confidence regions
high_conf_mask = (snr >= cfg.snr_high);
if nnz(high_conf_mask) < 2
    usable_mask = false(size(snr));
    return;
end

x_sp = csapi(t(high_conf_mask), x(high_conf_mask));
y_sp = csapi(t(high_conf_mask), y(high_conf_mask));

% predict positions for medium confidence regions
med_conf_mask = ~high_conf_mask & (snr >= cfg.snr_med);
x_predict = fnval(x_sp, t(med_conf_mask));
y_predict = fnval(y_sp, t(med_conf_mask));

% compare predicted and found positions
x_found = x(med_conf_mask);
y_found = y(med_conf_mask);
total_dist = sqrt((x_found - x_predict).^2 + (y_found - y_predict).^2);

% find points where prediction is good match to found value
med_conf_ok = total_dist <= cfg.allow_dist_error;

% return mask of points that either have high confidence, or have
% medium confidence and prediction matches found value
usable_mask = high_conf_mask;
usable_mask(med_conf_mask) = med_conf_ok;

end