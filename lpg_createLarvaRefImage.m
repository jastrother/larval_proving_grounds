function ref_set = lpg_createLarvaRefImage(mov_fname, frame_num, output)
%LPG_CREATELARVAREFIMAGE Select larva reference image from movie
% This function accepts the following arguments:
%   mov_fname - Name of movie
%   frame_num - Frame in movie to use
%   output - Name of output file

crop_enlarge_factor = 3;

% check if output already exists
if exist(output, 'file')
    error 'output file already exists';
end

% read in movie header
header = ufmf_read_header(mov_fname);

% find the image type
switch header.dataclass
    case 'uint8'
        max_value = 255;
    otherwise
        error 'unrecognized data class';
end

% show the selected frame
fig = figure;
ax = axes(fig);

im = ufmf_read_frame(header, frame_num);
im = double(im) ./ max_value;

imshow(im, 'Parent', ax);


% retrieve the larva
title(ax, 'Select box around the larva');

while true
    rect = round(getrect(fig));
    
    if rect(3) >= 3 && rect(4) >= 3
        break;
    end
end

roi = im(rect(2):rect(2)+rect(4)-1, rect(1):rect(1)+rect(3)-1);

% select axes of the larva
imshow(roi, 'Parent', ax);
title(ax, 'Select point between eyes then at tail tip, hit Enter');

while true
    [line_x, line_y] = getline(fig);
   
    if length(line_x) == 2
        break;
    end
end

animal_len = sqrt(diff(line_x).^2 + diff(line_y).^2);

% find rotation angle
theta = atan2(diff(line_y),diff(line_x));
larva_im = imrotate(roi, theta*180/pi, 'bicubic', 'crop');

% select background
imshow(larva_im, 'Parent', ax);
title(ax, 'Select background region');

while true
    rect = round(getrect(fig));

    if rect(3) >= 3 && rect(4) >= 3
        break;
    end
end

roi = larva_im(rect(2):rect(2)+rect(4)-1, rect(1):rect(1)+rect(3)-1);
bkg_value = median(roi(:));

% select bounding box for animal
title(ax, 'Select box around animal');

while true
    rect = round(getrect(fig));

    if rect(3) >= 3 && rect(4) >= 3
        break;
    end
end

larva_im = larva_im(rect(2):rect(2)+rect(4)-1, rect(1):rect(1)+rect(3)-1);
imshow(larva_im, 'Parent', ax);

% select center of animal
title(ax, 'Select point between eyes');
while true
    [pt_x, pt_y] = getpts(ax);
    
    if length(pt_x) == 1
        break;
    end
end

center_pt = [pt_x; pt_y];

close(fig);

save(output, 'larva_im', 'bkg_value', 'center_pt');
end
