function is_done = lpg_PtGreySlowStopCamerasIsDone(cams)
% LPG_PTGREYSLOWSTOPCAMERASISDONE Check if cameras have stopped
%   This function returns one if the cameras have all stopped, and zero
%   otherwise. When the cameras have stopped, the caller must invoke
%   lpg_PtGreyFinishSlowStopCameras.
%
%   This function accepts a single argument 'cams', which is a structure
%   produced by the function lpg_PtGreyStartCameras. 

is_done = lpg_ptgrey_common('slow_stop_capture_is_done', cams.ptgrey_context);

end