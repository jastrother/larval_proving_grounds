function y_filtered = lpg_filterTimeSeries(delta_t, y, freq)
% This function filters the passed time series using a 4-th order
% butterworth filter. It uses zero-phase filtering to avoid phase
% shifts, and it pads the array to avoid edge effects. It accepts
% the following arguments:
%
%   delta_t - Time increment. Note that time increment must be constant.
%   y - Data values to filter
%   freq - Cutoff frequency for filter

if ~isscalar(delta_t)
    error 'delta_t has invalid size';
end

if ~isrow(y)
    error 'y has invalid size';
end

if ~isscalar(freq)
    error 'freq has invalid size';
end

pad_fraction = 0.1;
pad_len = ceil(pad_fraction * length(y));

% create pre-pad that is sign flipped and mirrored
y_pre_pad = y(2:pad_len+1);
y_pre_pad = y_pre_pad(end:-1:1);
y_pre_pad = 2*y(1) - y_pre_pad;

% create post-pad that is sign flipped and mirrored
y_post_pad = y(end-pad_len:end-1);
y_post_pad = y_post_pad(end:-1:1);
y_post_pad = 2*y(end) - y_post_pad;

y_padded = [y_pre_pad, y, y_post_pad];

% create filter matrices
nyquist_freq = 0.5 / delta_t;
[filt_b, filt_a] = butter(4, freq / nyquist_freq, 'low');

% perform filtration
y_filtered = filtfilt(filt_b, filt_a, y_padded);

% clip off the padding
y_filtered = y_filtered(1+pad_len:end-pad_len);

end

