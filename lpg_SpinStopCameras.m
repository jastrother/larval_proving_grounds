function cams = lpg_SpinStopCameras(cams)
% LPG_SPINSTOPCAMERAS Stop the cameras recording
%   This function stops cameras that were started by calling the function
%   lpg_SpinStartCameras().
%
%   This function accepts a single argument 'cams', which is a structure
%   produced by the function lpg_SpinStartCameras. After this function
%   has been invoked, the 'spin_context' in 'cams' is invalid. But the
%   'cfg' and 'preview_figure' fields of 'cams' are unaltered.

lpg_spin_common('stop_capture', cams.spin_context);
lpg_spin_common('destroy_context', cams.spin_context);

end