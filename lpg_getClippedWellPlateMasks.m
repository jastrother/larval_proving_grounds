function lpg_getClippedWellPlateMasks(file, output, num_rows, num_cols, num_clips)
%LPG_GETCLIPPEDWELLPLATEMASKS Get clipped masks for well plate
%   This function calculates a background image for the passed
%   ufmf movie, and then prompts the user to select the corners
%   and clip lines of the well plate.

% check if output already exists
if exist(output, 'file')
    error 'output file already exists';
end

% read in first frame
header = ufmf_read_header(file);
im_first = ufmf_read_frame(header, 1);
fclose(header.fid);

% select the circular masks
masks = lpg_selectCircularMaskArray(im_first, num_rows, num_cols);

% add clipping to the masks
fig = figure;
ax = axes('Parent', fig);

for mask_idx=1:numel(masks)
    while true
        im_masked = im_first;
        cur_mask = masks(mask_idx).im;

        for clip_idx = 1:num_clips
            while true
                im_masked(~cur_mask) = 0;
                imshow(im_masked, 'Parent', ax);
                msg_str = sprintf('Select clip line %d', clip_idx');
                title(ax, msg_str);

                [line_x, line_y] = getline(ax);
                if length(line_x) == 2
                    break;
                else
                    imshow(ones(size(cur_mask,1), size(cur_mask,2)), 'Parent', ax);
                    pause(1);
                end
            end

            cur_mask = addClipping(cur_mask, masks(mask_idx).params, line_x, line_y);
        end

        im_masked(~cur_mask) = 0;
        imshow(im_masked, 'Parent', ax);

        title(ax, 'Click Enter to accept or Esc to re-try');
        while true
            wait_btn = waitforbuttonpress;
            if wait_btn == 0
                continue;
            end

            wait_btn = uint8(get(fig, 'CurrentCharacter'));
            break;
        end

        switch wait_btn
            case 13
                % Enter
                break;
            otherwise
                continue;
        end
    end
    
    masks(mask_idx).im = cur_mask;
end

close(fig);

save(output, 'masks');

end

function cur_mask = addClipping(cur_mask, circ_params, line_x, line_y)

delta_vec = [line_x(2)-line_x(1); line_y(2)-line_y(1); 0];
delta_vec = delta_vec / norm(delta_vec);

norm_vec = cross(delta_vec, [0; 0; 1]);
b_factor = line_x(1)*norm_vec(1) + line_y(1)*norm_vec(2);

if circ_params.pos(1)*norm_vec(1) + circ_params.pos(2)*norm_vec(2) - b_factor < 0
    norm_vec = -norm_vec;
    b_factor = -b_factor;
end

[mesh_X,mesh_Y] = meshgrid(1:size(cur_mask,2), 1:size(cur_mask,1));
divide_mask = mesh_X * norm_vec(1) + mesh_Y * norm_vec(2) - b_factor;
divide_mask = divide_mask > 0;

cur_mask = cur_mask & divide_mask;

end



