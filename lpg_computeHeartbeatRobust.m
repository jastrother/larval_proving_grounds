function lpg_computeHeartbeatRobust(ufmf_file, heartm_fname, heartb_fname)
%LPG_COMPUTEHEARTBEAT Calculate the heartbeat
%   This function traverses the passed movie in order to calculate
%   the heartbeat for the movie. It accepts the following inputs:
%
%   ufmf_file - filename of video as string
%   heartm_fname - filename of heart mask as string
%   heartb_fname - filename of output heartbeat file
%
%   This function differs from lpg_computeHeartbeat in that it uses
%   a more complex, but more robust algorithm. In a first pass, the
%   derivative of every pixel in the mask is calculated, and the 
%   n-th quantile of this derivative is stored (n configurable).
%   In a second pass, edges in this value are calculated by finding
%   the maximum positive and minimum negative values, and then taking
%   all values above n% of the maximum as rising edges, and all
%   values less than n%  of the minimum as falling edges (n configurable).
%   In a third pass, a square wave is calculated by integrating the
%   rising and falling edges.

cfg.prev_window = 3; % number of frames used to calculate prev value in derivative
cfg.ignore_change = 15; % ignore changes less than some number of bit levels
cfg.thresh_fraction = 0.15; % fraction of max derivative used for edge threshold

header = ufmf_read_header(ufmf_file);
load(heartm_fname, 'mask');

% Calculate previous value
im_init = ufmf_read_frame(header, 1);
prev_array = NaN*zeros(size(im_init,1), size(im_init,2), cfg.prev_window);
prev_idx = 1;
prev_array(:,:,prev_idx) = im_init;
prev_idx = mod(prev_idx, cfg.prev_window) + 1;

% Calculate the derivative
deriv_val = zeros(1, header.nframes);

for frame_idx=2:header.nframes
    im_prev = median(prev_array, 3, 'omitnan');
    
    im = ufmf_read_frame(header, frame_idx);
    im = double(im);
    
    im_diff = im(mask) - im_prev(mask);
    above_thresh_mask = abs(im_diff) >= cfg.ignore_change;
    
    if nnz(above_thresh_mask) == 0
        deriv_val(frame_idx) = 0;
    else
        deriv_val(frame_idx) = sum(im_diff(above_thresh_mask));
    end
    
    prev_array(:,:,prev_idx) = im;
    prev_idx = mod(prev_idx, cfg.prev_window) + 1;
end

% Find edge transitions
most_pos = max(deriv_val(deriv_val > 0));
pos_thresh = most_pos * cfg.thresh_fraction;
most_neg = -max(-deriv_val(deriv_val < 0));
neg_thresh = most_neg * cfg.thresh_fraction;

deriv_thresh = zeros(1, header.nframes);
deriv_thresh(deriv_val > pos_thresh) = 1;
deriv_thresh(deriv_val < neg_thresh) = -1;

% Apply edge transitions
square_vals = zeros(1, header.nframes);

init_state = deriv_thresh(find(deriv_thresh ~= 0, 1, 'first'));
if init_state == 1
    cur_val = 0;
else
    cur_val = 1;
end

for frame_idx=1:header.nframes
    if deriv_thresh(frame_idx) > 0.5
        cur_val = 1;
    elseif deriv_thresh(frame_idx) < -0.5
        cur_val = 0;
    end
    
    square_vals(frame_idx) = cur_val;
end

% Output the heartbeat
heartbeat = zeros(2, header.nframes);
heartbeat(1,:) = header.timestamps;
heartbeat(2,:) = square_vals;

fclose(header.fid);

save(heartb_fname, 'heartbeat');

end



