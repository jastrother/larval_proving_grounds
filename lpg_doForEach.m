function output = lpg_doForEach(par_cfg, func, start_idx, num_indices)
%LPG_DOFOREACH RUN FUNCTION FOR EACH INDEX VALUE
%   This function runs the passed function for each index value between
%   start_idx and (start_idx+num_indices-1). Data is processed in parallel
%   as determined by par_cfg. Data is first broken into blocks and then
%   chunks. Each time the passed function handle is invoked, it is given
%   one chunk of data. Multiple chunks are processed in parallel. When all
%   of the chunks of one block have completed, then processing moves onto
%   the next block. Given this approach, the chunk size should be large
%   enough that the passed function has enough work to do that the overhead
%   associated with running the code in in parallel is small, but the chunk
%   size should be small enough that there are more chunks in a block than
%   there are processing threads. The block size can be made large, but
%   since results are consolidated only at the end of each block, larger
%   block sizes will consume more memory. This is functionally similar
%   to matlab's parfor, but it allow for more control and progress
%   messages.
%
%   This function accepts the following arguments:
%
%   par_cfg - Parallelization options. It must have the following fields
%       in_parallel - True to perform computations in parallel
%       block_size - Number of indices in a block
%       chunk_size - Number of indices in a chunk
%       show_progress - True to print out messages with each block
%
%   func - Function to call. It must accept two arguments, the first being
%   the first index it should process and the second being the number of
%   indices it should process. It must return a matrix of doubles. This
%   matrix must have the same number of rows for each index, and should
%   have a number of columns equal to the number of indices it processed.
%
%   start_idx - First index to process
%
%   num_indices - Number of indices to process
%

if ~par_cfg.in_parallel
    
    % similar to ceil(num_indices / par_cfg.block_size), w/o
    % roundoff error from floating-point
    num_blocks = double(idivide(uint64(num_indices + ...
        par_cfg.block_size - 1), uint64(par_cfg.block_size)));
    
    output = [];
    
    % Run each of the blocks
    for block_idx=1:num_blocks
        block_start_idx = (block_idx-1) * par_cfg.block_size + 1;
        
        indices_in_block = min([num_indices - block_start_idx + 1, ...
            par_cfg.block_size]);
        
        if par_cfg.show_progress
            fprintf(1, 'Processing frames %d to %d\n', block_start_idx, ...
                block_start_idx + indices_in_block - 1);
        end
        
        % similar to ceil(indices_in_block / par_cfg.chunk_size), w/o
        % roundoff error from floating-point
        num_chunks = double(idivide(uint64(indices_in_block + ...
            par_cfg.chunk_size - 1), uint64(par_cfg.chunk_size)));

        % Launch each of the chunks
        for launched_chunk_idx=1:num_chunks
            chunk_start_idx = (launched_chunk_idx-1) * par_cfg.chunk_size + 1;
            
            indices_in_chunk = min([indices_in_block - chunk_start_idx + 1, ...
                par_cfg.chunk_size]);
            
            local_start_idx = start_idx + block_start_idx + chunk_start_idx - 2;
            local_num_indices = indices_in_chunk;
            
            local_output = func(local_start_idx, local_num_indices);
            
            if isempty(output)
                output = zeros(size(local_output,1),num_indices);
            end
            
            output(:, local_start_idx-start_idx+1 : ...
                local_start_idx-start_idx+local_num_indices) = local_output;
        end    
    end
    
else
    % similar to ceil(num_indices / par_cfg.block_size), w/o
    % roundoff error from floating-point
    num_blocks = double(idivide(uint64(num_indices + ...
        par_cfg.block_size - 1), uint64(par_cfg.block_size)));
    
    allocate_output = true;
    
    % Run each of the blocks
    for block_idx=1:num_blocks
        block_start_idx = (block_idx-1) * par_cfg.block_size + 1;
        
        indices_in_block = min([num_indices - block_start_idx + 1, ...
            par_cfg.block_size]);
        
        if par_cfg.show_progress
            fprintf(1, 'Processing frames %d to %d\n', block_start_idx, ...
                block_start_idx + indices_in_block - 1);
        end
        
        % similar to ceil(indices_in_block / par_cfg.chunk_size), w/o
        % roundoff error from floating-point
        num_chunks = double(idivide(uint64(indices_in_block + ...
            par_cfg.chunk_size - 1), uint64(par_cfg.chunk_size)));
        
        % Launch each of the first num-worker chunks
        my_pool = gcp;
        launched_chunk_idx = 1;
        num_workers = min([my_pool.NumWorkers, num_chunks]);
        launched_index_log = zeros(num_workers, 1);
        
        for worker_idx=1:num_workers
            chunk_start_idx = (launched_chunk_idx-1) * par_cfg.chunk_size + 1;
            
            indices_in_chunk = min([indices_in_block - chunk_start_idx + 1, ...
                par_cfg.chunk_size]);
            
            local_start_idx = start_idx + block_start_idx + chunk_start_idx - 2;
            local_num_indices = indices_in_chunk;
            
            parF(launched_chunk_idx) = parfeval(func, 1, local_start_idx, local_num_indices);
            launched_index_log(launched_chunk_idx) = launched_chunk_idx;
            
            launched_chunk_idx = launched_chunk_idx + 1;
        end
            
        % Cycle through the remaining chunks
        while launched_chunk_idx <= num_chunks

            % Collect the output from finish chunk
            % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            [completed_idx,local_output] = fetchNext(parF);
            retrieved_chunk_idx = launched_index_log(completed_idx);
            
            % Calculate indices within this block
            local_start_idx = (retrieved_chunk_idx-1)*par_cfg.chunk_size + 1;
            local_last_idx = min([local_start_idx + par_cfg.chunk_size - 1, ...
                indices_in_block]);
            
            % Offset indices given block index
            block_offset = (block_idx-1)*par_cfg.block_size;
            local_start_idx = local_start_idx + block_offset;
            local_last_idx = local_last_idx + block_offset;
            
            if allocate_output
                % Allocate the output array
                output = zeros(size(local_output,1),num_indices);
                allocate_output = false;
            end
            
            output(:,local_start_idx:local_last_idx) = local_output;
            
            % Launch the next chunk
            % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            chunk_start_idx = (launched_chunk_idx-1) * par_cfg.chunk_size + 1;
            
            indices_in_chunk = min([indices_in_block - chunk_start_idx + 1, ...
                par_cfg.chunk_size]);
            
            local_start_idx = start_idx + block_start_idx + chunk_start_idx - 2;
            local_num_indices = indices_in_chunk;
            
            parF(completed_idx) = parfeval(func, 1, local_start_idx, local_num_indices);
            launched_index_log(completed_idx) = launched_chunk_idx;
            
            launched_chunk_idx = launched_chunk_idx + 1;
        end

        % Collect the remaining futures
        for worker_idx=1:num_workers
            % Collect the output from finish chunk
            % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            [completed_idx,local_output] = fetchNext(parF);
            retrieved_chunk_idx = launched_index_log(completed_idx);
            
            % Calculate indices within this block
            local_start_idx = (retrieved_chunk_idx-1)*par_cfg.chunk_size + 1;
            local_last_idx = min([local_start_idx + par_cfg.chunk_size - 1, ...
                indices_in_block]);
            
            % Offset indices given block index
            block_offset = (block_idx-1)*par_cfg.block_size;
            local_start_idx = local_start_idx + block_offset;
            local_last_idx = local_last_idx + block_offset;
            
            if allocate_output
                % Allocate the output array
                output = zeros(size(local_output,1),num_indices);
                allocate_output = false;
            end
            
            output(:,local_start_idx:local_last_idx) = local_output;
        end
    end
    
    if allocate_output
        output = [];
    end
end

end

