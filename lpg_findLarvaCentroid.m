function [pos, area] = lpg_findLarvaCentroid(~, im, thresh_value, im_mask)
% Calculate the position of the larva in pixel units. This function
% accepts the following arguments:
%
%   cfg - configuration parameters
%   im - image 
%   thresh_value - threshold value
%   im_mask - mask image (or empty for no mask)

use_nia = true;

% threshold image 
bw_im = ~imbinarize(im, thresh_value);

if ~isempty(im_mask)
    bw_im(~im_mask) = 0;
end

if use_nia
    [~, props] = nia_getConnectedRegions(uint8(bw_im));

    conn_area = [props(:).area];
    largest_area_idx = find(conn_area == max(conn_area), 1, 'first');
    
    pos = props(largest_area_idx).centroid;
    area = props(largest_area_idx).area;

else
    % find the connected components   
    conn_frags = bwconncomp(bw_im, 4);

    % find centroid of the component with largest area
    conn_data = regionprops('struct', conn_frags, 'Area', 'Centroid');
    
    conn_area = [conn_data(:).Area];
    largest_area_idx = find(conn_area == max(conn_area), 1, 'first');

    pos = conn_data(largest_area_idx).Centroid';
    area = conn_data(largest_area_idx).Area;
end

if area == 0
    pos = [NaN; NaN];
end

end
