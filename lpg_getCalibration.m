function lpg_getCalibration(file, output)
%LPG_GETCALIBRATION Find the calibration info for the movie
%   Find the coordinate system for passed frame

% check if output already exists
if exist(output, 'file')
    error 'output file already exists';
end

% read in movie headers
header = ufmf_read_header(file);

% find the image type
switch header.dataclass
    case 'uint8'
        max_value = 255;
    otherwise
        error 'unrecognized data class';
end

im = ufmf_read_frame(header, 1);
im = double(im) / max_value;

imshow(im);

title('Select Bottom Left Corner of Tank');
[bl_corner_x,bl_corner_y] = ginput(1);

title('Select Bottom Right Corner of Tank');
[br_corner_x, br_corner_y] = ginput(1);

title('Select First Point on Ruler (along axis)');
[sb1_point_x, sb1_point_y] = ginput(1);

title('Select Second Point on Ruler (along axis)');
[sb2_point_x, sb2_point_y] = ginput(1);

origin = [bl_corner_x; bl_corner_y]; %#ok<NASGU>

axis1_vec = [br_corner_x - bl_corner_x; br_corner_y - bl_corner_y];
axis1_vec = axis1_vec / norm(axis1_vec);

axis2_vec = [0, -1; 1, 0] * axis1_vec; %#ok<NASGU> % rotate 90 deg

scale_vec = [sb2_point_x - sb1_point_x; sb2_point_y - sb1_point_y];
scale_length = norm(scale_vec);

calib_length = input('What is the length of the scale bar in millimeters? ');

length_factor = calib_length / scale_length; %#ok<NASGU>

save(output, 'origin', 'axis1_vec', 'axis2_vec', 'length_factor');

% close movie
fclose(header.fid);

end
