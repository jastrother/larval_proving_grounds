function fig = lpg_createDisplay(cfg)
%LPG_CREATEDISPLAY Create figure that displays array of image projections
%   This function creates a new figure window which displays 'num_images'
%   images side by side, a textedit underneath that displays status
%   information, and a "Terminate" button underneath the textedit.
%
%   The optional argument cfg that provides configuration information. The
%   following fields are recognized, and unrecognized fields are ignored.
%      num_images - Number of images in array
%      width - Initial figure width
%      height - Initial figure height
%      horiz_margin - Internal horizontal margin
%      vert_margin - Internal vertical margin
%      textedit_min_height - Minimum textedit height
%      textedit_ratio - Preferred height of textedit as ratio of total
%          available height
%      term_ctrl_width - Terminate button width
%      term_ctrl_height - Terminate button height
%
%   This function returns a handle to the figure. The figure UserData
%   property contains the following fields:
%      cfg - Configuration information
%      tedit_ctrl - Text edit control handle
%      term_ctrl - Terminate button handle
%      win_axes - Cell array of window axes
%      win_images - Cell array of window images
%      updateImageDims - Function handle to be invoked in order to 
%        resize the figure for new image sizes
%

% Check the input arguments and assign defaults
if nargin >= 1
    if ~isstruct(cfg) || numel(cfg) ~= 1
        error 'cfg has invalid type';
    end
else
    cfg = [];
end

cfg = setDefault(cfg, 'num_images', 1);
cfg = setDefault(cfg, 'fig_width', 800);
cfg = setDefault(cfg, 'fig_height', 800);
cfg = setDefault(cfg, 'horiz_margin', 10);
cfg = setDefault(cfg, 'vert_margin', 10);
cfg = setDefault(cfg, 'textedit_min_height', 100);
cfg = setDefault(cfg, 'textedit_ratio', 0.2);
cfg = setDefault(cfg, 'term_ctrl_width', 100);
cfg = setDefault(cfg, 'term_ctrl_height', 50);

% Center preview window on screen
screen_pos = get(groot, 'ScreenSize');

fig_position = [
    (screen_pos(3) - cfg.fig_width)/2, ...
    (screen_pos(4) - cfg.fig_height)/2, ...
    cfg.fig_width, cfg.fig_height];

% Create the preview figure window
fig = figure(...
    'Position', fig_position, ...
    'SizeChangedFcn', @previewSizeChangedCB, ...
    'MenuBar', 'none', ...
    'Visible', 'off');

% Note: Initial positions for controls are temporary, overwritten
% by the SizeChanged Callback

% Create the textedit
tedit_ctrl = uicontrol(...
    'Parent', fig, ...
    'Style', 'edit', ...
    'HorizontalAlignment', 'left', ...
    'Units', 'Pixels', ...
    'Min', 0, ...
    'Max', 1000, ...
    'Enable', 'inactive', ...
    'Position', [0, 0, 10, 10]);

% Create the "Terminate" button
term_ctrl = uicontrol(...
    'Parent', fig, ...
    'String', 'Terminate', ...
    'Style', 'pushbutton', ...
    'Units', 'Pixels', ...
    'Callback', @(elem, evt) terminateCB(fig, elem, evt), ...
    'Position', [0, 0, 10, 10]);

% Create the image axes and image handles
win_axes = cell(cfg.num_images, 1);
win_images = cell(cfg.num_images, 1);

for win_idx=1:cfg.num_images
    % Create axes
    win_axes{win_idx} = axes(...
        'Parent', fig, ...
        'Units', 'Pixels', ...
        'Position', [0, 0, 10, 10], ...
        'Visible', 'off', ...
        'YDir', 'reverse');

    % Create image
    win_images{win_idx} = image(...
        'Parent', win_axes{win_idx},...
        'CDataMapping', 'scaled');
    
    % Set the colormap
    colormap(win_axes{win_idx}, 'gray');
    
    % Create initial image values
    win_images{win_idx}.CData = uint8(255*rand(10,10));
end

% Push user data to figure
fig_udata.cfg = cfg;
fig_udata.tedit_ctrl = tedit_ctrl;
fig_udata.term_ctrl = term_ctrl;
fig_udata.win_axes = win_axes;
fig_udata.win_images = win_images;
fig_udata.updateImageDims = @() updateFigure(fig);
fig_udata.term_activated = false;
fig.UserData = fig_udata;

% Now that everything is configured, make figure visible
fig.Visible = 'on';

end

function cfg = setDefault(cfg, name, val)
% This function checks cfg for a field 'name'.  If it exists,
% then this function returns immediately.  If it does not exist
% then this function creates it and assigns it the value in val.

if ~isfield(cfg, name)
    cfg.(name) = val;
end

end

function previewSizeChangedCB(~, ~)
% This function is invoked when the figure is resized
    updateFigure(gcbo);
end

function terminateCB(fig, ~, ~)
% This function is invoked with then terminate button is clicked.
    fig.UserData.term_activated = true;
end

function updateFigure(fig)
% This function updates the axes positions and limits
% to accomodate the current figure size and image contents

    udata = fig.UserData;
    cfg = udata.cfg;
    
    fig_pos = fig.Position;
    
    % Calculate the terminate button position
    term_pos = round([...
        fig_pos(3) - cfg.horiz_margin - cfg.term_ctrl_width, ...
        cfg.vert_margin, ...
        cfg.term_ctrl_width, ...
        cfg.term_ctrl_height]);
    
    % Calculate textedit width and height
    tedit_width = fig_pos(3) - 2*cfg.horiz_margin;
    tedit_height_avail = fig_pos(4) - (term_pos(2) + term_pos(4)) - ...
        3*cfg.vert_margin;
    tedit_height_ratio = tedit_height_avail * cfg.textedit_ratio;
    
    tedit_width = max([1, tedit_width]);
    tedit_height = max([cfg.textedit_min_height, tedit_height_ratio]);
    
    tedit_pos = round([...
        cfg.horiz_margin, ...
        term_pos(2) + term_pos(4) + cfg.vert_margin, ...
        tedit_width, ...
        tedit_height]);
    
    % Calculate maximum window width and height
    max_win_width = (fig_pos(3) - (cfg.num_images+1) * ...
        cfg.horiz_margin)/cfg.num_images;
    max_win_height = fig_pos(4)-  (tedit_pos(2) + tedit_pos(4)) - ...
        2*cfg.vert_margin;
    
    max_win_width = max([1, max_win_width]);
    max_win_height = max([1, max_win_height]);
   
    % Calculate actual size of windows
    win_widths = zeros(cfg.num_images, 1);
    win_heights = zeros(cfg.num_images, 1);
    
    for win_idx=1:cfg.num_images
    
        win_xrange = udata.win_images{win_idx}.XData;
        win_yrange = udata.win_images{win_idx}.YData;
        win_aspect_ratio = (max(win_xrange) - min(win_xrange)) / ...
            (max(win_yrange) - min(win_yrange));
    
        if max_win_width / max_win_height < win_aspect_ratio
            % Width is constraint
            win_widths(win_idx) = max_win_width;
            win_heights(win_idx) = win_widths(win_idx) / win_aspect_ratio;
        else
            % Height is constraint
            win_heights(win_idx) = max_win_height;
            win_widths(win_idx) = win_heights(win_idx) * win_aspect_ratio;
        end
 
    end
    
    % After all the positions have been calculated, we make one 
    % more pass in order to enlarge the textedit to fill the 
    % available space left over by the windows.
    
    % Recalculate the textedit height using maximum window height
    actual_win_height = max(win_heights);
    
    tedit_height = fig_pos(4) - (term_pos(2) + term_pos(4)) - ...
        3*cfg.vert_margin - actual_win_height;
    tedit_height = max([cfg.textedit_min_height, tedit_height]);
    
    tedit_pos = round([...
        cfg.horiz_margin, ...
        term_pos(2) + term_pos(4) + cfg.vert_margin, ...
        tedit_width, ...
        tedit_height]);
    
    % Recalculate the window positions using new textedit position
    win_posns = cell(cfg.num_images, 1);
    
    for win_idx=1:cfg.num_images
        win_posns{win_idx} = round([...
            win_idx*cfg.horiz_margin + (win_idx-1)*max_win_width + ...
               (max_win_width - win_widths(win_idx))/2, ...
            tedit_pos(2) + tedit_pos(4) + cfg.vert_margin + ...
                (actual_win_height - win_heights(win_idx))/2, ...
            win_widths(win_idx), ...
            win_heights(win_idx)]);
    end
    
    % Push calculated positions to controls
    udata.term_ctrl.Position = term_pos;
    udata.tedit_ctrl.Position = tedit_pos;
    
    for win_idx=1:cfg.num_images
        % Push position to axes
        udata.win_axes{win_idx}.Position = win_posns{win_idx};
        
        % Push axes ranges to axes
        win_xrange = udata.win_images{win_idx}.XData;
        win_yrange = udata.win_images{win_idx}.YData;
        
        udata.win_axes{win_idx}.XLim = ...
            [win_xrange(1)-0.5, win_xrange(2)+0.5];
        udata.win_axes{win_idx}.YLim = ...
            [win_yrange(1)-0.5, win_yrange(2)+0.5];
    end
    
end



