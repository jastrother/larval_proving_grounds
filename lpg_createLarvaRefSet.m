function lpg_createLarvaRefSet(masks_fname, ref_img_fname, output)
%LPG_CREATELARVAREFSET Create larva image reference image set
%   This function creates a cell array of larva reference images.
%   This function uses the output of lpg_createLarvaRefImage().

enlarge_factor = 2.5;

% check if output already exists
if exist(output, 'file')
    error 'output file already exists';
end

% find the size of the large comparator image
load(masks_fname, 'masks');

for mask_idx=1:numel(masks)
    im = masks(mask_idx).im;
    
    bnds = lpg_getMaskBounds(im);  
    height = bnds(2);
    width = bnds(4);
    
    if mask_idx==1
        max_mask_width = width;
        max_mask_height = height;
    else
        max_mask_width = max(max_mask_width, width);
        max_mask_height = max(max_mask_height, height);
    end
end

large_im_size = [max_mask_height, max_mask_width];

% find size of the small comparator image
load(ref_img_fname);
small_im_size = repmat(round(enlarge_factor * max(size(larva_im))), [1,2]);


% create a reference sets
pitch_num = 1;
pitch_max = 0; %degrees
yaw_num = 30;

yaw_angles = linspace(0, 360, yaw_num+1);
yaw_angles = yaw_angles(1:end-1);
pitch_angles = linspace(0, pitch_max, pitch_num);

large_ref_set = create_specific_set(pitch_angles, yaw_angles, ...
    ref_img_fname, large_im_size);

small_ref_set = create_specific_set(pitch_angles, yaw_angles, ...
    ref_img_fname, small_im_size);

save(output, 'large_ref_set', 'small_ref_set');

end

function ref_set = create_specific_set(pitch_angles, yaw_angles, ...
    ref_image_filename, comp_im_size)

fdata = load(ref_image_filename);
ref_larva_im = fdata.larva_im;
ref_bkg_value = fdata.bkg_value;
ref_center_pt = fdata.center_pt;
clear fdata;

images = cell(length(pitch_angles), length(yaw_angles));
image_ffts = cell(length(pitch_angles), length(yaw_angles));

for yaw_idx=1:length(yaw_angles)
    for pitch_idx=1:length(pitch_angles)
        phi = yaw_angles(yaw_idx)*pi/180;
        theta = pitch_angles(pitch_idx)*pi/180;
        
        pth_mat = [cos(theta),      0,      0; ...
                            0,      1,      0; ...
                            0,      0,      1];
        
        yaw_mat = [cos(phi), -sin(phi),     0; ...
                   sin(phi),  cos(phi),     0; ...
                          0,         0,     1];
        
        hw_ratio = size(ref_larva_im,1)/size(ref_larva_im,2);
        
        offset_x = (ref_center_pt(1)-(size(ref_larva_im,2)+1)/2) ...
            /(size(ref_larva_im,2)-1);
        offset_y = (ref_center_pt(2)-(size(ref_larva_im,1)+1)/2) ...
            /(size(ref_larva_im,1)-1);
        
        in_view = imref2d(size(ref_larva_im), ...
            [-0.5, 0.5] - offset_x, ...
            ([-0.5, 0.5] - offset_y) * hw_ratio);
        
        % double reference image size so we are sure not to clip anything
        ref_image_size = 2*[size(ref_larva_im,2), size(ref_larva_im,2)];
        
        out_view = imref2d(ref_image_size, [-1, 1], [-1, 1]);
        
        tform = affine2d(yaw_mat*pth_mat);
        im = imwarp(ref_larva_im, in_view, tform, ...
            'OutputView', out_view, ...
            'Interp', 'cubic', ...
            'FillValues', NaN);
        
        im(isnan(im)) = ref_bkg_value;
        im = im - mean(im(:));
        
        images{pitch_idx,yaw_idx} = im;
        
        % increase size so that the two can be offset        
        im_padded_size = size(im) + comp_im_size(1) - 2;
        
        % increase size to the next powr of 2 for fft efficiency
        im_padded_size = 2.^nextpow2(im_padded_size);
        
        % offset to center the image
        im_offset = floor((im_padded_size - size(im))/2)+1;
        fft_center_pt = [...
            (size(im,2)+1)/2 + im_offset(2) - 1; ...
            (size(im,1)+1)/2 + im_offset(1) - 1];
        
        im_padded = zeros(im_padded_size);
        im_padded(im_offset(1):im_offset(1)+size(im,1)-1, ...
            im_offset(2):im_offset(2)+size(im,2)-1) = im(end:-1:1, end:-1:1);
        
        im_padded_fft = fft2(im_padded);
        image_ffts{pitch_idx,yaw_idx} = im_padded_fft;
    end
end

ref_set.image_size = ref_image_size;
ref_set.fft_center_pt = fft_center_pt;
ref_set.pitch_angles = pitch_angles;
ref_set.yaw_angles = yaw_angles;
ref_set.images = images;
ref_set.image_ffts = image_ffts;
ref_set.comp_im_size = comp_im_size;

end
