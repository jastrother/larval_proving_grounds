/* This file reads an array of tiff files and creates a compressed ufmf
 * file. It exists only for testing, it doesn't do appropriate error
 * checking, and is generally not robust or well-written.
 */

#include <stdlib.h>
#include <assert.h>
#include "ufmf.h"
#include "tiffio.h"

#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
typedef __int8 int8_t;
typedef __int16 int16_t;
typedef __int32 int32_t;
typedef __int64 int64_t;
typedef unsigned __int8 uint8_t;
typedef unsigned __int16 uint16_t;
typedef unsigned __int32 uint32_t;
typedef unsigned __int64 uint64_t;
#else
#include <stdint.h>
#endif

int main()
{
   TIFF* tif;
   char fname[1024];
   int idx;

   ufmf_params_t params;
   ufmf_params_initialize(&params);

   ufmf_writer_t* writer = ufmf_writer_create("compressed.ufmf", 1024, 717, &params);
   if (writer == NULL) {
      abort();
   }
   
   for (idx = 1; idx <= 1000; idx++) { 
      uint32_t im_length;
      tsize_t scan_length;

      sprintf(fname, "image_%04d.tif", idx);
      tif = TIFFOpen(fname, "r");

      TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &im_length);
      scan_length = TIFFScanlineSize(tif);

      uint8_t* im = (uint8_t*)_TIFFmalloc(im_length*scan_length*sizeof(uint8_t));

      for (size_t row = 0; row < im_length; row++) {
         TIFFReadScanline(tif, im + row*scan_length, row, 0);
      }

      assert(ufmf_writer_add(writer, im, ((float)idx) * 0.1) == 0);

      _TIFFfree(im);

      TIFFClose(tif);
   }

   assert(ufmf_writer_close(writer) == 0);
   ufmf_writer_destroy(writer);
   
   return 0;
}

