function ufmf_compare_test_data()

width = 1024;
height = 717;
num_frames = 1000;
spot_radius = 10;
diff_threshold = 10;

header = ufmf_read_header('compressed.ufmf');

for idx=1:num_frames

    fname = sprintf('image_%04d.tif', idx);
    imR = imread(fname);
    
    imT = ufmf_read_frame(header, idx, 0);
    
    T = abs(imR-imT);
    if (max(T(:)) > diff_threshold) 
        error(['Frame ', num2str(idx), ' is flawed']);
    end
end


end