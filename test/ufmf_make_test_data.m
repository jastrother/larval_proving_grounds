function make_test_data()

width = 1024;
height = 717;
num_frames = 1000;
spot_radius = 10;

im_background = uint8(rand(height, width)*255);
[X,Y] = meshgrid(0:width-1, 0:height-1);

for idx=1:num_frames
    x = (width - 1) * rand(1);
    y = (height - 1) * rand(1);
    
    x = max([spot_radius, x]);
    x = min([width - spot_radius - 1, x]);
    
    y = max([spot_radius, y]);
    y = min([width - spot_radius - 1, y]);
    
    mask = (X-x).^2 + (Y-y).^2 < spot_radius;
    im = im_background;
    im(mask) = uint8(rand(1, nnz(mask))*255);
    
    fname = sprintf('image_%04d.tif', idx);
    imwrite(im, fname, 'TIFF');
end


end