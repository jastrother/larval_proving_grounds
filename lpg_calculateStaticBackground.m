function bkg_im = lpg_calculateStaticBackground(in_filename, cfg)
% This function calculates a background image by taking the
% moving quantile of pixel values. To reduce compute costs, not every frame
% is included in the moving quantile. Instead, the frame index is 
% incremented by 'frame_skipping' between samples. Samples are collected
% into a 'window_size' arrays, and when it is full the quantile of the
% array is taken. The background image taken as the mean of the
% medians.
%
% This function accepts the following arguments:
%   in_filename - input filename
%   cfg - configuration information
%
% The argument 'cfg' has the following fields:
%   quantile - quantile to use for background (0.5 for median)
%   window_size - size of moving window
%   frame_skipping - increment in frame index
%

if nargin < 2
    cfg.window_size = 50;
    cfg.frame_skipping = 100;
    cfg.quantile = 0.9;
end

% read in movie header
header = ufmf_read_header(in_filename);
num_frames = header.nframes;

% allocate accumulation window
first_im = ufmf_read_frame(header, 1);
window_accum = NaN*zeros(size(first_im,1), size(first_im,2), cfg.window_size);
window_mean = zeros(size(first_im,1), size(first_im,2));
window_num_mean = 0;

% scan through the movie
cur_idx = 1;

for frame_idx=1:cfg.frame_skipping:num_frames
    im = ufmf_read_frame(header, frame_idx);
    
    window_accum(:,:,cur_idx) = im;
    
    if cur_idx == cfg.window_size
        window_mean = window_mean + quantile(window_accum, cfg.quantile, 3);
        window_num_mean = window_num_mean + 1;
        
        cur_idx = 1;
        window_accum = NaN * window_accum;
    end
    
    cur_idx = cur_idx + 1;
end

% check for corner case in which less than 1 window is filled
if window_num_mean == 0
    window_mean = quantile(window_accum, cfg.quantile, 3);
else
    window_mean = window_mean / window_num_mean;
end  

% close the input ufmf movie
fclose(header.fid);

bkg_im = window_mean;

end
