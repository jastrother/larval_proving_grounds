function lpg_getLarvaTrajectoryPattern(mov_fname, bkg_fname, masks_fname, ref_fname, output, cfg)
%LPG_GETLARVATRAJECTORYPATTERN Get position and orientation of larva 
%   This function reads in a UFMF file and calculates the orientation
%   of the larva using a reference image set. The reference set should
%   be generated using lpg_createLarvaRefSet()
%
%   This function takes the following arguments:
%
%   mov_fname - Filename of UFMF movie
%   bkg_fname - Filename of UFMF movie of background
%   masks_fname - Filename of mask file
%   ref_fname - Filename of reference image set
%   output - Filename for output
%   cfg (optional) - Configuration for pattern matching with these fields:
%       imref_refinement - True to used imreg to optimize match
%       show_progress - True to output progress messages
%       parallel_info - See lpg_doForEach par_cfg argument
%       redo_threshold_xc - Threshold SNR to do full frame search
%       xcorr_norm_denom - Extra factor used for normalizing XC

if nargin < 6
    cfg.imreg_refinement = false;
    cfg.show_progress = true;
    cfg.parallel_info.show_progress = true;
    cfg.parallel_info.in_parallel = true;
    cfg.parallel_info.block_size = 2000;
    cfg.parallel_info.chunk_size = 50;
    cfg.redo_threshold_xc = 0.5;
    cfg.xcorr_norm_denom = 0.2;
end
    
% check if output already exists
if exist(output, 'file')
    error 'output file already exists';
end

% read in mask information
load(masks_fname, 'masks');

% load in reference image set
load(ref_fname, 'large_ref_set', 'small_ref_set');

% read in movie header
header = ufmf_read_header(mov_fname);
num_frames = header.nframes;

bkg_header = ufmf_read_header(bkg_fname);
if bkg_header.nframes ~= num_frames
    error 'background movie does not match';
end

% find the image type
switch header.dataclass
    case 'uint8'
        max_value = 255;
    otherwise
        error 'unrecognized data class';
end

% find larva in each frame
if cfg.show_progress
    fprintf(1, 'Calculating larva positions\n');
end

if ~cfg.parallel_info.in_parallel
    udata.header = header;
    udata.bkg_header = bkg_header;
else
    udata.header = mov_fname;
    udata.bkg_header = bkg_fname;
end

udata.masks = masks;
udata.large_ref_set = large_ref_set;
udata.small_ref_set = small_ref_set;
udata.max_value = max_value;

raw_output = lpg_doForEach(cfg.parallel_info, ...
    @(start, num) processFrames(cfg, udata, start, num), ...
    1, num_frames);

raw_output = reshape(raw_output, 4, numel(masks), num_frames);
raw_pos = raw_output(1:2,:,:);

% note: cannot use squeeze here, since that will collapse the 
% second dimension when there is only one well
raw_heading = reshape(raw_output(3,:,:), [numel(masks), num_frames]);
xcorr_signal = reshape(raw_output(4,:,:), [numel(masks), num_frames]);

time = header.timestamps;

save(output, 'raw_pos', 'raw_heading', 'xcorr_signal', 'time');

fclose(header.fid);
fclose(bkg_header.fid);

end

function raw_output = processFrames(cfg, udata, start_frame, num_frames)

% load data from udata, don't copy large structures to save memory
max_value = udata.max_value;

have_fid = false;
if ischar(udata.header)
    udata.header = ufmf_read_header(udata.header);
    udata.bkg_header = ufmf_read_header(udata.bkg_header);
    have_fid = true;
end

header = udata.header;
bkg_header = udata.bkg_header;

% create image registration optimizer
[reg_optimizer, reg_metric] = imregconfig('monomodal');

% pre-allocate transformation
raw_output = zeros(4, numel(udata.masks), num_frames);

% traverse each frame
for frame_idx=start_frame:start_frame+num_frames-1
    im = ufmf_read_frame(header, frame_idx);
    im = double(im) / max_value;
    
    bkg_im = ufmf_read_frame(bkg_header, frame_idx);
    bkg_im = double(bkg_im) / max_value;
    
    for mask_idx=1:numel(udata.masks)

        mask_im = udata.masks(mask_idx).im;
        
        % find the mask bounding box
        mask_col_vals = sum(mask_im,1) > 0;
        mask_row_vals = sum(mask_im,2) > 0;
        
        mask_col_start = find(mask_col_vals,1,'first');
        mask_col_finish = find(mask_col_vals,1,'last');
        
        mask_row_start = find(mask_row_vals,1,'first');
        mask_row_finish = find(mask_row_vals,1,'last');
        
        im_masked = im - bkg_im;
        im_masked(~mask_im) = 0;
        
        local_idx = frame_idx-start_frame+1;
        
        if local_idx > 1
            % search a small subwindow
            prev_pos = squeeze(raw_output(1:2, mask_idx, local_idx-1));
            
            comp_size = udata.small_ref_set.comp_im_size;
            im_subwindow = zeros(comp_size);
            
            sub_col_start = round(prev_pos(1) - 0.5 * comp_size(2));
            sub_col_start = max(sub_col_start, mask_col_start);
            
            sub_col_finish = sub_col_start + comp_size(2) - 1;
            sub_col_finish = min(sub_col_finish, mask_col_finish);
            
            sub_row_start = round(prev_pos(2) - 0.5 * comp_size(1));
            sub_row_start = max(sub_row_start, mask_row_start);
            
            sub_row_finish = sub_row_start + comp_size(1) - 1;
            sub_row_finish = min(sub_row_finish, mask_row_finish);
            
            im_subwindow(1:sub_row_finish-sub_row_start+1, ...
                1:sub_col_finish-sub_col_start+1) = ...
                im_masked(sub_row_start:sub_row_finish, ...
                sub_col_start:sub_col_finish);
              
            [found_displ,found_rot,found_xc] = findBestMatch(cfg, udata.small_ref_set, ...
                im_subwindow, reg_optimizer, reg_metric);
            
            found_displ = found_displ + [sub_col_start-1; sub_row_start-1];
        end
        
        if local_idx == 1 || found_xc < cfg.redo_threshold_xc
            % search the entire mask
            im_subwindow = zeros(udata.large_ref_set.comp_im_size);
            
            im_subwindow(1:mask_row_finish-mask_row_start+1, ...
                1:mask_col_finish-mask_col_start+1) = ...
                im_masked(mask_row_start:mask_row_finish, ...
                mask_col_start:mask_col_finish);
            
            [found_displ,found_rot,found_xc] = findBestMatch(cfg, udata.large_ref_set, ...
                im_subwindow, reg_optimizer, reg_metric);
            
            found_displ = found_displ + [mask_col_start-1; mask_row_start-1];
        end
        
        % save results
        raw_output(1:2,mask_idx, local_idx) = found_displ;
        raw_output(3, mask_idx, local_idx) = found_rot;
        raw_output(4, mask_idx, local_idx) = found_xc;
    end
end

if have_fid
    fclose(header.fid);
    fclose(bkg_header.fid);
end

raw_output = reshape(raw_output, 4 * numel(udata.masks), num_frames);

end


function [displ,xc_val] = findDisplacement(x_fft, y_fft)
% This function finds the displacement that aligns the two ffts

% x_fft is conjugated when generated

z = real(ifft2(x_fft.*y_fft));
z = fftshift(z);

xc_val = max(z(:));
[max_row,max_col] = find(z == xc_val, 1, 'first');

displ = zeros(2,1);

displ(1) = max_col;
if max_col > 1 && max_col < size(z, 2)
    displ(1) = displ(1) + interpolate(z(max_row, max_col-1), ...
        z(max_row, max_col), z(max_row, max_col+1));
end

displ(2) = max_row;
if max_row > 1 && max_row < size(z, 1)
    displ(2) = displ(2) + interpolate(z(max_row-1, max_col), ...
        z(max_row, max_col), z(max_row+1, max_col));
end

end

function incr = interpolate(lftval, midval, rhtval)
% This function performs parabolic interpolation

   incr = 0.5 * (rhtval - lftval) / ...
     (2 * midval - lftval - rhtval);
end

function [displ, rot] = convertTform(tform, im_size)

center_pt = [(im_size(2)+1)/2; (im_size(1)+1)/2];

[x_new,y_new] = transformPointsForward(tform, center_pt(1), center_pt(2));
displ = [x_new - center_pt(1); y_new - center_pt(2)];

rot = atan2(-tform.T(1,2), tform.T(1,1));

end

function [out_displ,out_rot,out_xc] = findBestMatch(cfg, ref_set, ...
    im_subwindow, reg_optimizer, reg_metric)

ref_image_size = size(ref_set.images{1});
fft_size = size(ref_set.image_ffts{1});

% add padding to the image subwindow
% note: we do this ourselves so we can center image and avoid issues
% where found displacement is near edge of the fft
col_pre_pad_len = floor((fft_size(2) - size(im_subwindow,2))/2);
col_post_pad_len = fft_size(2) - size(im_subwindow,2) - col_pre_pad_len;

row_pre_pad_len = floor((fft_size(1) - size(im_subwindow,1))/2);
row_post_pad_len = fft_size(1) - size(im_subwindow,1) - row_pre_pad_len;

im_subwindow_pad = padarray(im_subwindow, [row_pre_pad_len, col_pre_pad_len], 0, 'pre');
im_subwindow_pad = padarray(im_subwindow_pad, [row_post_pad_len, col_post_pad_len], 0, 'post');

im_subwindow_fft = fft2(im_subwindow_pad);

ft_center_pt = ref_set.fft_center_pt;
im_center_pt = [(fft_size(2) + 1)/2; (fft_size(1) + 1)/2];

best_xc_val = -Inf;
best_displ = [];
best_ref_idx = [];

% compare the masked subwindow to
for ref_idx=1:numel(ref_set.images)
    [displ,xc_val] = findDisplacement(...
        ref_set.image_ffts{ref_idx}, ...
        im_subwindow_fft);
    
    displ(1) = displ(1) - ft_center_pt(1) + im_center_pt(1) - col_pre_pad_len;
    displ(2) = displ(2) - ft_center_pt(2) + im_center_pt(2) - row_pre_pad_len;
    
    if xc_val > best_xc_val
        best_xc_val = xc_val;
        best_displ = displ;
        best_ref_idx = ref_idx;
    end
end

if cfg.imreg_refinement
    % retrieve roi around the larva. note that the reference
    % image has coordinates of -1 -> 1, selected arbitrarily,
    % but is the same as in createLarvaRefSet
    in_view = imref2d(size(im_subwindow), ...
        ([1, size(im_subwindow,2)] - best_displ(1))/(0.5*ref_image_size(2)), ...
        ([1, size(im_subwindow,1)] - best_displ(2))/(0.5*ref_image_size(1)));
    
    out_view = imref2d(ref_image_size, ...
        [-1, 1], [-1, 1]);
    
    tform = affine2d(eye(3));
    local_im = imwarp(im_subwindow, in_view, tform, ...
        'OutputView', out_view, ...
        'Interp', 'cubic', ...
        'FillValues', 0.0);
    
    local_ref_im = ref_set.images{best_ref_idx};
    
    reg_tform = imregtform(local_ref_im, local_im, ...
        'rigid', reg_optimizer, reg_metric);
    
    [reg_displ, reg_rot] = convertTform(reg_tform, size(local_im));
    
    norm_ref = norm(local_ref_im(:));
    norm_local = norm(local_im(:));
    norm_larger = max([norm_ref, norm_local]);
    
    best_xc_val = best_xc_val / (norm_ref * norm_local + ...
        cfg.xcorr_norm_denom * norm_larger.^2) * (1 + cfg.xcorr_norm_denom);
else
    reg_displ = [0; 0];
    reg_rot = 0.0;
    
    % get local image, quicker than using imwarp
    start_col = round(best_displ(1) - 0.5 * ref_image_size(2));
    finish_col = start_col + ref_image_size(2) - 1;
    start_row = round(best_displ(2) - 0.5 * ref_image_size(1));
    finish_row = start_row + ref_image_size(1) - 1;
    
    clipped_start_col = max(start_col, 1);
    clipped_finish_col = min(finish_col, size(im_subwindow,2));
    clipped_start_row = max(start_row, 1);
    clipped_finish_row = min(finish_row, size(im_subwindow,1));
    
    offset_col = clipped_start_col - start_col;
    length_col = clipped_finish_col - clipped_start_col + 1;
    offset_row = clipped_start_row - start_row;
    length_row = clipped_finish_row - clipped_start_row + 1;
    
    local_im = zeros(ref_image_size);

    local_im(1+offset_row:offset_row+length_row, ...
        1+offset_col:offset_col+length_col) = ...
        im_subwindow(clipped_start_row:clipped_finish_row, ...
        clipped_start_col:clipped_finish_col);
    
    local_ref_im = ref_set.images{best_ref_idx};
    
    norm_ref = norm(local_ref_im(:));
    norm_local = norm(local_im(:));
    norm_larger = max([norm_ref, norm_local]);
    
    best_xc_val = best_xc_val / (norm_ref * norm_local + ...
        cfg.xcorr_norm_denom * norm_larger.^2) * (1 + cfg.xcorr_norm_denom);
end

out_displ = best_displ + reg_displ;

[~,yaw_idx] = ind2sub(size(ref_set.images), best_ref_idx);
out_rot = ref_set.yaw_angles(yaw_idx) + reg_rot * 180/pi;

out_xc = best_xc_val;
end


     