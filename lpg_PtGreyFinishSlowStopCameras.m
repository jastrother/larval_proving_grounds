function cams = lpg_PtGreyFinishSlowStopCameras(cams)
% LPG_PTGREYFINISHSLOWSTOPCAMERAS Finish stopping the cameras
%   This releases any and all resources associated with the cameras.
%
%   This function accepts a single argument 'cams', which is a structure
%   produced by the function lpg_PtGreyStartCameras. After this function
%   has been invoked, the 'ptgrey_context' in 'cams' is invalid. But the
%   'cfg' and 'preview_figure' fields of 'cams' are unaltered.

lpg_ptgrey_common('finish_slow_stop_capture', cams.ptgrey_context);
lpg_ptgrey_common('destroy_context', cams.ptgrey_context);

end