function lpg_calculateBackgroundMovie(in_filename, out_filename, cfg)
% 
% This function calculates a background movie by taking the
% moving quantile of pixel values. The moving quantile window has
% 'window_size' elements. To reduce compute costs, not every frame
% is included in the moving quantile. Instead, the frame index is 
% incremented by 'frame_skipping' between samples. As a result,
% each frame of the background movie averages over window_size
% frames spanning a period of window_size * frame_skipping frames.
%
% This function accepts the following arguments:
%   in_filename - input filename
%   out_filename - output filename
%   cfg - configuration information
%
% The argument 'cfg' has the following fields:
%   quantile - quantile to use for background (0.5 for median)
%   window_size - size of moving window
%   frame_skipping - increment in frame index
%

if nargin < 3
    cfg.quantile = 0.5;
    cfg.window_size = 51;
    cfg.frame_skipping = 100;
end

% calculate number of frames before and after current in window
window_before = floor(cfg.window_size/2);
window_after = cfg.window_size - window_before - 1;

% read ufmf header
header = ufmf_read_header(in_filename);
timestamps = ufmf_read_timestamps(header);
num_frames = header.nframes;

first_im = ufmf_read_frame(header, 1);

% if there are not enough frames to fill a single window,
% then use a single background image for all frames
last_frame_first_window = 1 + window_after*cfg.frame_skipping;
if num_frames < last_frame_first_window
    % load the images into the window accumulator
    window_len = floor((num_frames-1)/cfg.frame_skipping) + 1;
    window_accum = zeros(size(first_im,1), size(first_im,2), window_len);
    
    for window_idx=1:window_len
        frame_idx = (window_idx-1)*cfg.frame_skipping + 1;
        im = ufmf_read_frame(header, frame_idx);
        
        window_accum(:,:,window_idx) = im;
    end
    
    % calculate the background image
    bkg_im = uint8(quantile(window_accum, cfg.quantile, 3));
    
    % write background movie
    out_config.filename = out_filename;
    out_config.width = size(bkg_im,2);
    out_config.height = size(bkg_im,1);
    
    out_ctxt = lpg_ufmfw_common('create_context', out_config);
    
    for frame_idx=1:num_frames
        lpg_ufmfw_common('add_frame', out_ctxt, ...
            bkg_im, timestamps(frame_idx));
    end
    
    lpg_ufmfw_common('close_context', out_ctxt);
    
    return;
end


% pre-allocate quantile accumulator
window_accum = NaN * zeros(size(first_im,1), ...
    size(first_im,2), cfg.window_size);

out_config.filename = out_filename;
out_config.width = size(first_im,2);
out_config.height = size(first_im,1);

out_ctxt = lpg_ufmfw_common('create_context', out_config);

cur_output_frame = 1;
num_output_frames = 0;

% scan through the movie
for frame_idx=1:cfg.frame_skipping:num_frames
    im = ufmf_read_frame(header, frame_idx);
    
    % the current index is incremented with each new element,
    % wrapping around when the end of the buffer is reached.
    % The array window_accum is initialized to NaN, so unset
    % values are ignored when calculating the quantile.
    cur_idx = (frame_idx-1)/cfg.frame_skipping;
    cur_idx = mod(cur_idx, cfg.window_size)+1;
    
    window_accum(:,:,cur_idx) = im;
    
    if frame_idx < last_frame_first_window
        continue;
    end
    
    bkg_im = uint8(quantile(window_accum, cfg.quantile, 3));
    
    last_output_frame = cur_output_frame + cfg.frame_skipping - 1;
    last_output_frame = min(num_frames, last_output_frame);
    
    % for first window, dump same image for each frame
    for out_idx=cur_output_frame:last_output_frame
        lpg_ufmfw_common('add_frame', out_ctxt, ...
            bkg_im, timestamps(out_idx));
        num_output_frames = num_output_frames + 1;
    end
    
    cur_output_frame = last_output_frame + 1;
end

% write any trailing frames
for out_idx=cur_output_frame:num_frames
    lpg_ufmfw_common('add_frame', out_ctxt, ...
        bkg_im, timestamps(out_idx));
    num_output_frames = num_output_frames + 1;
end

if num_output_frames ~= num_frames
    error 'internal error: input/output num frames mismatch';
end

lpg_ufmfw_common('close_context', out_ctxt);

% close the input ufmf movie
fclose(header.fid);



