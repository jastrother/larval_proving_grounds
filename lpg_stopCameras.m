function cams = lpg_stopCameras(cams)
%% This code ends the lpg_startCameras.m code.

delete([cams.vinputs{:}]);

end