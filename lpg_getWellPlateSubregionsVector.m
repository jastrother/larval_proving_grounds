function lpg_getWellPlateSubregionsVector(mov_file, masks_file, output)
%LPG_GETWELLPLATESUBREGIONSVECTOR Get divider line for well plates
%   This function prompts the user to select a line that
%   divides each well plate in half at an angle. It accepts the following
%   inputs:
%       mov_file - A string describing the file name of the movie
%       masks_file - A string describing the fil ename of the masks
%       output - A string describing the output file name
%
%   It saves a struct array that contains the following fields for each well:
%       type - A string describing the subregion type ('linear')
%       params - A [1x3] array whos first 2 elements describe
%                the normal vector of the dividing line, and
%                the last element describes offset of the line
%                from the origin in the normal direction. The
%                points on the line then satisfy the equation
%                dot(r,n) - b = 0. 
%       im - A mask array the same size as the video image,
%            which splits the left and right.


% check if output already exists
if exist(output, 'file')
    error 'output file already exists';
end


% retrive background image
% read in first frame
header = ufmf_read_header(mov_file);
im_first = ufmf_read_frame(header, 1);
fclose(header.fid);

% find line for each mask
load(masks_file, 'masks');

subregions(size(masks,1), size(masks,2)).type = [];

fig = figure;
ax = axes(fig);

for mask_idx=1:numel(masks)
    im_masked = im_first;
    im_masked(~masks(mask_idx).im) = 0;
    imshow(im_masked, 'Parent', ax);
    title(ax, 'Select line then hit Enter, or just Enter to skip');
    
    while true
        [line_x,line_y] = getline;
    
        if isempty(line_x) || length(line_x) == 2
            break;
        end
    end
        
    if isempty(line_x)
        subregions(mask_idx).type = 'linear';
        subregions(mask_idx).params = [];
        subregions(mask_idx).im = [];
        continue;
    end

    delta_vec = [line_x(2)-line_x(1); line_y(2)-line_y(1); 0];
    delta_vec = delta_vec / norm(delta_vec);
    
    norm_vec = cross(delta_vec, [0; 0; 1]);

    b_factor = line_x(1)*norm_vec(1) + line_y(1)*norm_vec(2);
         
    title(ax, 'Select right side of the line');
    [pt_x,pt_y] = getpts(fig);
    
    if pt_x*norm_vec(1) + pt_y*norm_vec(2) - b_factor < 0
        norm_vec = -norm_vec;
        b_factor = -b_factor;
    end    
        
    [mesh_X,mesh_Y] = meshgrid(1:size(im_first,2), 1:size(im_first,1));
    divide_mask = mesh_X * norm_vec(1) + mesh_Y * norm_vec(2) - b_factor;
    divide_mask = divide_mask > 0;
    
    subregions(mask_idx).type = 'linear';
    subregions(mask_idx).params = [norm_vec(1); norm_vec(2); b_factor];
    subregions(mask_idx).im = divide_mask;
end

close(fig);

save(output, 'subregions');

end



