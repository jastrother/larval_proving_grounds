#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>

typedef __int8 int8_t;
typedef __int16 int16_t;
typedef __int32 int32_t;
typedef __int64 int64_t;

typedef unsigned __int8 uint8_t;
typedef unsigned __int16 uint16_t;
typedef unsigned __int32 uint32_t;
typedef unsigned __int64 uint64_t;

#else
#define _LARGEFILE_SOURCE
#define _FILE_OFFSET_BITS 64
#define _POSIX_C_SOURCE 199309L
#include <unistd.h>
#include <stdint.h>
#include <sys/time.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <pthread.h>
#endif

#include <stdio.h> 
#include <stdlib.h>
#include <stdarg.h>
#include <math.h>
#include <assert.h>
#include <string.h>


#include "ufmf.h"


/**
 * Define type used for thread
 */
#if defined(_WIN32) || defined(_WIN64)
typedef HANDLE ufmf_thread_t;
#elif defined(_POSIX_VERSION)
typedef pthread_t ufmf_thread_t;
#else
#error "Unsupported operating system"
#endif

/**
 * Define type used for mutex 
 */
#if defined(_WIN32) || defined(_WIN64)
typedef HANDLE ufmf_mutex_t;
#elif defined(_POSIX_VERSION)
typedef pthread_mutex_t ufmf_mutex_t;
#else
#error "Unsupported operating system"
#endif

/**
 * Define type used for semaphore
 */
#if defined(_WIN32) || defined(_WIN64)
typedef HANDLE ufmf_sem_t;
#elif defined(_POSIX_VERSION)
typedef sem_t ufmf_sem_t;
#else
#error "Unsupported operating system"
#endif



/* Define chunk identifiers used in ufmf file format */
const uint8_t UFMF_KEYFRAME_CHUNK = 0;
const uint8_t UFMF_FRAME_CHUNK = 1;
const uint8_t UFMF_INDEX_DICT_CHUNK = 2;


/* Define error logger */
#ifdef UFMF_DEBUG
#define ufmf_log_init() ufmf_log_init(1, NULL, 0, NULL);
#define ufmf_log(...) ufmf_log_implementation(0, __FILE__, __LINE__, __VA_ARGS__)
#else
#define ufmf_log_init(...) do{}while(0)
#define ufmf_log(...) do{}while(0)
#endif


/**
 * The following structure contains the data for the background model.
 */
typedef struct {
   /** Number of pixels in each image */
   size_t num_pixels;

   /** Pointer to computed background */
   uint8_t* bg_pixels;

   /** Current position in quantile window */
   size_t quantile_window_pos;

   /** Pointer to quantile window pixel buffer */
   uint8_t* quantile_window_pixels;

   /** Length of quantile window */
   size_t quantile_window_length;

   /** Quantile value used for background */
   double quantile_value;

   /** One if median window is full */
   int median_window_full;

   /** Current position in median window */
   size_t median_window_pos;

   /** Pointer to median window pixel buffer */
   uint8_t *median_window_pixels;

   /** Length of median window */
   size_t median_window_length;
} ufmf_background_t;


/**
 * Structure defining a window in the compressed format.
 */
typedef struct 
{
   /** True if window is in active set */
   int active;

   /** Starting row for window */
   uint16_t row;

   /** Starting column for window */
   uint16_t col;

   /** Width of the window */
   uint16_t width;

   /** Height of the window */
   uint16_t height;
} ufmf_window_t;


/**
 * Structure defining the data for the ufmf compressor
 */
typedef struct {
   /** Width of image */
   uint16_t width;

   /** Height of image */
   uint16_t height;

   /** Array of windows */
   ufmf_window_t* window_list;

   /** Number of populated window entries */
   size_t num_windows;

   /** Length of window list */
   size_t window_list_length;

   /** Foreground mask */
   uint8_t* im_mask0;

   /** Foreground mask */
   uint8_t* im_mask1;

   /** Array of window ids */
   size_t* window_ids;

   /** Pointer to the image data */
   uint8_t* image;

   /** Difference that triggers storage */
   int diff_threshold;

   /** Number of dilation rounds on mask */
   int dilation_rounds;

   /** Max fraction of foreground to still compress */
   double max_frac_foreground;
} ufmf_compressor_t;


/**
 * Structure storing data for a single frame
 */
typedef struct {
   /** Image to compress */
   uint8_t* image;

   /** Background image */
   uint8_t* bg_image;

   /** Compression engine */
   ufmf_compressor_t* compressor;

   /** Semaphore that is one when it is ready to fill */
   ufmf_sem_t ready_to_fill;

   /** Semaphore that is one when it is ready to compress */
   ufmf_sem_t ready_to_compress;

   /** Semaphore that is one when it is ready to write  */
   ufmf_sem_t ready_to_write;

   /** True if frame should not be output */
   int poison_pill;

   /** Truie if frame contains garbage and should be ignored */
   int is_garbage;

   /** Non-zero to output background key frame */
   int output_bg;

   /** Timestamp for frame */
   double timestamp;
} ufmf_queue_slot_t;

/**
 * Structure storing metadata for frame 
 */
typedef struct {
   /** Position within file for frame */
   uint64_t pos;
 
   /** Timestamp for the frame */
   double timestamp; 
} ufmf_frame_metadata_t;


/**
 * Structure defining the data for the ufmf writer
 */
struct _ufmf_writer_t {
   /* the following variables are constant for the lifetimes
    * of the threads and may be accessed from any thread:
    *
    * width
    * height
    * index_ptr_location
    * bg_update_period_ramp_max
    * bg_update_period_ramp_min
    * bg_update_period_ramp_a
    * bg_update_period_ramp_b
    * queue_length
    * is_closed
    */

   /* the loader thread has exclusive access to
    * the following member variables:
    *
    * last_update_time
    * bg_update_period
    * bg_model
    * next_to_enqueue
    * queue_data[ready_to_fill]
    */

   /* the compression thread has exclusive access to
    * the following member variables:
    *
    * next_to_compress
    * queue_data[ready_to_compress]
    */

   /* the output thread has exclusive access to
    * the following member variables:
    *
    * file_ptr
    * frame_index
    * frame_index_pos
    * frame_index_length
    * bg_index
    * bg_index_pos
    * bg_index_length
    * index_location
    * next_to_output
    * queue_data[ready_to_write]
    */

   /* the following member variables are protected
    * by compress_mutex:
    *
    * next_to_compress
    */

   /**
    * the following member variables are protected
    * by error_mutex:
    *
    * has_error
    */
 
   /** Image width */
   uint16_t width;

   /** Image height */
   uint16_t height;

   /** Output file pointer */
   FILE *file_ptr;

   /** Metadata for each frame in file */
   ufmf_frame_metadata_t* frame_index;

   /** Position of first unused element in frame index list */
   size_t frame_index_pos;

   /** Allocated length of the index list */
   size_t frame_index_length;

   /** Metadata for each background key frame in file */
   ufmf_frame_metadata_t* bg_index;

   /** Position of first unused element in bg index list */
   size_t bg_index_pos;

   /** Allocated length of the background index */
   size_t bg_index_length;

   /** Location of index in file */
   uint64_t index_location;

   /** Location in file of pointer to index location */
   uint64_t index_ptr_location;

   /** Index of next frame to enqueue (zero-based) */
   uint64_t next_to_enqueue;

   /** Index of next frame to compress (zero-based) */
   uint64_t next_to_compress;

   /** Index of next frame to output (zero-based) */
   uint64_t next_to_output;

   /** Last time the background was updated */
   double last_update_time;

   /** Current background update period */
   double bg_update_period;

   /** Maximum number of seconds between background keyframes */
   double bg_update_period_ramp_max;

   /** Starting value for update period during ramp */
   double bg_update_period_ramp_min;

   /** Multiply update period by this factor during ramp, stop once max is reached */
   double bg_update_period_ramp_a;

   /** Add this factor to update period during ramp, stop once max is reached */
   double bg_update_period_ramp_b;

   /** Non-zero if writer is closed */
   int is_closed;

   /** Non-zero if writer has error */
   int has_error;

   /** Mutex for has_error */
   ufmf_mutex_t error_mutex;

   /** Background Model */
   ufmf_background_t* bg_model;

   /** Mutex for compression queue */
   ufmf_mutex_t compress_mutex;

   /** Compression queue elements */
   ufmf_queue_slot_t* queue_data;

   /** Compression queue length */
   size_t queue_length;

   /** Output thread */
   ufmf_thread_t output_thread;

   /** Array of compression threads */
   ufmf_thread_t* threads_array;

   /** Number of threads */
   size_t threads_len;
};


/* function prototypes */
void ufmf_abort();
void ufmf_assert(int val);

void ufmf_log_implementation(int do_init, const char* filename, int line, const char* fmt, ...);

int ufmf_mutex_init(ufmf_mutex_t* mutex);
void ufmf_mutex_lock(ufmf_mutex_t* mutex);
void ufmf_mutex_unlock(ufmf_mutex_t* mutex);
void ufmf_mutex_destroy(ufmf_mutex_t* mutex);

int ufmf_sem_init(ufmf_sem_t* sem, int val);
void ufmf_sem_wait(ufmf_sem_t* sem);
void ufmf_sem_post(ufmf_sem_t* sem);
void ufmf_sem_destroy(ufmf_sem_t* sem);

ufmf_background_t *ufmf_background_create(size_t num_pixels,
   const ufmf_params_t* params);
void ufmf_background_destroy(ufmf_background_t * bg_model);
int ufmf_background_add_frame(ufmf_background_t* bg_model, const uint8_t* im,
   int* new_model);
uint8_t* ufmf_background_get(ufmf_background_t* bg_model);
void ufmf_background_sort(uint8_t* list, size_t length);
void ufmf_background_calc_quantile(uint8_t* pixels, size_t num_pixels,
   size_t window_skip, size_t window_length, double quantile_value,
   uint8_t * output, size_t output_skip);

ufmf_compressor_t* ufmf_compressor_create(uint16_t width,
  uint16_t height, const ufmf_params_t* params);
void ufmf_compressor_destroy(ufmf_compressor_t* comp);
int ufmf_compressor_compress(ufmf_compressor_t* comp, uint8_t* im,
  uint8_t* bg);
int ufmf_compressor_write_frame(ufmf_compressor_t* comp, FILE* fptr,
  double timestamp);
void ufmf_compressor_dilate(ufmf_compressor_t* comp,
   const uint8_t* im, uint8_t* im_out);
int ufmf_compressor_label_rows(ufmf_compressor_t* comp);
int ufmf_compressor_merge_windows(ufmf_compressor_t* comp);
int ufmf_compressor_check_and_merge(ufmf_compressor_t* comp,
  size_t id1, size_t id2);


int ufmf_writer_launch_threads(ufmf_writer_t* writer, const ufmf_params_t*);
int ufmf_writer_collect_threads(ufmf_writer_t* writer);

#if defined(_WIN32) || defined(_WIN64)
DWORD WINAPI ufmf_writer_start_output_thread(LPVOID arg);
DWORD WINAPI ufmf_writer_start_compression_thread(LPVOID arg);
#elif defined(_POSIX_VERSION)
void* ufmf_writer_start_output_thread(void* arg);
void* ufmf_writer_start_compression_thread(void* arg);
#else
#error "Unsupported operating system"
#endif

int ufmf_writer_make_header(ufmf_writer_t* writer);
int ufmf_writer_update_bg_model(ufmf_writer_t* writer, uint8_t* frame,
  double timestamp, int* bg_changed);
int ufmf_writer_add_to_queue(ufmf_writer_t* writer, uint8_t* frame,
  double timestamp, int bg_changed);
void ufmf_writer_add_poison_pill_to_queue(ufmf_writer_t* writer);
int ufmf_writer_output_frames(ufmf_writer_t* writer);
int ufmf_writer_compress_frames(ufmf_writer_t* writer);
int ufmf_writer_make_frame(ufmf_writer_t* writer, ufmf_queue_slot_t* slot);
int ufmf_writer_make_key_frame(ufmf_writer_t* writer,
  uint8_t* im, double timestamp);
int ufmf_writer_make_trailer(ufmf_writer_t* writer);
void ufmf_writer_make_index_dict(ufmf_writer_t* writer,
  ufmf_frame_metadata_t* mdata, size_t mdata_num);
int ufmf_writer_get_file_position(FILE* file_ptr, uint64_t* file_pos);
int ufmf_writer_set_file_position(FILE* file_ptr, uint64_t file_pos);
int ufmf_writer_goto_file_end(FILE* file_ptr);
int ufmf_writer_init_queue(ufmf_writer_t* writer, const ufmf_params_t* params);
void ufmf_writer_free_queue(ufmf_writer_t* writer);


/**
 * This function implements the abort function. This can be used for setting
 * breakpoints.
 */
void
ufmf_abort()
{
   abort();
}


/**
 * This function implements the assert function. This can be used for setting
 * breakpoints.
 */
void
ufmf_assert(int val)
{
   if (!val) {
      ufmf_abort();
   }
}


/**
 * This function implements the error logger.
 *
 * \param do_init If non-zero, just do initialization
 * \param filename Filename generating message
 * \param line Line number generating message
 * \param fmt Format for message
 */
void
ufmf_log_implementation(int do_init, const char* filename, int line, const char* fmt, ...) 
{
   static ufmf_mutex_t mutex;

   if (do_init) {
      ufmf_mutex_init(&mutex);
	  return;
   }

   ufmf_mutex_lock(&mutex);
   va_list ap;

   printf("%s line %d: ", filename, line);

   va_start(ap, fmt);
   vprintf(fmt, ap);
   va_end(ap);

   printf("\n");

   fflush(stdout);
   ufmf_mutex_unlock(&mutex);
}


/**
 * This function initializes the thread mutex 
 *
 * @param mutex Mutex to initialize
 * @returns Zero on success, non-zero on failure.
 */
int
ufmf_mutex_init(ufmf_mutex_t* mutex)
{
#if defined(_WIN32) || defined(_WIN64)
   *mutex = CreateMutex(NULL, FALSE, NULL);
   return (*mutex == NULL); 
#elif defined(_POSIX_VERSION)
   return pthread_mutex_init(mutex, NULL);
#else
#error "Unsupported operating system"
#endif
}


/**
 * This function locks the thread mutex 
 *
 * @param mutex Mutex to lock 
 * @returns Zero on success, non-zero on failure.
 */
void
ufmf_mutex_lock(ufmf_mutex_t* mutex)
{
#if defined(_WIN32) || defined(_WIN64)
   WaitForSingleObject(*mutex, INFINITE);
#elif defined(_POSIX_VERSION)
   int retval = pthread_mutex_lock(mutex);
   if (retval != 0) {
      ufmf_abort();
   }
#else
#error "Unsupported operating system"
#endif
}


/**
 * This function unlocks the thread mutex 
 *
 * @param mutex Mutex to unlock 
 * @returns Zero on success, non-zero on failure.
 */
void
ufmf_mutex_unlock(ufmf_mutex_t* mutex)
{
#if defined(_WIN32) || defined(_WIN64)
   ReleaseMutex(*mutex);
#elif defined(_POSIX_VERSION)
   int retval = pthread_mutex_unlock(mutex);
   if (retval != 0) {
      ufmf_abort();
   }
#else
#error "Unsupported operating system"
#endif
}


/**
 * This function destroys the thread mutex 
 *
 * @param mutex Mutex to destroy 
 * @returns Zero on success, non-zero on failure.
 */
void
ufmf_mutex_destroy(ufmf_mutex_t* mutex)
{
#if defined(_WIN32) || defined(_WIN64)
   CloseHandle(*mutex);
#elif defined(_POSIX_VERSION)
   pthread_mutex_destroy(mutex);
#else
#error "Unsupported operating system"
#endif
}


/**
 * This function initializes the thread semaphore. 
 *
 * @param sem Semaphore to initialize
 * @returns Zero on success, non-zero on failure.
 */
int
ufmf_sem_init(ufmf_sem_t* sem, int val)
{
#if defined(_WIN32) || defined(_WIN64)
   *sem = CreateSemaphore(NULL, val, INT_MAX, NULL);
   return (*sem == NULL); 
#elif defined(_POSIX_VERSION)
   return sem_init(sem, 0, val);
#else
#error "Unsupported operating system"
#endif
}


/**
 * This function waits for the thread semaphore. 
 *
 * @param sem Semaphore to wait for
 */
void
ufmf_sem_wait(ufmf_sem_t* sem)
{
#if defined(_WIN32) || defined(_WIN64)
   WaitForSingleObject(*sem, INFINITE);
#elif defined(_POSIX_VERSION)
   int retval = sem_wait(sem);
   if (retval != 0) {
      ufmf_abort();
   }
#else
#error "Unsupported operating system"
#endif
}


/**
 * This function increments the thread semaphore
 *
 * @param sem Semaphore to release
 * @returns Zero on success, non-zero on failure.
 */
void
ufmf_sem_post (ufmf_sem_t* sem)
{
#if defined(_WIN32) || defined(_WIN64)
   ReleaseSemaphore(*sem, 1, NULL);
#elif defined(_POSIX_VERSION)
   int retval = sem_post(sem);
   if (retval != 0) {
      ufmf_abort();
   }
#else
#error "Unsupported operating system"
#endif
}


/**
 * This function destroys the thread semaphore
 *
 * @param sem Semaphore to destroy 
 * @returns Zero on success, non-zero on failure.
 */
void
ufmf_sem_destroy(ufmf_sem_t* sem)
{
#if defined(_WIN32) || defined(_WIN64)
   CloseHandle(*sem);
#elif defined(_POSIX_VERSION)
   sem_destroy(sem);
#else
#error "Unsupported operating system"
#endif
}


/**
 * This function sets the fields of the passed parameters struct
 * to the default values.
 *
 * @param params Parameters struct
 */
void ufmf_params_initialize(ufmf_params_t* params)
{
   params->bg_update_period_ramp_max = 2.0;
   params->bg_update_period_ramp_min = 0.1;
   params->bg_update_period_ramp_a = 1.5;
   params->bg_update_period_ramp_b = 0.0;
   params->bg_quantile_value = 0.1;
   params->bg_updates_per_key_frame = 15;
   params->bg_median_window_length = 10;
   params->diff_threshold = 10;
   params->dilation_rounds = 2;
   params->max_frac_foreground = 0.1;
   params->num_threads = 4;
   params->queue_depth = 8;
}


/**
 * This function creates an empty background model.
 * 
 * @param num_pixels Number of pixels in background model
 * @param params Parameters to use for making background model
 * @returns Pointer to background, or NULL on error
 */
ufmf_background_t*
ufmf_background_create(
  size_t num_pixels,
  const ufmf_params_t* params)
{
   ufmf_background_t *bg_model;
   bg_model = (ufmf_background_t*)malloc(sizeof(ufmf_background_t));
   if (bg_model == NULL)
   {
      return NULL;
   }

   /* initialize the structure */
   bg_model->num_pixels = num_pixels;
   bg_model->bg_pixels = NULL;
   bg_model->quantile_window_pos = 0;
   bg_model->quantile_window_pixels = NULL;
   bg_model->quantile_window_length = params->bg_updates_per_key_frame;
   bg_model->quantile_value = params->bg_quantile_value;
   bg_model->median_window_full = 0;
   bg_model->median_window_pos = 0;
   bg_model->median_window_pixels = NULL;
   bg_model->median_window_length = params->bg_median_window_length;

   /* allocate memory for buffers */
   bg_model->bg_pixels = (uint8_t*)malloc(sizeof(uint8_t) * num_pixels);
   if (bg_model->bg_pixels == NULL) {
      free (bg_model);
      return NULL;
   }

   bg_model->quantile_window_pixels = (uint8_t*)malloc(
     sizeof(uint8_t) * num_pixels * bg_model->quantile_window_length);
   if (bg_model->quantile_window_pixels == NULL) {
      free(bg_model->bg_pixels);
      free(bg_model);
      return NULL;
   }

   bg_model->median_window_pixels = (uint8_t*)malloc(
      sizeof(uint8_t) * num_pixels * bg_model->median_window_length);
   if (bg_model->median_window_pixels == NULL) {
      free(bg_model->bg_pixels);
      free(bg_model->quantile_window_pixels);
      free(bg_model);
      return NULL;
   }

   return bg_model;
}



/**
 * This function destroys the resources associated with the background model.
 *
 * @param bg_model Background model.
 */
void
ufmf_background_destroy(ufmf_background_t* bg_model)
{
   if (bg_model->bg_pixels != NULL) {
      free(bg_model->bg_pixels);
   }

   if (bg_model->quantile_window_pixels != NULL) {
      free(bg_model->quantile_window_pixels);
   }

   if (bg_model->median_window_pixels != NULL) {
      free(bg_model->median_window_pixels);
   }

   free(bg_model);
}


/**
 * This function adds a frame to the background model.
 *
 * @param bg_model Background model
 * @param im Image to add to model
 * @param new_model Pointer that is set to one if new model was generated, zero otherwise.
 * @returns One on error, zero on success.
 */
int
ufmf_background_add_frame(ufmf_background_t* bg_model, const uint8_t* im,
  int* new_model)
{

   /* we estimate the quantile, by calculating the quantile for quantile_window_length
    * length samples, and then taking the median of a sample of median_window_length
    * computed quantile values.
    */

   {
      size_t num_pixels = bg_model->num_pixels;
      uint8_t* quantile_window_pixels = bg_model->quantile_window_pixels;
      size_t quantile_window_length = bg_model->quantile_window_length;
      size_t quantile_window_pos = bg_model->quantile_window_pos;

      /* add the image to an array that will be used to calculate the quantile */
      for (size_t i = 0; i < num_pixels; i++) {
         quantile_window_pixels[i*quantile_window_length + quantile_window_pos] = im[i];
      }

      quantile_window_pos += 1;
      bg_model->quantile_window_pos = quantile_window_pos;
   }

   /* if this is the first frame, then initialize the background */
   if (bg_model->quantile_window_pos == 1 && bg_model->median_window_pos == 0
       && !bg_model->median_window_full) {
      size_t num_pixels = bg_model->num_pixels;
      uint8_t* bg_pixels = bg_model->bg_pixels;

      for (size_t i = 0; i < num_pixels; i++) {
         bg_pixels[i] = im[i];
      }

      *new_model = 1;
      return 0;
   }

   /* if we have not filled the quantile buffer array, then we are done. otherwise,
    * we need to compute the quantile and update the median array
    */
   if (bg_model->quantile_window_pos < bg_model->quantile_window_length) {
      *new_model = 0;
      return 0;
   }

   ufmf_background_calc_quantile(bg_model->quantile_window_pixels,
      bg_model->num_pixels, bg_model->quantile_window_length,
      bg_model->quantile_window_length, bg_model->quantile_value,
      bg_model->median_window_pixels + bg_model->median_window_pos,
      bg_model->median_window_length);

   bg_model->quantile_window_pos = 0;
   bg_model->median_window_pos += 1;

   /* at the beginning, only the first m_median_window_pos slots are populated,
    * after the the window is full we can use the entire thing.
    */
   size_t cur_median_window_length = bg_model->median_window_length;
   if (!bg_model->median_window_full) {
      if (bg_model->median_window_pos == bg_model->median_window_length) {
         bg_model->median_window_full = 1;
      } else {
         cur_median_window_length = bg_model->median_window_pos;
      }
   }

   bg_model->median_window_pos = bg_model->median_window_pos
      % bg_model->median_window_length;

   ufmf_background_calc_quantile(bg_model->median_window_pixels, bg_model->num_pixels,
      bg_model->median_window_length, cur_median_window_length, 0.5,
      bg_model->bg_pixels, 1);

   *new_model = 1;
   return 0;
}


/**
 * This function retrieves the current background image
 */
uint8_t*
ufmf_background_get(ufmf_background_t* bg_model)
{
  return bg_model->bg_pixels;
}


/**
 * This function does a simple insertion sort, it should work great for small
 * arrays but would need to use a more scalable algorithm for longer arrays.
 *
 * @param list List of integers to sort
 * @param length Length of list
 */
void
ufmf_background_sort(uint8_t* list, size_t length)
{
   for (size_t i = 1; i < length; i++) {
      uint8_t x = list[i];
      size_t j = 1;

      while (j <= i) {
         size_t k = i - j;
         uint8_t y = list[k];

         if (x < y) {
            list[k + 1] = y;
            j += 1;
         } else {
            list[k + 1] = x;
            break;
         }
      }
   }
}


/**
 * This function calculates the given quantile value for the passed list of pixels.
 * 
 * @param pixels - Array of pixels where each pixel is a window_length chunk of values
 * @param num_pixels - Number of pixels in the pixel array
 * @param window_skip - Number of elements to skip to get to next pixel
 * @param window_length- Number of values in the window for each pixel
 * @param quantile_value - Quantile to calculate (0 to 1)
 * @param output - Array to write result for each pixel
 * @param output_skip - Number of elements to skip when writing output
 */
void
ufmf_background_calc_quantile(uint8_t* pixels, size_t num_pixels,
   size_t window_skip, size_t window_length, double quantile_value,
   uint8_t * output, size_t output_skip)
{
   /* calculate index of value to use for quantile */
   double quantile_float_index = ceil(quantile_value *
      (window_length - 1) - 0.5);

   size_t quantile_index;
   if (quantile_float_index < 0) {
      quantile_index = 0;
   } else if (quantile_float_index > window_length - 1) {
      quantile_index = window_length - 1;
   } else {
      quantile_index = (size_t) quantile_float_index;
   }

   for (size_t i = 0; i < num_pixels; i++) {
      uint8_t* window_pixels = pixels + i * window_skip;

      /* sort the array */
      ufmf_background_sort(window_pixels, window_length);

      /* get the value with the right index */
      output[i * output_skip] = window_pixels[quantile_index];
   }
}


/**
 * This function constructs an empty compressor
 *
 * @param width Image width
 * @param height Image height
 * @param params Parameters
 * @returns Pointer to compressor or NULL on failure.
 */
ufmf_compressor_t*
ufmf_compressor_create(uint16_t width, uint16_t height,
   const ufmf_params_t* params)
{
   ufmf_compressor_t* comp;
   comp = (ufmf_compressor_t*)malloc(sizeof(ufmf_compressor_t));
   if (comp == NULL) {
      return NULL;
   }

   comp->width = width;
   comp->height = height;
   comp->window_list = NULL;
   comp->num_windows = 0;
   comp->window_list_length = 0;
   comp->im_mask0 = NULL;
   comp->im_mask1 = NULL;
   comp->window_ids = NULL;
   comp->image = NULL;
   comp->diff_threshold = params->diff_threshold;
   comp->dilation_rounds = params->dilation_rounds;
   comp->max_frac_foreground = params->max_frac_foreground;

   /* allocate buffers */
   comp->im_mask0 = (uint8_t*)malloc(width *
     height * sizeof(uint8_t));
   if (comp->im_mask0 == NULL) {
      free(comp);
      return NULL;
   }

   comp->im_mask1 = (uint8_t*)malloc(width *
     height * sizeof(uint8_t));
   if (comp->im_mask1 == NULL) {
      free(comp->im_mask0);
      free(comp);
      return NULL;
   }

   comp->window_ids = (size_t*)malloc(width *
     height * sizeof(size_t));
   if (comp->window_ids == NULL) {
      free(comp->im_mask0);
      free(comp->im_mask1);
      free(comp);
      return NULL;
   }

   return comp;
}


/**
 * This function destroys the compressor
 *
 * @param comp Compressor
 */
void
ufmf_compressor_destroy(ufmf_compressor_t* comp)
{
   if (comp->window_list != NULL) {
      free(comp->window_list);
   }

   if (comp->im_mask0 != NULL) {
      free(comp->im_mask0);
   }

   if (comp->im_mask1 != NULL) {
      free(comp->im_mask1);
   }

   if (comp->window_ids != NULL) {
      free(comp->window_ids);
   }


   free(comp);
}


/**
 * This function compresses the passed frame. 
 *
 * @param im Image to add to stream
 * @param bg Background image
 * @returns Zero on sucess, one on failure.
 */
int
ufmf_compressor_compress(ufmf_compressor_t* comp,
   uint8_t* im, uint8_t* bg)
{
   size_t width = comp->width;
   size_t height = comp->height;
   int diff_threshold = comp->diff_threshold;
   uint8_t* im_mask0 = comp->im_mask0;
   uint8_t* im_mask1 = comp->im_mask1;

   /* reset the compressor */
   comp->num_windows = 0;
   comp->image = im;

   /* scan frame for changed pixels, use window_ids
    * to store foreground mask
    */
   size_t foreground_pixels = 0;
   for (size_t pixel_idx = 0; pixel_idx < width * height; pixel_idx++) {
      int is_fg = abs((int)im[pixel_idx] - bg[pixel_idx]) > diff_threshold;

      im_mask0[pixel_idx] = is_fg;
      foreground_pixels += is_fg;
   }

   /* if too many pixels are in the foreground, don't try to compress */
   float foreground_frac = ((float)foreground_pixels)/(width * height);

   if (foreground_frac > comp->max_frac_foreground) {
      if (comp->window_list_length == 0) {
         size_t new_len;
         ufmf_window_t* new_array;

         new_len = 1;

         new_array = (ufmf_window_t*)realloc(comp->window_list, new_len *
            sizeof(ufmf_window_t));
         if (new_array == NULL) {
            return 1;
         }

         comp->window_list = new_array;
         comp->window_list_length = new_len;
      }

      comp->num_windows = 1;

      ufmf_window_t* win = &comp->window_list[0];
      win->active = 1;
      win->row = 0;
      win->col = 0;
      win->width = (uint16_t)width; 
      win->height = (uint16_t)height;

      return 0;
   }

   /* perform dilation operation */
   for (size_t dilate_idx = 0; dilate_idx < comp->dilation_rounds; dilate_idx++) {
      ufmf_compressor_dilate(comp, im_mask0, im_mask1);

      /* swap im_mask0 and im_mask1 */
      uint8_t* tmp = im_mask1;
      comp->im_mask1 = im_mask1 = im_mask0;
      comp->im_mask0 = im_mask0 = tmp;
   }

   /* assign each pixel to a window */
   if (ufmf_compressor_label_rows(comp)) {
      return 1;
   }

   /* merge windows together to save space */
   if (ufmf_compressor_merge_windows(comp)) {
      return 1;
   }

   return 0;
}


/**
 * This function writes the passed frame to the output file.
 *
 * @param fptr File pointer
 * @param timestamp Timestamp for frame
 * @returns Zero on success, and one on failure
 */
int
ufmf_compressor_write_frame(ufmf_compressor_t* comp, 
   FILE* fptr, double timestamp)
{

   /* write timestamp */
   ufmf_assert(sizeof(timestamp) == 8);
   fwrite(&timestamp, 8, 1, fptr);

   /* count the number of active windows and output pixels */
   size_t num_active_windows = 0;
   size_t num_output_pixels = 0;

   ufmf_window_t* window_list = comp->window_list;

   for (size_t i = 0; i < comp->num_windows; i++) {
      ufmf_window_t *win = &window_list[i];
      num_active_windows += (win->active);
      num_output_pixels += (win->width * win->height);
   }

   /* allocate temporary memory for buffers */
   char* data_buffer = (char*)malloc(4 * sizeof(uint16_t) *
      num_active_windows + sizeof(uint8_t) * num_output_pixels);
   if (data_buffer == NULL) {
      return 1;
   }

   size_t data_buffer_pos = 0;

   /* populate the buffers */
   size_t win_idx = 0;
   for (size_t i = 0; i < num_active_windows; i++) {
      ufmf_window_t* win;

      size_t width = comp->width;
      uint8_t* image = comp->image;

      for (;;) {
         win = &window_list[win_idx];
         if (win->active) break;
         win_idx++;
      }

      data_buffer[data_buffer_pos++] = win->col & 0x00ff;
      data_buffer[data_buffer_pos++] = (win->col & 0xff00) >> 8;
      data_buffer[data_buffer_pos++] = win->row & 0x00ff;
      data_buffer[data_buffer_pos++] = (win->row & 0xff00) >> 8;
      data_buffer[data_buffer_pos++] = win->width & 0x00ff;
      data_buffer[data_buffer_pos++] = (win->width & 0xff00) >> 8;
      data_buffer[data_buffer_pos++] = win->height & 0x00ff;
      data_buffer[data_buffer_pos++] = (win->height & 0xff00) >> 8;

      for (size_t row = win->row; row < win->row + win->height; row++) {
         for (size_t col = win->col; col < win->col + win->width; col++) {
            data_buffer[data_buffer_pos++] = (unsigned char)image[row * width + col];
         }
      }

      win_idx++;
   }

   /* number of active windows */
   uint32_t win_count = (uint32_t)num_active_windows;
   ufmf_assert(sizeof(win_count) == 4);
   fwrite(&win_count, 4, 1, fptr);

   /* write buffer to the file */
   fwrite(data_buffer, 1, data_buffer_pos, fptr);

   if (ferror(fptr)) {
      return 1;
   }

   free(data_buffer);
   return 0;
}


/**
 * This function performs image dilation on the passed image. The image
 * elements should have values of either zero or one.
 *
 * @param comp Compressor
 * @param im Image to dilate
 * @param im_out Output image
 */
void
ufmf_compressor_dilate(ufmf_compressor_t* comp,
   const uint8_t* im, uint8_t* im_out)
{
   size_t height = comp->height;
   size_t width = comp->width;

   memcpy(im_out, im, width * height * sizeof(uint8_t));

   /* implement special cases so that the usual
    * case can assume width>1 and height>1, which
    * allows faster implementation
    */
   if (width <= 1 && height <= 1) {
      return;
   }

   /* do one-dimensional dilation, because above we can
    * assume that the length is greater than zero
    */
   if (width == 1 || height == 1) {
      size_t len;

      if (width == 1) {
         len = height;
      } else {
         len = width;
      }

      size_t col;

      /* code for first column */
      for (col = 0; col < 1; col++) {
         uint8_t cur_pixel = im[col];
         if ((cur_pixel & 0x1) == 0) continue;

         im_out[col] = 1;
         im_out[col+1] = 1;
      }

      /* code for middle columns */
      for (; col+1 < len; col++) {
         uint8_t cur_pixel = im[col];
         if ((cur_pixel & 0x1) == 0) continue;

         im_out[col-1] = 1;
         im_out[col] = 1;
         im_out[col+1] = 1;
      }

      /* code for final column */
      for (; col < len; col++) {
         uint8_t cur_pixel = im[col];
         if ((cur_pixel & 0x1) == 0) continue;

         im_out[col] = 1;
         im_out[col-1] = 1;
      }

      return;
   }

   /* the code that follows can assume that
    * width > 1 and height > 1
    */

   /* code for the first row */
   size_t row;
   for (row = 0; row < 1; row++) {
      size_t col;

      /* code for the first column */
      for (col = 0; col < 1; col++) {
         uint8_t cur_pixel = im[row*width + col];
         if ((cur_pixel & 0x1) == 0) continue;

         im_out[(row+0)*width + (col+0)] = 1;
         im_out[(row+0)*width + (col+1)] = 1;

         im_out[(row+1)*width + (col+0)] = 1;
         im_out[(row+1)*width + (col+1)] = 1;
      }

      /* code for middle columns */
      for (; col+1 < width; col++) {
         uint8_t cur_pixel = im[row*width + col];
         if ((cur_pixel & 0x1) == 0) continue;

         im_out[(row+0)*width + (col-1)] = 1;
         im_out[(row+0)*width + (col+0)] = 1;
         im_out[(row+0)*width + (col+1)] = 1;

         im_out[(row+1)*width + (col-1)] = 1;
         im_out[(row+1)*width + (col+0)] = 1;
         im_out[(row+1)*width + (col+1)] = 1;
      }
    
      /* code for last column */ 
      for (; col < width; col++) {
         uint8_t cur_pixel = im[row*width + col];
         if ((cur_pixel & 0x1) == 0) continue;

         im_out[(row+0)*width + (col-1)] = 1;
         im_out[(row+0)*width + (col+0)] = 1;

         im_out[(row+1)*width + (col-1)] = 1;
         im_out[(row+1)*width + (col+0)] = 1;
      }
   }

   /* code for middle rows */
   for (; row+1 < height; row++) {
      size_t col;

      /* code for first column */
      for (col = 0; col < 1; col++) {
         uint8_t cur_pixel = im[row*width + col];
         if ((cur_pixel & 0x1) == 0) continue;

         im_out[(row-1)*width + (col+0)] = 1;
         im_out[(row-1)*width + (col+1)] = 1;

         im_out[(row+0)*width + (col+0)] = 1;
         im_out[(row+0)*width + (col+1)] = 1;

         im_out[(row+1)*width + (col+0)] = 1;
         im_out[(row+1)*width + (col+1)] = 1;
      }

      /* code for middle columns */
      for (; col+1 < width; col++) {
         uint8_t cur_pixel = im[row*width + col];
         if ((cur_pixel & 0x1) == 0) continue;

         im_out[(row-1)*width + (col-1)] = 1; 
         im_out[(row-1)*width + (col+0)] = 1;
         im_out[(row-1)*width + (col+1)] = 1;

         im_out[(row+0)*width + (col-1)] = 1;
         im_out[(row+0)*width + (col+0)] = 1;
         im_out[(row+0)*width + (col+1)] = 1;

         im_out[(row+1)*width + (col-1)] = 1;
         im_out[(row+1)*width + (col+0)] = 1;
         im_out[(row+1)*width + (col+1)] = 1;
      }

      /* code for last column */
      for (; col < width; col++) {
         uint8_t cur_pixel = im[row*width + col];
         if ((cur_pixel & 0x1) == 0) continue;

         im_out[(row-1)*width + (col-1)] = 1;
         im_out[(row-1)*width + (col+0)] = 1;

         im_out[(row+0)*width + (col-1)] = 1;
         im_out[(row+0)*width + (col+0)] = 1;

         im_out[(row+1)*width + (col-1)] = 1;
         im_out[(row+1)*width + (col+0)] = 1;
      }
   }

   /* code for last row */
   for (; row < height; row++) {
      size_t col;

      /* code for first column */
      for (col = 0; col < 1; col++) {
         uint8_t cur_pixel = im[row*width + col];
         if ((cur_pixel & 0x1) == 0) continue;

         im_out[(row-1)*width + (col+0)] |= 1;
         im_out[(row-1)*width + (col+1)] |= 1;

         im_out[(row+0)*width + (col+0)] = 1;
         im_out[(row+0)*width + (col+1)] |= 1;
      }

      /* code for middle columns */
      for (; col+1 < width; col++) {
         uint8_t cur_pixel = im[row*width + col];
         if ((cur_pixel & 0x1) == 0) continue;

         im_out[(row-1)*width + (col-1)] = 1;
         im_out[(row-1)*width + (col+0)] = 1;
         im_out[(row-1)*width + (col+1)] = 1;

         im_out[(row+0)*width + (col-1)] = 1;
         im_out[(row+0)*width + (col+0)] = 1;
         im_out[(row+0)*width + (col+1)] = 1;
      }

      /* code for last column */
      for (; col < width; col++) {
         uint8_t cur_pixel = im[row*width + col];
         if ((cur_pixel & 0x1) == 0) continue;

         im_out[(row-1)*width + (col-1)] = 1;
         im_out[(row-1)*width + (col+0)] = 1;

         im_out[(row+0)*width + (col-1)] = 1;
         im_out[(row+0)*width + (col+0)] = 1;
      }
   }
}


/**
 * This function creates the initial set of windows, in which each stretch
 * of pixels in each row is given a window. In subsequent steps, these 
 * windows will be merged together.
 *
 * @param comp Compressor
 * @param Zero on success, one on failure.
 */
int
ufmf_compressor_label_rows(ufmf_compressor_t* comp)
{
   uint8_t* im_mask0 = comp->im_mask0;
   size_t* window_ids = comp->window_ids;
   ufmf_window_t* window_list = comp->window_list;
   size_t window_list_length = comp->window_list_length;

   size_t cur_num_windows = 0;
   ufmf_window_t* cur_window_ptr = NULL;

   size_t height = comp->height;
  
   memset(window_ids, 0, ((size_t)comp->width)*comp->height*sizeof(size_t));
 
   /* assign each pixel to a window */
   for (size_t row = 0; row < height; row++) {
      int make_new_window = 1;

      size_t width = comp->width;

      for (size_t col = 0; col < width; col++) {
         size_t mask = im_mask0[row * width + col];

         /* if the current pixel is off, indicate that
          * that the next on pixel should produce a window.
          */
         if (!mask) {
            make_new_window = 1;
            continue;
         }

         if (make_new_window) {
            cur_num_windows++;

            /* check if we need to enlarge window_list array */
            if (cur_num_windows > window_list_length) {
               size_t new_len;
               ufmf_window_t* new_array;

               if (window_list_length == 0) {
                  new_len = 2;
               } else {
                  new_len = 2 * window_list_length;
               }

               new_array = (ufmf_window_t*)realloc(window_list, new_len *
                 sizeof(ufmf_window_t));
               if (new_array == NULL) {
                  return 1;
               }

               comp->window_list = window_list = new_array;
               comp->window_list_length = window_list_length = new_len;
            }

            cur_window_ptr = &window_list[cur_num_windows-1];
            cur_window_ptr->active = 1;
            cur_window_ptr->row = (uint16_t)row;
            cur_window_ptr->col = (uint16_t)col;
            cur_window_ptr->width = 0;
            cur_window_ptr->height = 1;

            make_new_window = 0;
         }

         /* by this point cur_window_ptr is assigned, so we just need
          * to increment the width to indicate that the new pixel
          * should be added to this row stretch
          */
         cur_window_ptr->width++;

         /* tag pixel with window identifier (one-based index) */
         window_ids[row * width + col] = cur_num_windows;
      }
   }

   comp->window_list = window_list;
   comp->num_windows = cur_num_windows;
   comp->window_list_length = window_list_length;

   return 0;
}


/**
 * This function scans through the list of windows, merging windows
 * when it improves space efficiency.
 * 
 * @param comp Compressor
 * @returns Zero on success, and one on failure.
 */
int
ufmf_compressor_merge_windows(ufmf_compressor_t* comp)
{
  /* This code just checks the pixel directly above
   * and to the left to improve performance
   */

   size_t height = comp->height;
   size_t width = comp->width;
   size_t row;

   if (height == 0 || width == 0) return 0;

   /* for first row, just check pixel to the left */
   for (row = 0; row < 1; row++) {
      size_t col;

      for (col = 1; col < width; col++) {
         size_t inner_id;

         size_t id = comp->window_ids[row * width + col];
         if (id == 0) continue;

         inner_id = comp->window_ids[row* width + (col-1)];
         if (inner_id == 0 || id == inner_id) continue;

         if (ufmf_compressor_check_and_merge(comp, id, inner_id)) {
            /* if merged, then our ID might have changed,
             * so we need to update it.
             */
            id = comp->window_ids[row * width + col];
         }
      }
   }

   /* for subsequent rows, check pixel above current pixel */
   for (; row < height; row++) {
      size_t col;

      /* for first column just check pixel above */
      for (col = 0; col < 1; col++) {
         size_t id = comp->window_ids[row * width + col];
         if (id == 0) continue;

         size_t inner_id = comp->window_ids[(row-1)* width + col];
         if (inner_id == 0 || id == inner_id) continue;

         if (ufmf_compressor_check_and_merge(comp, id, inner_id)) {
            /* if merged, then our ID might have changed,
             * so we need to update it.
             */
            id = comp->window_ids[row * width + col];
         }
      }

      /* for subsequent columns, also check pixel to left */
      for (; col < width; col++) {
         size_t inner_id;

         size_t id = comp->window_ids[row * width + col];
         if (id == 0) continue;

         inner_id = comp->window_ids[(row-1)* width + col];
         if (inner_id == 0 || id == inner_id) continue;

         if (ufmf_compressor_check_and_merge(comp, id, inner_id)) {
            /* if merged, then our ID might have changed,
             * so we need to update it.
             */
            id = comp->window_ids[row * width + col];
         }

         inner_id = comp->window_ids[row* width + (col-1)];
         if (inner_id == 0 || id == inner_id) continue;

         if (ufmf_compressor_check_and_merge(comp, id, inner_id)) {
            /* if merged, then our ID might have changed,
             * so we need to update it.
             */
            id = comp->window_ids[row * width + col];
         }
      }
   }

   return 0;
}


/**
 * This function tests if the passed windows should be merged together. If they
 * should, then it does.
 *
 * @param comp Compressor
 * @param id1 ID of first window
 * @param id2 ID of second window
 * @returns True if the windows were merged, false otherwise. 
 */
int
ufmf_compressor_check_and_merge(ufmf_compressor_t* comp, size_t id1, size_t id2)
{
   size_t* window_ids = comp->window_ids;
   ufmf_window_t* window_list = comp->window_list;
   size_t width = comp->width;

   ufmf_window_t* window1 = &window_list[id1 - 1];
   ufmf_window_t* window2 = &window_list[id2 - 1];

   /* calculate the size of encompassing window */
   size_t row1_min = window1->row;
   size_t row1_max = window1->row + window1->height - 1;
   size_t col1_min = window1->col;
   size_t col1_max = window1->col + window1->width - 1;

   size_t row2_min = window2->row;
   size_t row2_max = window2->row + window2->height - 1;
   size_t col2_min = window2->col;
   size_t col2_max = window2->col + window2->width - 1;

   size_t row3_min = row1_min < row2_min ? row1_min : row2_min;
   size_t row3_max = row1_max > row2_max ? row1_max : row2_max;
   size_t col3_min = col1_min < col2_min ? col1_min : col2_min;
   size_t col3_max = col1_max > col2_max ? col1_max : col2_max;
   size_t window3_width = col3_max - col3_min + 1;
   size_t window3_height = row3_max - row3_min + 1;


   /* check if the encompassing window would be useful */
   size_t overhead = 4 * sizeof(uint16_t);  /* row, col, width, height */

   size_t cur_size = window1->width * window1->height +
      window2->width * window2->height + 2 * overhead;

   size_t test_size = window3_width * window3_height + overhead;

   /* if merging the windows would make file larger, return immediately */
   if (cur_size < test_size) {
      return 0;
   }

   /* merging windows would make file smaller, test if there are any
    * pixels in the encompassing window with a different id. if so, 
    * then forget about merger.
    */
   for (size_t row = row3_min; row <= row3_max; row++) {
      for (size_t col = col3_min; col <= col3_max; col++) {
         size_t id = window_ids[row * width + col];

         if (id != 0 && !(id == id1 || id == id2)) {
            return 0;
         }
      }
   }

   /* looks like we should perform the merger, copy values into the
    * first window and mark the second window as inactive.
    */
   window1->row = (uint16_t)row3_min;
   window1->col = (uint16_t)col3_min;
   window1->width = (uint16_t)window3_width;
   window1->height = (uint16_t)window3_height;
   window2->active = 0;

   /* mark pixels in window2 as belonging to window1 */
   for (size_t row = row2_min; row <= row2_max; row++) {
      for (size_t col = col2_min; col <= col2_max; col++) {
         size_t id = window_ids[row * width + col];

         if (id) {
            window_ids[row * width + col] = id1;
         }
      }
   }

   return 1;
}


/**
 * This function creates a ufmf_writer object
 *
 * @param file_name File name of output
 * @param width Image width
 * @param height Image height
 * @param params Compression parameters
 * @returns Pointer to writer on success, NULL pointer on failure.
 */
ufmf_writer_t* ufmf_writer_create(const char *file_name, uint16_t width,
  uint16_t height, const ufmf_params_t* params)
{
   ufmf_writer_t* writer;

   writer = (ufmf_writer_t*)malloc(sizeof(ufmf_writer_t));
   if (writer == NULL) {
      return NULL;
   }

   /* image variables */
   writer->width = width;
   writer->height = height;

   /* output variables */
   writer->file_ptr = NULL;

   /* frame index */
   writer->frame_index = NULL;
   writer->frame_index_pos = 0;
   writer->frame_index_length = 0;

   /* bg index */
   writer->bg_index = NULL;
   writer->bg_index_pos = 0;
   writer->bg_index_length = 0;

   /* index variables */
   writer->index_location = 0;
   writer->index_ptr_location = 0;

   /* writing state */
   writer->next_to_enqueue = 0;
   writer->next_to_compress = 0;
   writer->next_to_output = 0;
   writer->last_update_time = 0.0;

   writer->bg_update_period = params->bg_update_period_ramp_min;
   writer->bg_update_period_ramp_max = params->bg_update_period_ramp_max;
   writer->bg_update_period_ramp_min = params->bg_update_period_ramp_min;
   writer->bg_update_period_ramp_a = params->bg_update_period_ramp_a;
   writer->bg_update_period_ramp_b = params->bg_update_period_ramp_b;

   writer->is_closed = 0;
   writer->has_error = 0;

   /* initialize error mutex */
   if (ufmf_mutex_init(&writer->error_mutex)) {
      ufmf_mutex_destroy(&writer->error_mutex);
      free(writer);
      return NULL;
   }

   /* create bg */
   writer->bg_model = ufmf_background_create(((size_t)width)*height, params);
   if (writer->bg_model == NULL) {
      ufmf_mutex_destroy(&writer->error_mutex);
      free(writer);
      return NULL;
   }

   /* initialize compress mutex */
   if (ufmf_mutex_init(&writer->compress_mutex)) {
      ufmf_mutex_destroy(&writer->error_mutex);
      ufmf_background_destroy(writer->bg_model);
      free(writer);
      return NULL;
   }

   /* initialize the queue */
   if (ufmf_writer_init_queue(writer, params)) {
      ufmf_mutex_destroy(&writer->error_mutex);
      ufmf_background_destroy(writer->bg_model);
      ufmf_mutex_destroy(&writer->compress_mutex);
      free(writer);
      return NULL;
   }

   /* open up the output file */
   writer->file_ptr = fopen(file_name, "wb");
   if (writer->file_ptr == NULL) {
      ufmf_mutex_destroy(&writer->error_mutex);
      ufmf_background_destroy(writer->bg_model);
      ufmf_mutex_destroy(&writer->compress_mutex);
      ufmf_writer_free_queue(writer);
      free(writer);
      return NULL;
   }

   /* write header */
   if (ufmf_writer_make_header(writer)) {
      ufmf_mutex_destroy(&writer->error_mutex);
      ufmf_background_destroy(writer->bg_model);
      ufmf_mutex_destroy(&writer->compress_mutex);
      ufmf_writer_free_queue(writer);
      fclose(writer->file_ptr);
      free(writer);
      return NULL;
   }

   /* launch compression and output threads */
   if (ufmf_writer_launch_threads(writer, params)) {
      ufmf_mutex_destroy(&writer->error_mutex);
      ufmf_background_destroy(writer->bg_model);
      ufmf_mutex_destroy(&writer->compress_mutex);
      ufmf_writer_free_queue(writer);
      fclose(writer->file_ptr);
      free(writer);
      return NULL;
   }

   return writer;
}


/**
 * This function destroys the ufmf writer. The writer does not
 * need to be closed prior to calling this function. However,
 * if the writer is not closed then the output file will likely
 * be invalid.
 *
 * @param writer Writer
 */
void
ufmf_writer_destroy(ufmf_writer_t* writer)
{
    if (!writer->is_closed) {
       ufmf_writer_collect_threads(writer);
    }

   ufmf_background_destroy(writer->bg_model);

   ufmf_mutex_destroy(&writer->compress_mutex);

   ufmf_writer_free_queue(writer);

   if (writer->file_ptr != NULL) {
      fclose(writer->file_ptr);
   }

   if (writer->frame_index != NULL) {
      free(writer->frame_index);
   }

   if (writer->bg_index != NULL) {
      free(writer->bg_index);
   }

   if (writer->threads_array != NULL) {
      free(writer->threads_array);
   }

   free(writer);
}


/**
 * This function stops writing and closes the file.
 *
 * @param writer Writer
 * @returns Zero on success, and one on failure.
 */
int
ufmf_writer_close(ufmf_writer_t* writer)
{
   if (ufmf_writer_collect_threads(writer)) {
      return 1;
   }

   if (writer->has_error) {
      return 1;
   }

   if (ufmf_writer_make_trailer(writer)) {
      return 1;
   }

   writer->is_closed = 1;

   return 0;
}


/**
 * This function adds a frame to the movie.
 * 
 * @param writer Writer
 * @param frame Frame image data
 * @param timestamp Timestamp for image
 * @returns Zero on success, and one on failure.
 */
int
ufmf_writer_add(ufmf_writer_t* writer, uint8_t* frame, double timestamp)
{
   /* This function is invoked from the loader thread */

   int bg_changed;

   /* update background if necessary */
   if (ufmf_writer_update_bg_model(writer, frame, timestamp, &bg_changed)) {
      return 1;
   }

   /* compress frame and write to output */
   if (ufmf_writer_add_to_queue(writer, frame, timestamp, bg_changed)) {
      return 1;
   }

   ufmf_log("lock writer->error_mutex");
   ufmf_mutex_lock(&writer->error_mutex);

   int local_has_error = writer->has_error;

   ufmf_log("unlock writer->error_mutex");
   ufmf_mutex_unlock(&writer->error_mutex);

   return local_has_error;
}


/**
 * This function launches the compression and output threads.
 */
int
ufmf_writer_launch_threads(ufmf_writer_t* writer, const ufmf_params_t* params)
{
   writer->threads_len = params->num_threads;

   writer->threads_array = (ufmf_thread_t*)malloc(
     writer->threads_len * sizeof(ufmf_thread_t));
   if (writer->threads_array == NULL) {
      return 1;
   }

#if defined(_WIN32) || defined(_WIN64)
   writer->output_thread = CreateThread(NULL, 0,
      ufmf_writer_start_output_thread, (LPVOID)writer, 0, NULL);
#elif defined(_POSIX_VERSION)
   int retval;

   retval = pthread_create(&writer->output_thread, NULL,
     &ufmf_writer_start_output_thread, (void*)writer);
   if (retval != 0) {
      ufmf_abort();
   }
#else
#error "Unsupported operating system"
#endif

   for (size_t i = 0; i < writer->threads_len; i++) {
#if defined(_WIN32) || defined(_WIN64)
      writer->threads_array[i] = CreateThread(NULL, 0, 
        ufmf_writer_start_compression_thread, (LPVOID)writer, 0, NULL);
#elif defined(_POSIX_VERSION)
      retval = pthread_create(&writer->threads_array[i], NULL,
        &ufmf_writer_start_compression_thread, (void*)writer);
      if (retval != 0) {
         ufmf_abort();
      }
#else
#error "Unsupported operating system"
#endif

   }

   return 0;
}


/**
 * This function collects the compression and output threads.
 *
 * @returns Zero on success and non-zero number of failure.
 */
int
ufmf_writer_collect_threads(ufmf_writer_t* writer)
{
   int retval;

   /* load poison pills into queue, when compression thread
   * receives the pill, then it will shut itself down.
   */
   for (size_t idx = 0; idx < writer->threads_len; idx++) {
      ufmf_writer_add_poison_pill_to_queue(writer);
   }

   for (size_t i = 0; i < writer->threads_len; i++) {

#if defined(_WIN32) || defined(_WIN64)
      retval = WaitForSingleObject(writer->threads_array[i], INFINITE);
      if (retval != WAIT_OBJECT_0) {
         ufmf_abort();
      }
#elif defined(_POSIX_VERSION)
      retval = pthread_join(writer->threads_array[i], NULL);
      if (retval != 0) {
         ufmf_abort();
      }
#else
#error "Unsupported operating system"
#endif

   }

#if defined(_WIN32) || defined(_WIN64)
   retval = WaitForSingleObject(writer->output_thread, INFINITE);
   if (retval != WAIT_OBJECT_0) {
       ufmf_abort();
   }
#elif defined(_POSIX_VERSION)
   retval = pthread_join(writer->output_thread, NULL);
   if (retval != 0) {
       ufmf_abort();
   }
#else
#error "Unsupported operating system"
#endif

   return 0;
}


/**
 * This function launches the output thread
 */
#if defined(_WIN32) || defined(_WIN64)
DWORD WINAPI
ufmf_writer_start_output_thread(LPVOID arg)
{
   return ufmf_writer_output_frames((ufmf_writer_t*)arg);
}
#elif defined(_POSIX_VERSION)
void*
ufmf_writer_start_output_thread(void* arg)
{
   long retval = ufmf_writer_output_frames((ufmf_writer_t*)arg);
   return (void*)retval;
}
#else
#error "Unsupported operating system"
#endif


/**
 * This function launches the output thread
 */
#if defined(_WIN32) || defined(_WIN64)
DWORD WINAPI
ufmf_writer_start_compression_thread(LPVOID arg)
{
   return ufmf_writer_compress_frames((ufmf_writer_t*)arg);
}
#elif defined(_POSIX_VERSION)
void*
ufmf_writer_start_compression_thread(void* arg)
{
   long retval = ufmf_writer_compress_frames((ufmf_writer_t*)arg); 
   return (void*)retval;
}
#else
#error "Unsupported operating system"
#endif


/**
 * This function writes the header for the ufmf file.
 *
 * @returns Zero on success, and one on failure.
 */
int
ufmf_writer_make_header(ufmf_writer_t* writer)
{
   /* location of index */
   writer->index_location = 0;

   /* UFMF Version 4 */
   uint32_t ufmf_version = 4;

   /* write "ufmf" */
   const char ufmf_string[] = "ufmf";
   ufmf_assert(strlen(ufmf_string) == 4);
   fwrite(ufmf_string, 1, 4, writer->file_ptr); 

   /* write version */
   ufmf_assert(sizeof(ufmf_version) == 4);
   fwrite(&ufmf_version, 4, 1, writer->file_ptr);

   /* this is where we write the index location */
   writer->index_ptr_location = ftell(writer->file_ptr);

   /* write index location */
   ufmf_assert(sizeof(writer->index_location) == 8);
   fwrite(&writer->index_location, 8, 1, writer->file_ptr);

   /* image width, height */
   ufmf_assert(sizeof(writer->width) == 2);
   fwrite(&writer->width, 2, 1, writer->file_ptr);
   ufmf_assert(sizeof(writer->height) == 2);
   fwrite(&writer->height, 2, 1, writer->file_ptr);

   /* whether it is fixed size patches */
   uint8_t is_fixed_size = 0;
   ufmf_assert(sizeof(is_fixed_size) == 1);
   fwrite(&is_fixed_size, 1, 1, writer->file_ptr);

   /* raw coding string length */
   const char color_coding[] = "MONO8";
   uint8_t color_coding_length = (uint8_t)strlen(color_coding);
   ufmf_assert(sizeof(color_coding_length) == 1);
   fwrite(&color_coding_length, 1, 1, writer->file_ptr);

   /* color coding string */
   fwrite(color_coding, 1, color_coding_length, writer->file_ptr);

   /* check for error */
   if (ferror(writer->file_ptr)) {
      return 1;
   }

   return 0;
}


/**
 * This function adds the frame to the background model.
 *
 * @param writer Writer
 * @param frame Image frame
 * @param timestamp Timestamp
 * @param bg_changed Set to one if bg changed.
 * @returns Zero on success, and one on failure
 */
int
ufmf_writer_update_bg_model(ufmf_writer_t* writer, uint8_t* frame,
  double timestamp, int* bg_changed)
{
   /* This function is invoked from the loader thread */

   double dt = timestamp - writer->last_update_time;

   if (writer->next_to_enqueue != 0 && dt < writer->bg_update_period) {
      *bg_changed = 0;
      return 0;
   }

   if (ufmf_background_add_frame(writer->bg_model, frame, bg_changed)) {
      return 1;
   }

   /* store update time */
   writer->last_update_time = timestamp;

   /* re-compute update period */
   writer->bg_update_period = writer->bg_update_period * writer->bg_update_period_ramp_a +
      writer->bg_update_period_ramp_b;
   writer->bg_update_period = writer->bg_update_period < writer->bg_update_period_ramp_max ?
      writer->bg_update_period : writer->bg_update_period_ramp_max;

   return 0;
}


/** 
 * This function writes the passed frame to the output stream
 *
 * @param writer Writer
 * @param frame Uncompressed image data
 * @param timestamp Timestamp for frame
 * @param bg_changed True if background changed
 */
int
ufmf_writer_add_to_queue(ufmf_writer_t* writer, uint8_t* frame,
  double timestamp, int bg_changed)
{
   /* This function is invoked from the loader thread */

   size_t slot_idx = writer->next_to_enqueue % writer->queue_length;
   ufmf_queue_slot_t* slot = &writer->queue_data[slot_idx];

   ufmf_log("waiting for slot[%d].ready_to_fill", slot_idx);
   ufmf_sem_wait(&slot->ready_to_fill);
   ufmf_log("received slot[%d].ready_to_fill", slot_idx);

   memcpy(slot->image, frame, ((size_t)writer->width) *
     writer->height * sizeof(uint8_t));

   uint8_t* bg_image = ufmf_background_get(writer->bg_model);

   memcpy(slot->bg_image, bg_image, ((size_t)writer->width) *
     writer->height * sizeof(uint8_t));

   slot->poison_pill = 0;
   slot->output_bg = bg_changed;
   slot->timestamp = timestamp;

   ufmf_log("post slot[%d].ready_to_compress", slot_idx); 
   ufmf_sem_post(&slot->ready_to_compress);

   writer->next_to_enqueue += 1;

   return 0;
}


/** 
 * This function writes a poison pill frame to the output stream
 *
 * @param writer Writer
 */
void
ufmf_writer_add_poison_pill_to_queue(ufmf_writer_t* writer)
{
   /* This function is invoked from the loader thread */

   size_t slot_idx = writer->next_to_enqueue % writer->queue_length;
   ufmf_queue_slot_t* slot = &writer->queue_data[slot_idx];

   ufmf_log("waiting for slot[%d].ready_to_fill", slot_idx);
   ufmf_sem_wait(&slot->ready_to_fill);
   ufmf_log("received slot[%d].ready_to_fill", slot_idx);

   slot->poison_pill = 1;

   ufmf_log("post slot[%d].ready_to_compress", slot_idx); 
   ufmf_sem_post(&slot->ready_to_compress);

   writer->next_to_enqueue += 1;
}


/**
 * This function removes processed frames from the queue and
 * writes them to the output file
 */
int
ufmf_writer_output_frames(ufmf_writer_t* writer)
{
   /* This function is invoked from the output thread */

   /* This function should not return until num_threads poison pills have
    * been swallowed, otherwise it is difficult to collect threads correctly.
    * in the event of an error, the function should mark has_error (protected
    * by error_mutex), and keep on keeping on.
    */

   int poison_pill_count = 0;

   for (;;) {

      size_t slot_idx = writer->next_to_output % writer->queue_length;
      ufmf_queue_slot_t* slot = &writer->queue_data[slot_idx];

      ufmf_log("waiting for slot[%d].ready_to_write", slot_idx);
      ufmf_sem_wait(&slot->ready_to_write);
      ufmf_log("received slot[%d].ready_to_write", slot_idx);

      if (slot->poison_pill) {
         /* image processing was halted, so we should
          * call it quits as well.
          */
         ufmf_log("output thread swallowed poison pill");

         poison_pill_count++;
         writer->next_to_output++;

         ufmf_log("post slot[%d].ready_to_fill", slot_idx); 
         ufmf_sem_post(&slot->ready_to_fill);

         if (poison_pill_count == writer->threads_len) {
            return 0;
         } else {
            continue;
         }
      }

      if (slot->is_garbage) {
         writer->next_to_output++;
         ufmf_log("post slot[%d].ready_to_fill", slot_idx);
         ufmf_sem_post(&slot->ready_to_fill);
         continue;
      }

      if (slot->output_bg) {
         ufmf_log("writing key frame for frame %d", writer->next_to_output);

         if (ufmf_writer_make_key_frame(writer, slot->bg_image,
             slot->timestamp)) {

            ufmf_log("lock writer->error_mutex");
            ufmf_mutex_lock(&writer->error_mutex);
            writer->has_error = 1;
            ufmf_log("unlock writer->error_mutex");
            ufmf_mutex_unlock(&writer->error_mutex);

            writer->next_to_output++;
            ufmf_log("post slot[%d].ready_to_fill", slot_idx);
            ufmf_sem_post(&slot->ready_to_fill);
            continue;
         }
      }
 
      ufmf_log("writing frame for frame %d", writer->next_to_output);
      if (ufmf_writer_make_frame(writer, slot)) {
         ufmf_log("lock writer->error_mutex");
         ufmf_mutex_lock(&writer->error_mutex);
         writer->has_error = 1;
         ufmf_log("unlock writer->error_mutex");
         ufmf_mutex_unlock(&writer->error_mutex);

         writer->next_to_output++;
         ufmf_log("post slot[%d].ready_to_fill", slot_idx);
         ufmf_sem_post(&slot->ready_to_fill);
         continue;
      }

      writer->next_to_output++;
      ufmf_log("post slot[%d].ready_to_fill", slot_idx);
      ufmf_sem_post(&slot->ready_to_fill);
   }

   return 0;
}


/**
 * This function compresses the frames.
 */
int
ufmf_writer_compress_frames(ufmf_writer_t* writer)
{
   /* This function is invoked from the compresion thread */

   /* This function should not return until the poison pill is swallowed,
    * otherwise it is difficult to collect threads correctly. in the event
    * of an error, the function should mark has_error (protected by 
    * error_mutex), indicate that the slot is garbage, and keep on
    * keeping on.
    */

   for (;;) {
      /* below we get the slot to compress, note that because the queue
       * has finite length, multiple threads can arrive at the same slot,
       * in this case, the thread may not get the frame that corresponds
       * to the writer->next_to_compress value it had when it retrieved
       * the slot. this is okay, just think of it as the thread with the
       * right writer->next_to_compress value and this thread switched
       * roles. that is fine, but nothing else in the following code can
       * should assume that we actually have the frame with the frame
       * index from writer->next_to_compress
       */

      ufmf_log("lock writer->compress_mutex");
      ufmf_mutex_lock(&writer->compress_mutex);
      size_t slot_idx = writer->next_to_compress % writer->queue_length;
      ufmf_queue_slot_t* slot = &writer->queue_data[slot_idx];

      writer->next_to_compress++;

      ufmf_log("unlock writer->compress_mutex");
      ufmf_mutex_unlock(&writer->compress_mutex);

      ufmf_log("waiting for slot[%d].ready_to_compress", slot_idx);
      ufmf_sem_wait(&slot->ready_to_compress);
      ufmf_log("received slot[%d].ready_to_compress", slot_idx);

      if (slot->poison_pill) {
         ufmf_log("compression thread swallowed poison pill"); 

         ufmf_log("post slot[%d].ready_to_write", slot_idx); 
         ufmf_sem_post(&slot->ready_to_write);
         return 0; 
      }

      if (ufmf_compressor_compress(slot->compressor, slot->image,
          slot->bg_image)) {
         ufmf_log("lock writer->error_mutex");
         ufmf_mutex_lock(&writer->error_mutex);

         writer->has_error = 1;

         ufmf_log("unlock writer->error_mutex");
         ufmf_mutex_unlock(&writer->error_mutex);

         slot->is_garbage = 1;
      } else {
         slot->is_garbage = 0;
      }

      ufmf_log("post slot[%d].ready_to_write", slot_idx); 
      ufmf_sem_post(&slot->ready_to_write);
   }

   return 0;
}


/** 
 * This function writes the passed frame to the output stream. This function
 * must only be called from the file output thread.
 *
 * @param writer Writer
 * @param slot Queue slot 
 * @param timestamp Timestamp for frame
 */
int
ufmf_writer_make_frame(ufmf_writer_t* writer, ufmf_queue_slot_t* slot)
{
   uint64_t file_pos_start;

   /* location of this frame */
   if (ufmf_writer_get_file_position(writer->file_ptr, &file_pos_start)) {
      return 1;
   }

   /* add current location to index */
   if (writer->frame_index_pos >= writer->frame_index_length) {
      /* first we resize the list */
      ufmf_frame_metadata_t* new_alloc;
      size_t new_len;

      new_len = writer->frame_index_pos == 0 ? 1 : 
         2 * writer->frame_index_pos;

      new_alloc = (ufmf_frame_metadata_t*)realloc(
        writer->frame_index, sizeof(ufmf_frame_metadata_t) * new_len);
      if (new_alloc == NULL) {
         return 1;
      }

      writer->frame_index = new_alloc;
      writer->frame_index_length = new_len;
   }

   writer->frame_index[writer->frame_index_pos].pos = file_pos_start;
   writer->frame_index[writer->frame_index_pos].timestamp = slot->timestamp;
   writer->frame_index_pos++;
 
   /* write chunk type */
   uint8_t chunk = UFMF_FRAME_CHUNK;
   ufmf_assert(sizeof(chunk) == 1);
   fwrite(&chunk, 1, 1, writer->file_ptr);

   /* write the frame */
   if (ufmf_compressor_write_frame(slot->compressor,
         writer->file_ptr, slot->timestamp)) {
      return 1;
   }

   return 0;
}


/**
 * This function writes the passed background image to the output
 *
 * @param writer Writer
 * @param im Image data
 * @param timestamp Timestamp for image
 * @returns Zero on success, one on failure
 */
int
ufmf_writer_make_key_frame(ufmf_writer_t* writer,
  uint8_t* im, double timestamp)
{
   uint64_t file_pos_start;

   /* location of this frame */
   if (ufmf_writer_get_file_position(writer->file_ptr, &file_pos_start)) {
      return 1;
   }

   /* add to keyframe index */
   if (writer->bg_index_pos >= writer->bg_index_length) {
      /* first we resize the list */
      ufmf_frame_metadata_t* new_alloc;
      size_t new_len;

      new_len = writer->bg_index_pos == 0 ? 1 : 
         2 * writer->bg_index_pos;

      new_alloc = (ufmf_frame_metadata_t*)realloc(
        writer->bg_index, sizeof(ufmf_frame_metadata_t) * new_len);
      if (new_alloc == NULL) {
         return 1;
      }

      writer->bg_index = new_alloc;
      writer->bg_index_length = new_len;
   }

   writer->bg_index[writer->bg_index_pos].pos = file_pos_start;
   writer->bg_index[writer->bg_index_pos].timestamp = timestamp;
   writer->bg_index_pos++;
 
   /* write keyframe chunk identifier */
   uint8_t chunk = UFMF_KEYFRAME_CHUNK;
   ufmf_assert(sizeof(chunk) == 1);
   fwrite(&chunk, 1, 1, writer->file_ptr);

   /* write the keyframe type */
   const char key_frame_type[] = "mean";
   uint8_t key_frame_type_length = sizeof(key_frame_type) - 1;

   ufmf_assert(sizeof(key_frame_type_length) == 1);
   fwrite(&key_frame_type_length, 1, 1, writer->file_ptr);

   ufmf_assert(sizeof(key_frame_type) - 1 == key_frame_type_length);
   fwrite(key_frame_type, 1, key_frame_type_length, writer->file_ptr);

   /* write the data type */
   char data_type = 'B';
   fwrite(&data_type, 1, 1, writer->file_ptr);

   /* width, height */
   ufmf_assert(sizeof(writer->width) == 2);
   fwrite(&writer->width, 2, 1, writer->file_ptr);

   ufmf_assert(sizeof(writer->height) == 2);
   fwrite(&writer->height, 2, 1, writer->file_ptr);

   /* timestamp */
   ufmf_assert(sizeof(timestamp) == 8);
   fwrite(&timestamp, 8, 1, writer->file_ptr);

   /* write the frame */
   fwrite(im, 1, ((size_t)writer->width)*writer->height,
      writer->file_ptr);

   if (ferror(writer->file_ptr)) {
      return 1;
   }

   return 0;
}


/**
 * This function writes the trailor for the file.
 *
 * @param writer Writer
 * @returns Zero on success, one on failure.
 */
int
ufmf_writer_make_trailer(ufmf_writer_t* writer)
{
   /* write the index at the end of the file */
   if (ufmf_writer_goto_file_end(writer->file_ptr)) {
      return 1;
   }

   /* write index chunk identifier */
   uint8_t chunk = UFMF_INDEX_DICT_CHUNK;
   ufmf_assert(sizeof(chunk) == 1);
   fwrite(&chunk, 1, 1, writer->file_ptr);

   /* save location of index */
   if (ufmf_writer_get_file_position(writer->file_ptr,
         &writer->index_location)) {
      return 1;
   }

#if 0
   // Format for the end of the file is as follows:

   dict(
      "frame"-> dict(
         "loc"->uint64_t([index0, index1, ...]),
         "timestamp"->double ([timestamp0, timestamp1]))
      "keyframe"->dict(
         "mean"->dict(
            "loc"->uint64_t([index0, index1, ...]),
            "timestamp"->double ([timestamp0, timestamp1]))))
#endif

   /* write index dictionary */
   
   /* write a 'd' for dict */
   char d = 'd';
   fwrite(&d, 1, 1, writer->file_ptr);

   /* write the number of keys */
   uint8_t num_keys = 2;
   ufmf_assert(sizeof(num_keys) == 1);
   fwrite(&num_keys, 1, 1, writer->file_ptr);

   /* write index->frame */
   const char frame_string[] = "frame";
   uint16_t frame_string_length = (uint16_t)strlen(frame_string);

   /* write the length of the key */
   ufmf_assert(sizeof(frame_string_length) == 2);
   fwrite(&frame_string_length, 2, 1, writer->file_ptr);

   /* write the key */
   fwrite(frame_string, 1, frame_string_length, writer->file_ptr);

   /* write the index dictionary */
   ufmf_writer_make_index_dict(writer, writer->frame_index,
     writer->frame_index_pos);

   /* write index->keyframe */
   const char keyframe_string[] = "keyframe";
   uint16_t keyframe_string_length = (uint16_t)strlen(keyframe_string);

   /* write the length of the key */
   ufmf_assert(sizeof(keyframe_string_length) == 2);
   fwrite(&keyframe_string_length, 2, 1, writer->file_ptr);

   /* write the key */
   fwrite(keyframe_string, 1, keyframe_string_length, writer->file_ptr);

   /* write a 'd' for dict */
   fwrite(&d, 1, 1, writer->file_ptr);

   /* write the number of keys */
   uint8_t nkeys = 1;
   ufmf_assert(sizeof(nkeys) == 1);
   fwrite(&nkeys, 1, 1, writer->file_ptr);

   /* write index->keyframe->mean */
   const char mean_string[] = "mean";
   uint16_t mean_string_length = (uint16_t)strlen(mean_string);

   /* write the length of the key */
   ufmf_assert(sizeof(mean_string_length) == 2);
   fwrite(&mean_string_length, 2, 1, writer->file_ptr);

   /* write the key */
   fwrite(mean_string, 1, mean_string_length, writer->file_ptr);

   /* write the index dictionary */
   ufmf_writer_make_index_dict(writer, writer->bg_index,
     writer->bg_index_pos);

   /* write the index location */
   if (ufmf_writer_set_file_position(writer->file_ptr,
         writer->index_ptr_location)) {
      return 1;
   }

   ufmf_assert(sizeof(writer->index_location) == 8);
   fwrite(&writer->index_location, 8, 1, writer->file_ptr);

   /* check for errors */
   if (ferror(writer->file_ptr)) {
      return 1;
   }

   /* close the file */
   fclose(writer->file_ptr);
   writer->file_ptr = NULL;

   free(writer->frame_index);
   writer->frame_index = 0;
   writer->frame_index_pos = 0;
   writer->frame_index_length = 0;

   free(writer->bg_index);
   writer->bg_index = 0;
   writer->bg_index_pos = 0;
   writer->bg_index_length = 0;

   return 0;
}

/**
 * This function writes the index dictionary to the output file
 *
 * @param writer Writer
 */
void
ufmf_writer_make_index_dict(ufmf_writer_t* writer, 
   ufmf_frame_metadata_t* mdata, size_t mdata_num)
{
   /* write a 'd' for dict */
   char d = 'd';
   fwrite(&d, 1, 1, writer->file_ptr);

   /* write the number of keys */
   uint8_t num_keys = 2;
   ufmf_assert(sizeof(num_keys) == 1);
   fwrite(&num_keys, 1, 1, writer->file_ptr);

   /* write index->frame->loc */
   const char loc_string[] = "loc";
   uint16_t loc_string_length = (uint16_t)strlen(loc_string);

   /* write the length of the key */
   ufmf_assert(sizeof(loc_string_length) == 2);
   fwrite(&loc_string_length, 2, 1, writer->file_ptr);

   /* write the key */
   fwrite(loc_string, 1, loc_string_length, writer->file_ptr);

   /* write a for array */
   char a = 'a';
   fwrite(&a, 1, 1, writer->file_ptr);

   /* write the data type */
   char datatype = 'q';
   fwrite(&datatype, 1, 1, writer->file_ptr);

   /* write the number of bytes */
   uint32_t nbytes = (uint32_t)(8 * mdata_num);
   ufmf_assert(sizeof(nbytes) == 4);
   fwrite(&nbytes, 4, 1, writer->file_ptr);

   /* write the array */
   for (size_t i = 0; i < mdata_num; i++) {
      uint64_t loc = mdata[i].pos;
      ufmf_assert(sizeof(loc) == 8);
      fwrite(&loc, 8, 1, writer->file_ptr);
   }

   /* end of index->frame->loc */

   /* write index->frame->timestamp */
   const char timestamp_string[] = "timestamp";
   uint16_t timestamp_string_length = (uint16_t)strlen(timestamp_string);

   /* write the length of the key */
   ufmf_assert(sizeof(timestamp_string_length) == 2);
   fwrite(&timestamp_string_length, 2, 1, writer->file_ptr);

   /* write the key */
   fwrite(timestamp_string, 1, timestamp_string_length, writer->file_ptr);

   /* write a for array */
   fwrite(&a, 1, 1, writer->file_ptr);

   /* write the data type */
   datatype = 'd';
   fwrite(&datatype, 1, 1, writer->file_ptr);

   /* write the number of bytes */
   nbytes = (uint32_t)(8 * mdata_num);
   ufmf_assert(sizeof(nbytes) == 4);
   fwrite(&nbytes, 4, 1, writer->file_ptr);

   /* write the array */
   for (size_t i = 0; i < mdata_num; i++) {
      double timestamp = mdata[i].timestamp;
      ufmf_assert(sizeof(timestamp) == 8);
      fwrite(&timestamp, 8, 1, writer->file_ptr);
   }

   // end index->frame->timestamp
   // end index->frame
}


/**
 * This function retrieves the current file position
 *
 * @param file_ptr File pointer
 * @param file_pos Pointer set to file position 
 * @returns Zero on success, one on failure
 */
int
ufmf_writer_get_file_position(FILE* file_ptr, uint64_t* file_pos)
{
#if defined(_WIN32) || defined(_WIN64)
   int64_t signed_pos = _ftelli64(file_ptr);
   if (signed_pos < 0) {
      return 1;
   } else {
      *file_pos = (uint64_t)signed_pos;
      return 0;
   }
#elif defined(_POSIX_VERSION)
   off_t signed_pos = ftello(file_ptr);
   if (signed_pos < 0) {
      return 1;
   } else {
      *file_pos = (uint64_t)signed_pos;
      return 0;
   }
#else
#error "Unsupported operating system"
#endif
}


/**
 * This function seeks the file to the passed position
 *
 * @param file_ptr File pointer
 * @param file_pos File position 
 * @returns Zero on success, one on failure
 */
int
ufmf_writer_set_file_position(FILE* file_ptr, uint64_t file_pos)
{
#if defined(_WIN32) || defined(_WIN64)
   int64_t signed_pos = (int64_t)file_pos;
   return _fseeki64(file_ptr, signed_pos, SEEK_SET) != 0;
#elif defined(_POSIX_VERSION)
   off_t signed_pos = (off_t)file_pos;
   return fseeko(file_ptr, signed_pos, SEEK_SET) != 0;
#else
#error "Unsupported operating system"
#endif
}


/**
 * This function seeks to the end of the file
 *
 * @param file_ptr File pointer
 * @returns Zero on success, one on failure
 */
int
ufmf_writer_goto_file_end(FILE* file_ptr)
{
#if defined(_WIN32) || defined(_WIN64)
   return _fseeki64(file_ptr, 0, SEEK_END) != 0;
#elif defined(_POSIX_VERSION)
   return fseeko(file_ptr, 0, SEEK_END) != 0;
#else
#error "Unsupported operating system"
#endif
}


/**
 * This function initializes the queue of the writer
 *
 * @param writer Writer
 * @param params Parameters
 * @returns Zero on success, non-zero on failure.
 */
int
ufmf_writer_init_queue(ufmf_writer_t* writer, const ufmf_params_t* params)
{
   size_t idx;
   int abort_alloc = 0;

   writer->queue_length = params->queue_depth;
   writer->queue_data = (ufmf_queue_slot_t*)malloc(writer->queue_length *
     sizeof(ufmf_queue_slot_t));
   if (writer->queue_data == NULL) {
      return 1;
   }

   /* allocate image arrays */
   for (idx = 0; idx < writer->queue_length; idx++) {
      ufmf_queue_slot_t* slot = &writer->queue_data[idx];

      uint8_t* image = (uint8_t*)malloc(((size_t)writer->width)*
        writer->height * sizeof(uint8_t));

      if (image == NULL) {
         abort_alloc = 1;
         break;
      }

      slot->image = image;

      uint8_t* bg_image = (uint8_t*)malloc(((size_t)writer->width)*
        writer->height * sizeof(uint8_t));
      if (bg_image == NULL) {
         free(image);
         abort_alloc = 1;
         break;
      }

      slot->bg_image = bg_image;

      slot->compressor = ufmf_compressor_create(writer->width, writer->height, params);
      if (slot->compressor == NULL) {
         free(image);
         free(bg_image);
         abort_alloc = 1;
         break;
      }

      if (ufmf_sem_init(&slot->ready_to_fill, 1)) {
         free(image);
         free(bg_image);
         ufmf_compressor_destroy(slot->compressor);
         abort_alloc = 1;
         break;
      }

      if (ufmf_sem_init(&slot->ready_to_compress, 0)) {
         free(image);
         free(bg_image);
         ufmf_compressor_destroy(slot->compressor);
         ufmf_sem_destroy(&slot->ready_to_fill);
         abort_alloc = 1;
         break;
      }

      if (ufmf_sem_init(&slot->ready_to_write, 0)) {
         free(image);
         free(bg_image);
         ufmf_compressor_destroy(slot->compressor);
         ufmf_sem_destroy(&slot->ready_to_fill);
         ufmf_sem_destroy(&slot->ready_to_compress);
         abort_alloc = 1;
         break;
      }

      slot->poison_pill = 0;
      slot->is_garbage = 0;
      slot->output_bg = 0;
      slot->timestamp = 0.0;
   }

   if (abort_alloc) {
      for (size_t cidx = 0; cidx < idx; cidx++) {
         ufmf_queue_slot_t* cslot = &writer->queue_data[cidx];
         free(cslot->image);
         free(cslot->bg_image);
         ufmf_compressor_destroy(cslot->compressor);
         ufmf_sem_destroy(&cslot->ready_to_fill);
         ufmf_sem_destroy(&cslot->ready_to_compress);
         ufmf_sem_destroy(&cslot->ready_to_write);
      }

      free(writer->queue_data);

      return 1;
   }
 
   return 0;
}


/**
 * This function destroys the queue of the writer
 *
 * @param writer Writer
 * @returns Zero on success, non-zero on failure.
 */
void
ufmf_writer_free_queue(ufmf_writer_t* writer)
{
   for (size_t idx = 0; idx < writer->queue_length; idx++) {
      ufmf_queue_slot_t* slot = &writer->queue_data[idx];
      free(slot->image);
      free(slot->bg_image);
      ufmf_compressor_destroy(slot->compressor);
      ufmf_sem_destroy(&slot->ready_to_fill);
      ufmf_sem_destroy(&slot->ready_to_compress);
      ufmf_sem_destroy(&slot->ready_to_write);
   }

   free(writer->queue_data);
}

