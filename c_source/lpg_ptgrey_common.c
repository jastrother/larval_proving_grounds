/*
 * This file contains common functions used by multiple 
 * routines in the lpg_ptgrey suite.
 *
 * This function can be compiled with the following command
 * mex -v -largeArrayDims -L'C:\Program Files\Point Grey Research\FlyCapture2\lib64\vs2015' lpg_ptgrey_common.c ufmf.c  -lFlyCapture2_C_v140
 */

#if defined(_WIN32) || defined(_WIN64)
#define _CRT_SECURE_NO_WARNINGS
#else
#error "Unsupported operating system"
#endif

#include "FlyCapture2_C.h"
#include "ufmf.h"
#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <string.h>

#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#elif defined(_POSIX_VERSION)
#include <pthread.h>
#else
#error "Unsupported operating system"
#endif


/**
 * Define type used for thread
 */
#if defined(_WIN32) || defined(_WIN64)
typedef HANDLE lpg_ptgrey_thread_t;
#elif defined(_POSIX_VERSION)
typedef pthread_t lpg_ptgrey_thread_t;
#else
#error "Unsupported operating system"
#endif


/**
 * Define type used for mutex
 */
#if defined(_WIN32) || defined(_WIN64)
typedef HANDLE lpg_ptgrey_mutex_t;
#elif defined(_POSIX_VERSION)
typedef pthread_mutex_t lpg_ptgrey_mutex_t;
#else
#error "Unsupported operating system"
#endif


/* output types */
enum {
   LPG_OUTPUT_TYPE_NONE,
   LPG_OUTPUT_TYPE_UFMF
};

/* configuration for camera */
typedef struct {
   /* device id */
   unsigned int dev_id; 

   /* device specific mode */
   int set_mode;
   unsigned mode;

   /* offsetX in pixels */
   int set_offsetX;
   unsigned offsetX;

   /* offsetY in pixels */
   int set_offsetY;
   unsigned offsetY;

   /* width in pixels */
   int set_width;
   unsigned width;

   /* height in pixels */
   int set_height;
   unsigned height;

   /* frame rate in percent max */
   int set_framerate;
   double framerate;

   /* pixel format */
   int set_pixel_format;
   int pixel_format;

   /* frames per capture */
   int set_frames_per_capture;
   long frames_per_capture;

   /* enable auto gain mode */
   int set_gain_auto;
   int gain_auto;

   /* gain value if not auto */
   int set_gain_value;
   double gain_value;

   /* enable auto shutter mode */
   int set_shutter_auto;
   int shutter_auto;

   /* shutter value if not auto */
   int set_shutter_value;
   double shutter_value;

   /* start in paused state */
   int set_start_paused;
   int start_paused;

   /* trigger enabled */
   int set_trigger_enabled;
   int trigger_enabled;

   /* trigger mode (see manual) */
   int set_trigger_mode;
   unsigned trigger_mode;

   /* trigger mode (gpio pin number) */
   int set_trigger_source;
   unsigned trigger_source;

   /* trigger polarity (0 - active low, 1 - active high) */
   int set_trigger_polarity;
   int trigger_polarity;

   /* strobe enabled */
   int set_strobe_enabled;
   int strobe_enabled;

   /* strobe source (gpio pin number */
   int set_strobe_source;
   unsigned strobe_source;

   /* strobe polarity (0 - low, 1 - high) */
   int set_strobe_polarity;
   int strobe_polarity;

   /* output file */
   int set_output_file;
   char* output_file;

   /* output type */
   int set_output_type;
   int output_type;

   /* callback data */
   int set_callback_fcn;
   mxArray* callback_fcn;

   int set_callback_period;
   unsigned long callback_period;
} lpg_ptgrey_camera_config_t;


/* state information for camera */
typedef struct {
   /* configuration info */
   lpg_ptgrey_camera_config_t config;

   /* pause the camera */
   int is_paused;

   /* driver context */
   fc2Context fc_context;
} lpg_ptgrey_camera_t;


/* configuration for context */
typedef struct {
   int empty;
} lpg_ptgrey_context_config_t;


/* image data for callback */
typedef struct {
   unsigned cam_idx;
   size_t width;
   size_t height;
   int pixel_format;
   unsigned char* pixels;
} lpg_ptgrey_callback_image_t;


/* number of entries in callback buffer */
#define LPG_PTGREY_DEFAULT_CALLBACK_BUF_LEN 8


/* buffer for callback images */
typedef struct {
   size_t next_writable_image;
   size_t num_readable_images;
   size_t next_readable_image;
   size_t buf_len;
   lpg_ptgrey_callback_image_t* images;
} lpg_ptgrey_callback_buffer_t;


/* state information for camera */
typedef struct {
   /* global configuration information */
   lpg_ptgrey_context_config_t config;

   /* number of cameras */
   unsigned int num_cameras;

   /* data for each camera, owned by context, must be freed
    * when context is destroyed
    */
   lpg_ptgrey_camera_t* cameras;

   /* if currently running */
   int is_running;

   /* if start_time is set */
   int has_start_time;

   /* start time for capture */
   fc2TimeStamp start_time;

   /* callback buffers */
   lpg_ptgrey_callback_buffer_t callback_buffer;

   /* threads information */
   lpg_ptgrey_thread_t* threads;
   int* threads_is_done;
   lpg_ptgrey_mutex_t mutex;

   int halt;

   int has_error_msg;
   char* error_msg;
} lpg_ptgrey_context_t;


/* function prototypes */
int lpg_ptgrey_mutex_init(lpg_ptgrey_mutex_t* mutex);
void lpg_ptgrey_mutex_lock(lpg_ptgrey_mutex_t* mutex);
void lpg_ptgrey_mutex_unlock(lpg_ptgrey_mutex_t* mutex);
void lpg_ptgrey_mutex_destroy(lpg_ptgrey_mutex_t* mutex);

const char* lpg_ptgrey_fc2error_to_string(int id);
void lpg_ptgrey_fc2error(const char* function_name, int id);
void lpg_ptgrey_initialize_camera(lpg_ptgrey_camera_t* camera);
void lpg_ptgrey_destroy_camera(lpg_ptgrey_camera_t* camera);
void lpg_ptgrey_initialize_callback_buffer(lpg_ptgrey_callback_buffer_t* buffer);
void lpg_ptgrey_destroy_callback_buffer(lpg_ptgrey_callback_buffer_t* buffer);
int lpg_ptgrey_pixel_format_props(int pixel_format, int* bytes_per_pixel);
int lpg_ptgrey_append_to_callback_buffer(lpg_ptgrey_callback_buffer_t* buffer,
   unsigned cam_idx, size_t width, size_t height, int pixel_format, unsigned char* pixels);
int lpg_ptgrey_retrieve_from_callback_buffer(lpg_ptgrey_callback_buffer_t* buffer,
   lpg_ptgrey_callback_image_t* image);
void lpg_ptgrey_initialize_context(lpg_ptgrey_context_t* context);
void lpg_ptgrey_destroy_context(lpg_ptgrey_context_t* context);
void lpg_ptgrey_realize_context(lpg_ptgrey_context_t* context);
void lpg_ptgrey_spin(void);
void lpg_ptgrey_start_camera_capture(lpg_ptgrey_context_t* context, unsigned cam_idx);
void lpg_ptgrey_start_capture(lpg_ptgrey_context_t* context, unsigned ctxt_id);
void lpg_ptgrey_stop_capture(lpg_ptgrey_context_t* context);
void lpg_ptgrey_start_slow_stop_capture(lpg_ptgrey_context_t* context);
mxArray* lpg_ptgrey_slow_stop_capture_is_done(lpg_ptgrey_context_t* context);
void lpg_ptgrey_finish_slow_stop_capture(lpg_ptgrey_context_t* context);
mxArray* lpg_ptgrey_convert_pixels_to_mxarray(const lpg_ptgrey_callback_image_t* im);
mxArray* lpg_ptgrey_poll_callbacks(lpg_ptgrey_context_t* context);
void lpg_ptgrey_unpause(lpg_ptgrey_context_t* context, unsigned cam_idx);
void lpg_ptgrey_reset(void);

int lpg_ptgrey_get_context(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[],
   unsigned int* ctxt_idx_output, lpg_ptgrey_context_t** ctxt_ptr_output);
int lpg_ptgrey_parse_boolean(mxArray* mx_val, int* set, int* out);
int lpg_ptgrey_parse_signed_int(mxArray* mx_val, int* set, int* out);
int lpg_ptgrey_parse_unsigned_int(mxArray* mx_val, int* set, unsigned int* out);
int lpg_ptgrey_parse_signed_long(mxArray* mx_val, int* set, long* out);
int lpg_ptgrey_parse_unsigned_long(mxArray* mx_val, int* set, unsigned long* out);

int lpg_ptgrey_parse_unsigned_double(mxArray* mx_val, int* set, double* out);
int lpg_ptgrey_parse_camera_config(const mxArray* mx_struct, unsigned struct_idx,
   lpg_ptgrey_camera_config_t* config, char** field_str);
void lpg_ptgrey_cmd_create_context(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void lpg_ptgrey_cmd_destroy_context(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void lpg_ptgrey_cmd_start_capture(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void lpg_ptgrey_cmd_stop_capture(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void lpg_ptgrey_cmd_start_slow_stop_capture(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void lpg_ptgrey_cmd_slow_stop_capture_is_done(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void lpg_ptgrey_cmd_finish_slow_stop_capture(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void lpg_ptgrey_cmd_poll_callbacks(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void lpg_ptgrey_cmd_unpause(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void lpg_ptgrey_cmd_reset(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);


/* create a global contexts array */
#define LPG_PTGREY_MAX_CONTEXTS 128
static int lpg_ptgrey_global_contexts_is_init = 0;
static lpg_ptgrey_context_t* lpg_ptgrey_global_contexts[LPG_PTGREY_MAX_CONTEXTS];


/**
 * This function initializes the thread mutex
 *
 * @param mutex Mutex to initialize
 * @returns Zero on success, non-zero on failure.
 */
int
lpg_ptgrey_mutex_init(lpg_ptgrey_mutex_t* mutex)
{
#if defined(_WIN32) || defined(_WIN64)
   *mutex = CreateMutex(NULL, FALSE, NULL);
   return (*mutex == NULL);
#elif defined(_POSIX_VERSION)
   return pthread_mutex_init(mutex, NULL);
#else
#error "Unsupported operating system"
#endif
}


/**
 * This function locks the thread mutex
 *
 * @param mutex Mutex to lock
 * @returns Zero on success, non-zero on failure.
 */
void
lpg_ptgrey_mutex_lock(lpg_ptgrey_mutex_t* mutex)
{
#if defined(_WIN32) || defined(_WIN64)
   WaitForSingleObject(*mutex, INFINITE);
#elif defined(_POSIX_VERSION)
   int retval = pthread_mutex_lock(mutex);
   if (retval != 0) {
      abort();
   }
#else
#error "Unsupported operating system"
#endif
}


/**
 * This function unlocks the thread mutex
 *
 * @param mutex Mutex to unlock
 * @returns Zero on success, non-zero on failure.
 */
void
lpg_ptgrey_mutex_unlock(lpg_ptgrey_mutex_t* mutex)
{
#if defined(_WIN32) || defined(_WIN64)
   ReleaseMutex(*mutex);
#elif defined(_POSIX_VERSION)
   int retval = pthread_mutex_unlock(mutex);
   if (retval != 0) {
      abort();
   }
#else
#error "Unsupported operating system"
#endif
}


/**
 * This function destroys the thread mutex
 *
 * @param mutex Mutex to destroy
 * @returns Zero on success, non-zero on failure.
 */
void
lpg_ptgrey_mutex_destroy(lpg_ptgrey_mutex_t* mutex)
{
#if defined(_WIN32) || defined(_WIN64)
   CloseHandle(*mutex);
#elif defined(_POSIX_VERSION)
   pthread_mutex_destroy(mutex);
#else
#error "Unsupported operating system"
#endif
}


/* The following function converts a fly capture error message into
 * a string.
 *
 * Inputs:
 * id - FC2 error ID number
 *
 * Returns:
 * Constant string describing error.
 */
const char*
lpg_ptgrey_fc2error_to_string(int id)
{
   const char* str;

   switch (id) {
      case FC2_ERROR_UNDEFINED: str = "Undefined."; break;
      case FC2_ERROR_OK: str = "Function returned with no errors."; break;
      case FC2_ERROR_FAILED: str = "General failure."; break;
      case FC2_ERROR_NOT_IMPLEMENTED: str = "Function has not been implemented."; break;
      case FC2_ERROR_FAILED_BUS_MASTER_CONNECTION: str = "Could not connect to Bus Master."; break;
      case FC2_ERROR_NOT_CONNECTED: str = "Camera has not been connected."; break;
      case FC2_ERROR_INIT_FAILED: str = "Initialization failed."; break;
      case FC2_ERROR_NOT_INTITIALIZED: str = "Camera has not been initialized."; break;
      case FC2_ERROR_INVALID_PARAMETER: str = "Invalid parameter passed to function."; break;
      case FC2_ERROR_INVALID_SETTINGS: str = "Setting set to camera is invalid."; break;
      case FC2_ERROR_INVALID_BUS_MANAGER: str = "Invalid Bus Manager object."; break;
      case FC2_ERROR_MEMORY_ALLOCATION_FAILED: str = "Could not allocate memory."; break;
      case FC2_ERROR_LOW_LEVEL_FAILURE: str = "Low level error."; break;
      case FC2_ERROR_NOT_FOUND: str = "Device not found."; break;
      case FC2_ERROR_FAILED_GUID: str = "GUID failure."; break;
      case FC2_ERROR_INVALID_PACKET_SIZE: str = "Packet size set to camera is invalid."; break;
      case FC2_ERROR_INVALID_MODE: str = "Invalid mode has been passed to function."; break;
      case FC2_ERROR_NOT_IN_FORMAT7: str = "Error due to not being in Format7."; break;
      case FC2_ERROR_NOT_SUPPORTED: str = "This feature is unsupported."; break;
      case FC2_ERROR_TIMEOUT: str = "Timeout error."; break;
      case FC2_ERROR_BUS_MASTER_FAILED: str = "Bus Master Failure."; break;
      case FC2_ERROR_INVALID_GENERATION: str = "Generation Count Mismatch."; break;
      case FC2_ERROR_LUT_FAILED: str = "Look Up Table failure."; break;
      case FC2_ERROR_IIDC_FAILED: str = "IIDC failure."; break;
      case FC2_ERROR_STROBE_FAILED: str = "Strobe failure."; break;
      case FC2_ERROR_TRIGGER_FAILED: str = "Trigger failure."; break;
      case FC2_ERROR_PROPERTY_FAILED: str = "Property failure."; break;
      case FC2_ERROR_PROPERTY_NOT_PRESENT: str = "Property is not present"; break;
      case FC2_ERROR_REGISTER_FAILED: str = "Register access failed."; break;
      case FC2_ERROR_READ_REGISTER_FAILED: str = "Register read failed."; break;
      case FC2_ERROR_WRITE_REGISTER_FAILED: str = "Register write failed."; break;
      case FC2_ERROR_ISOCH_FAILED: str = "Isochronous failure."; break;
      case FC2_ERROR_ISOCH_ALREADY_STARTED: str = "Isochronous transfer has already been started."; break;
      case FC2_ERROR_ISOCH_NOT_STARTED: str = "Isochronous transfer has not been started."; break;
      case FC2_ERROR_ISOCH_START_FAILED: str = "Isochronous start failed."; break;
      case FC2_ERROR_ISOCH_RETRIEVE_BUFFER_FAILED: str = "Isochronous retrieve buffer failed."; break;
      case FC2_ERROR_ISOCH_STOP_FAILED: str = "Isochronous stop failed."; break;
      case FC2_ERROR_ISOCH_SYNC_FAILED: str = "Isochronous image synchronization failed."; break;
      case FC2_ERROR_ISOCH_BANDWIDTH_EXCEEDED: str = "Isochronous bandwidth exceeded."; break;
      case FC2_ERROR_IMAGE_CONVERSION_FAILED: str = "Image conversion failed."; break;
      case FC2_ERROR_IMAGE_LIBRARY_FAILURE: str = "Image library failure."; break;
      case FC2_ERROR_BUFFER_TOO_SMALL: str = "Buffer is too small."; break;
      case FC2_ERROR_IMAGE_CONSISTENCY_ERROR: str = "There is an image consistency error."; break;
      case FC2_ERROR_INCOMPATIBLE_DRIVER: str = "The installed driver is not compatible with the library."; break;
      default: str = "Unrecognized error.";
   }

   return str;
}


/* The following function logs a fly cap generated error message with
 * matlab. This function does not return, so any required cleanup must be done
 * prior to invoking this function.
 *
 * Inputs:
 * function_name - function that generated error
 * id - error identifier
 */
void
lpg_ptgrey_fc2error(const char* function_name, int id)
{
   size_t error_buflen = 1024;
   char error_buf[1024];

   snprintf(error_buf, error_buflen, "Error in %s: %s", 
      function_name, lpg_ptgrey_fc2error_to_string(id));
   mexErrMsgTxt(error_buf);
}



/* The following function initializes the passed camera.
 * This function must be matched to lpg_ptgrey_destroy_camera
 * to ensure that the context can be destroyed at any point.
 *
 * Inputs:
 * camera - Pointer to camera to initialize
 */
void
lpg_ptgrey_initialize_camera(lpg_ptgrey_camera_t* camera)
{
   memset(&camera->config, 0, sizeof(lpg_ptgrey_camera_config_t));
   camera->is_paused = 0;
   camera->fc_context = NULL;
}


/* The following function destroys the passed camera, and
 * releases any internal memory associated with the camera.
 * It does not attempt to free the camera pointer itself.
 *
 * Inputs:
 * camera - Pointer to camera to destroy
 */
void
lpg_ptgrey_destroy_camera(lpg_ptgrey_camera_t* camera)
{
   if (camera->config.set_output_file &&
      camera->config.output_file != NULL) {
      free(camera->config.output_file);
   }

   if (camera->config.set_callback_fcn) {
      mxDestroyArray(camera->config.callback_fcn);
   }

   if (camera->fc_context != NULL) {
      fc2DestroyContext(camera->fc_context);
   }
}


/* The following function initializes the passed callback buffer array.
 * This function must be matched to lpg_ptgrey_destroy_callback_buffer
 * to ensure that the buffer can be destroyed at any point.
 *
 * Inputs:
 * buffer - Pointer to buffer to initialize
 */
void
lpg_ptgrey_initialize_callback_buffer(lpg_ptgrey_callback_buffer_t*  buffer)
{
   buffer->next_writable_image = 0;
   buffer->num_readable_images = 0;
   buffer->next_readable_image = 0;
   buffer->buf_len = 0;
   buffer->images = NULL;
}


/* The following function destroys the passed callback buffer, and
 * releases any internal memory associated with the camera.
 * It does not attempt to free the buffer pointer itself.
 *
 * Inputs:
 * buffer - Pointer to buffer to destroy
 */
void
lpg_ptgrey_destroy_callback_buffer(lpg_ptgrey_callback_buffer_t* buffer)
{
   if (buffer->buf_len != 0 && buffer->images != NULL) {
      size_t image_idx;

      for (image_idx = 0; image_idx < buffer->buf_len; image_idx++) {
         unsigned char* pixels = buffer->images[image_idx].pixels;

         if (pixels != NULL) {
            free(pixels);
         }
      }

      free(buffer->images);
   }
}


/* The following function calculates the number of bytes in each pixel
 * of an image with the given pixel format.
 *
 * Input:
 * pixel_format - FC2 pixel format enum
 * bytes_per_pixel - Pointer overwritten with bytes per pixel
 *
 * Returns:
 * Zero on success, one on error
 */
int
lpg_ptgrey_pixel_format_props(int pixel_format, int* bytes_per_pixel)
{
   switch (pixel_format) {
      case FC2_PIXEL_FORMAT_MONO8:
      case FC2_PIXEL_FORMAT_RAW8:
         *bytes_per_pixel = 1;
         break;
      default:
         return 1;
   }

   return 0;
}


/* The following function appends an image to the callback buffer.
 *
 * Inputs:
 * buffer - Buffer to append to
 * cam_idx - Camera generating image
 * width - Width of image being appended
 * height - Height of image being appended
 * pixel_format - FC2 pixel format of image being appended
 * pixels - Pixel data of image being appended, this is copied so
 *   the passed pointer is available once this function returns
 *
 * Returns:
 * Zero on success, one on error.
 */
int
lpg_ptgrey_append_to_callback_buffer(lpg_ptgrey_callback_buffer_t* buffer,
  unsigned cam_idx, size_t width, size_t height, int pixel_format, unsigned char* pixels)
{
   lpg_ptgrey_callback_image_t* im;
   int bytes_per_pixel;

   if (buffer->buf_len == 0) {
      size_t image_idx;

      buffer->buf_len = LPG_PTGREY_DEFAULT_CALLBACK_BUF_LEN;

      buffer->images = (lpg_ptgrey_callback_image_t*)malloc(
         buffer->buf_len * sizeof(lpg_ptgrey_callback_image_t));
      if (buffer->images == NULL) {
         return 1;
      }

      for (image_idx = 0; image_idx < buffer->buf_len; image_idx++) {
         buffer->images[image_idx].pixels = NULL;
      }
   }

   if (lpg_ptgrey_pixel_format_props(pixel_format, &bytes_per_pixel)) {
      return 1;
   }

   /* copy image into our buffer */
   im = &buffer->images[buffer->next_writable_image];
   im->cam_idx = cam_idx;
   im->width = width;
   im->height = height;
   im->pixel_format = pixel_format;
   im->pixels = (unsigned char*)realloc(im->pixels,
      bytes_per_pixel*width*height*sizeof(unsigned char));
   if (im->pixels == NULL) {
      return 1;
   }

   memcpy(im->pixels, pixels, bytes_per_pixel*width*height*sizeof(unsigned char));

   /* check if we buffer overflowed and we need to adjust next readable */
   if (buffer->num_readable_images > 0 &&
      buffer->next_writable_image == buffer->next_readable_image) {
      buffer->next_readable_image = (buffer->next_readable_image + 1) % buffer->buf_len;
      buffer->num_readable_images--;
   }

   /* adjust next writable */
   buffer->next_writable_image = (buffer->next_writable_image + 1) % buffer->buf_len;
   buffer->num_readable_images++;

   return 0;
}


/* This function retrieves an image from the callback buffer. The caller should
 * have the context mutex before calling this function and should use the image
 * data before releasing the mutex, since it may be overwritten the next time that
 * append_to_callback_buffer is invoked.
 *
 * Inputs:
 * buffer - Buffer to query
 * image - Pointer overwritten with image data
 *
 * Returns:
 * A value of 0 if image successfully retrieve, -1 if no image available,
 * and 1 if an error occurred.
 */
int
lpg_ptgrey_retrieve_from_callback_buffer(lpg_ptgrey_callback_buffer_t* buffer,
   lpg_ptgrey_callback_image_t* image)
{
   lpg_ptgrey_callback_image_t* buf_im;

   if (buffer->num_readable_images == 0) {
      return -1;
   }

   buf_im = &buffer->images[buffer->next_readable_image];
   memcpy(image, buf_im, sizeof(lpg_ptgrey_callback_image_t));

   buffer->next_readable_image = (buffer->next_readable_image + 1) % buffer->buf_len;
   buffer->num_readable_images--;

   return 0;
}


/* The following function initializes the passed context.
 * This function must be matched to lpg_ptgrey_destroy_context
 * to ensure that the context can be destroyed at any point.
 *
 * Inputs:
 * context - Pointer to context to initialize
 */
void
lpg_ptgrey_initialize_context(lpg_ptgrey_context_t* context)
{
   context->cameras = NULL;
   context->is_running = 0;
   context->has_start_time = 0;
   
   lpg_ptgrey_initialize_callback_buffer(&context->callback_buffer);

   context->threads = NULL;
   context->threads_is_done = NULL;
   lpg_ptgrey_mutex_init(&context->mutex);

   context->halt = 0;
   context->has_error_msg = 0;
   context->error_msg = NULL;
}


/* The following function destroys the passed context, and 
 * releases any internal memory associated with the context.
 * The context itself will be freed after resources are freed.
 * The passed context must be in a self-consistent multi-threaded
 * state. In other words, if context->threads is non-NULL then
 * all camera threads must be running.
 *
 * Inputs:
 * context - Pointer to context to destroy
 */
void
lpg_ptgrey_destroy_context(lpg_ptgrey_context_t* context)
{
   /* stop any running threads */
   if (context->threads != NULL) {
      unsigned thrd_idx;

      lpg_ptgrey_mutex_lock(&context->mutex);
      context->halt = 1;
      lpg_ptgrey_mutex_unlock(&context->mutex);

#if defined(_WIN32) || defined(_WIN64)
      for (thrd_idx = 0; thrd_idx < context->num_cameras; thrd_idx++) {
         if (!context->threads_is_done[thrd_idx]) {
            WaitForSingleObject(context->threads[thrd_idx], INFINITE);
         }
      }

      for (thrd_idx = 0; thrd_idx < context->num_cameras; thrd_idx++) {
         CloseHandle(context->threads[thrd_idx]);
      }

#else
#error "Unsupported operating system"
#endif

      free(context->threads);
      free(context->threads_is_done);
   }


   lpg_ptgrey_destroy_callback_buffer(&context->callback_buffer);

   lpg_ptgrey_mutex_destroy(&context->mutex);

   if (context->cameras != NULL) {
      unsigned int cam_idx;

      for (cam_idx = 0; cam_idx < context->num_cameras; cam_idx++) {
         lpg_ptgrey_destroy_camera(&context->cameras[cam_idx]);
      }

      free(context->cameras);
   }

   if (context->has_error_msg) {
      free(context->error_msg);
   }

   free(context);
}


/* The following function configures the point grey cameras according
 * to the included configuration information.
 *
 * Inputs:
 * context - Context to configure
 */
void
lpg_ptgrey_realize_context(lpg_ptgrey_context_t* context)
{
   fc2Error fc_error;
   unsigned int cam_idx;

   for (cam_idx = 0; cam_idx < context->num_cameras; cam_idx++) {
      lpg_ptgrey_camera_config_t* config;
      fc2Context fc_context;
      fc2PGRGuid fc_guid;
      fc2Config fc_config;
      fc2Format7Info fc_format7_info;
      fc2Format7ImageSettings fc_format7_settings;
      fc2Format7PacketInfo fc_packet_info;
      BOOL format7_supported;
      fc2TriggerMode fc_trig_mode;
      fc2Property fc_prop;
      fc2EmbeddedImageInfo fc_eminfo;

      config = &context->cameras[cam_idx].config;

      fc_error = fc2CreateContext(&fc_context);
      if (fc_error != FC2_ERROR_OK) {
         lpg_ptgrey_destroy_context(context);
         lpg_ptgrey_fc2error("fc2CreateContext", fc_error);
         return;
      }

      context->cameras[cam_idx].fc_context = fc_context;

      fc_error = fc2GetCameraFromIndex(fc_context,
         config->dev_id, &fc_guid);
      if (fc_error != FC2_ERROR_OK) {
         lpg_ptgrey_destroy_context(context);
         lpg_ptgrey_fc2error("fc2GetCameraFromIndex", fc_error);
         return;
      }

      fc_error = fc2Connect(fc_context, &fc_guid);
      if (fc_error != FC2_ERROR_OK) {
         lpg_ptgrey_destroy_context(context);
         lpg_ptgrey_fc2error("fc2Connect", fc_error);
         return;
      }

      fc_error = fc2GetConfiguration(fc_context, &fc_config);
      if (fc_error != FC2_ERROR_OK) {
         lpg_ptgrey_destroy_context(context);
         lpg_ptgrey_fc2error("fc2GetConfiguration", fc_error);
         return;
      }

      fc_config.numBuffers = 32; /* arbitrary but reasonable */
      fc_config.grabMode = FC2_BUFFER_FRAMES;
      fc_config.highPerformanceRetrieveBuffer = 1;

      fc_error = fc2SetConfiguration(fc_context, &fc_config);
      if (fc_error != FC2_ERROR_OK) {
         lpg_ptgrey_destroy_context(context);
         lpg_ptgrey_fc2error("fc2SetConfiguration", fc_error);
         return;
      }

      /* assign default values */
      if (!config->set_mode) {
         config->set_mode = 1;
         config->mode = 0;
      }

      fc_format7_info.mode = config->mode;

      fc_error = fc2GetFormat7Info(fc_context, &fc_format7_info,
         &format7_supported);
      if (fc_error != FC2_ERROR_OK) {
         lpg_ptgrey_destroy_context(context);
         lpg_ptgrey_fc2error("fc2GetFormat7Info", fc_error);
         return;
      }
 
      if (!format7_supported) {
         lpg_ptgrey_destroy_context(context);
         mexErrMsgTxt("Camera does not support required functionality (format 7)");
         return;
      }      

      /* assign default values */
      if (!config->set_offsetX) {
         config->set_offsetX = 1;
         config->offsetX = 0;
      }

      if (!config->set_offsetY) {
         config->set_offsetY = 1;
         config->offsetY = 0;
      }

      if (!config->set_width) {
         config->set_width = 1;
         config->width = fc_format7_info.maxWidth;         
      }

      if (!config->set_height) {
         config->set_height = 1;
         config->height = fc_format7_info.maxHeight;
      }

      if (!config->set_framerate) {
         config->framerate = 100.0;
      }

      if (!config->set_pixel_format) {
         config->set_pixel_format = 1;
         config->pixel_format = FC2_PIXEL_FORMAT_MONO8;
      }

      if (!config->set_frames_per_capture) {
         config->set_frames_per_capture = 1;
         config->frames_per_capture = -1;
      }

      if (!config->set_gain_auto) {
         config->set_gain_auto = 1;
         config->gain_auto = 1;
      }

      if (!config->set_gain_value) {
         config->set_gain_value = 1;
         config->gain_value = 0;
      }

      if (!config->set_shutter_auto) {
         config->set_shutter_auto = 1;
         config->shutter_auto = 1;
      }

      if (!config->set_shutter_value) {
         config->set_shutter_value = 1;
         config->shutter_value = 10;
      }

      if (!config->set_start_paused) {
         config->set_start_paused = 1;
         config->start_paused = 0;
      }

      if (!config->set_trigger_enabled) {
         config->set_trigger_enabled = 1;
         config->trigger_enabled = 0;
      }

      if (!config->set_trigger_mode) {
         config->set_trigger_mode = 1;
         config->trigger_mode = 0;
      }

      if (!config->set_trigger_source) {
         config->set_trigger_source = 1;
         config->trigger_source = 0;
      }

      if (!config->set_trigger_polarity) {
         config->set_trigger_polarity = 1;
         config->trigger_polarity = 1;
      }

      if (!config->set_strobe_enabled) {
         config->set_strobe_enabled = 1;
         config->strobe_enabled = 0;
      }

      if (!config->set_strobe_source) {
         config->set_strobe_source = 1;
         config->strobe_source = 1;
      }

      if (!config->set_strobe_polarity) {
         config->set_strobe_polarity = 1;
         config->strobe_polarity = 1;
      }

      if (!config->set_callback_period) {
         config->set_callback_period = 1;
         config->callback_period = 1;
      }

      context->cameras[cam_idx].is_paused = config->start_paused;

      /* set format7 settings */
      memset(&fc_format7_settings, 0, sizeof(fc_format7_settings));
      fc_format7_settings.mode = config->mode;
      fc_format7_settings.offsetX = config->offsetX;
      fc_format7_settings.offsetY = config->offsetY;
      fc_format7_settings.width = config->width;
      fc_format7_settings.height = config->height;
      fc_format7_settings.pixelFormat = config->pixel_format;

      fc_error = fc2ValidateFormat7Settings(fc_context, &fc_format7_settings,
         &format7_supported, &fc_packet_info);
      if (fc_error != FC2_ERROR_OK) {
         lpg_ptgrey_destroy_context(context);
         lpg_ptgrey_fc2error("fc2ValidateFormat7Settings", fc_error);
      }

      if (!format7_supported) {
         lpg_ptgrey_destroy_context(context);
         mexErrMsgTxt("Camera does not supported specified parameters");
         return;
      }

      fc_error = fc2SetFormat7Configuration(fc_context, &fc_format7_settings,
         (float)config->framerate);
      if (fc_error != FC2_ERROR_OK) {
         lpg_ptgrey_destroy_context(context);
         lpg_ptgrey_fc2error("fc2SetFormat7Configuration", fc_error);
      }

      /* set trigger settings */
      memset(&fc_trig_mode, 0, sizeof(fc_trig_mode));
      fc_trig_mode.onOff = config->trigger_enabled;
      fc_trig_mode.polarity = config->trigger_polarity;
      fc_trig_mode.source = config->trigger_source;
      fc_trig_mode.mode = config->trigger_mode;
      fc_trig_mode.parameter = 0;

      fc_error = fc2SetTriggerMode(fc_context, &fc_trig_mode);
      if (fc_error != FC2_ERROR_OK) {
         lpg_ptgrey_destroy_context(context);
         lpg_ptgrey_fc2error("fc2SetTriggerMode", fc_error);
      }
   
      /* set strobe settings */
      fc2StrobeControl fc_strobe_ctrl;

      memset(&fc_strobe_ctrl, 0, sizeof(fc_strobe_ctrl));
      fc_strobe_ctrl.source = config->strobe_source;
      fc_strobe_ctrl.onOff = config->strobe_enabled;
      fc_strobe_ctrl.polarity = config->strobe_polarity;
      fc_strobe_ctrl.delay = 0;
      fc_strobe_ctrl.duration = 0.5; /* arbitrary */

      fc_error = fc2SetStrobe(fc_context, &fc_strobe_ctrl);
      if (fc_error != FC2_ERROR_OK) {
         lpg_ptgrey_destroy_context(context);
         lpg_ptgrey_fc2error("fc2SetStrobe", fc_error);
      }

      /* set image processing settings */
      memset(&fc_prop, 0, sizeof(fc2Property));
      fc_prop.type = FC2_BRIGHTNESS;
      fc_prop.absControl = 0;
      fc_prop.onePush = 0;
      fc_prop.onOff = 0;
      fc_prop.autoManualMode = 0;
      fc_prop.absValue = 0;
      fc_error = fc2SetProperty(fc_context, &fc_prop);
      if (fc_error != FC2_ERROR_OK) {
         lpg_ptgrey_destroy_context(context);
         lpg_ptgrey_fc2error("fc2SetProperty:BRIGHTNESS", fc_error);
      }

      memset(&fc_prop, 0, sizeof(fc2Property));
      fc_prop.type = FC2_AUTO_EXPOSURE;
      fc_prop.onOff = 0;
      fc_error = fc2SetProperty(fc_context, &fc_prop);
      if (fc_error != FC2_ERROR_OK) {
         lpg_ptgrey_destroy_context(context);
         lpg_ptgrey_fc2error("fc2SetProperty:AUTO_EXPOSURE", fc_error);
      }

      memset(&fc_prop, 0, sizeof(fc2Property));
      fc_prop.type = FC2_SHARPNESS;
      fc_prop.onOff = 0;
      fc_error = fc2SetProperty(fc_context, &fc_prop);
      if (fc_error != FC2_ERROR_OK) {
         lpg_ptgrey_destroy_context(context);
         lpg_ptgrey_fc2error("fc2SetProperty:SHARPNESS", fc_error);
      }

      memset(&fc_prop, 0, sizeof(fc2Property));
      fc_prop.type = FC2_GAMMA;
      fc_prop.onOff = 0;
      fc_error = fc2SetProperty(fc_context, &fc_prop);
      if (fc_error != FC2_ERROR_OK) {
         lpg_ptgrey_destroy_context(context);
         lpg_ptgrey_fc2error("fc2SetProperty:GAMMA", fc_error);
      }

      memset(&fc_prop, 0, sizeof(fc2Property));
      fc_prop.type = FC2_FRAME_RATE;
      fc_prop.onOff = 0;
      fc_error = fc2SetProperty(fc_context, &fc_prop);
      if (fc_error != FC2_ERROR_OK) {
         lpg_ptgrey_destroy_context(context);
         lpg_ptgrey_fc2error("fc2SetProperty:FRAME_RATE", fc_error);
      }

      /* set gain and shutter settings */
      memset(&fc_prop, 0, sizeof(fc2Property));
      fc_prop.type = FC2_GAIN;
      fc_prop.absControl = 1;
      fc_prop.onePush = 0;
      fc_prop.onOff = 1;
      fc_prop.autoManualMode = config->gain_auto;
      fc_prop.absValue = (float)config->gain_value;
      fc_error = fc2SetProperty(fc_context, &fc_prop);
      if (fc_error != FC2_ERROR_OK) {
         lpg_ptgrey_destroy_context(context);
         lpg_ptgrey_fc2error("fc2SetProperty:GAIN", fc_error);
      }

      memset(&fc_prop, 0, sizeof(fc2Property));
      fc_prop.type = FC2_SHUTTER;
      fc_prop.absControl = 1;
      fc_prop.onePush = 0;
      fc_prop.onOff = 1;
      fc_prop.autoManualMode = config->shutter_auto;
      fc_prop.absValue = (float)config->shutter_value;
      fc_error = fc2SetProperty(fc_context, &fc_prop);
      if (fc_error != FC2_ERROR_OK) {
         lpg_ptgrey_destroy_context(context);
         lpg_ptgrey_fc2error("fc2SetProperty:SHUTTER", fc_error);
      }

      /* add timestamps to embedded image info */
      memset(&fc_eminfo, 0, sizeof(fc2EmbeddedImageInfo));
      fc_error = fc2SetEmbeddedImageInfo(fc_context, &fc_eminfo);
      if (fc_error != FC2_ERROR_OK) {
         lpg_ptgrey_destroy_context(context);
         lpg_ptgrey_fc2error("fc2SetEmbeddedImageInfo", fc_error);
      }

      /* set output type to NONE */
      if (!config->set_output_file) {
         config->set_output_type = 1;
         config->output_type = LPG_OUTPUT_TYPE_NONE;
      }
   }
}


/* This function causes the processor to enter a spin loop thats runs
 * for a few thousand cycles. It can used to prevent a thread from
 * hammering on a mutex.
 */
void
lpg_ptgrey_spin(void)
{
   unsigned volatile char i, j;

   i = 64;
   while (--i) {
      j = 64;
      while (--j) {
         /* empty */
      }
   }
}


/* This function starts capturing for the passed context and camera.
 *
 * Inputs:
 * context - Context to capture
 * cam_idx - Index of camera to start.
 */
void
lpg_ptgrey_start_camera_capture(lpg_ptgrey_context_t* context, unsigned cam_idx)
{
   /* This function is invoked by lpg_ptgrey_start_capture, which is
    * multi-threaded. This function must not exit to matlab, since
    * that would not provide proper cleanup.
    */

   int fc_error_set = 0;
   fc2Error fc_error;
   const char* fc_error_fname;
   const char* gen_error_msg = NULL;
   int output_type;
   ufmf_params_t ufmf_params;
   ufmf_writer_t* ufmf_writer;
   lpg_ptgrey_camera_t* camera;
   fc2Image raw_image;
   fc2TimeStamp start_time, timestamp;
   long frame_idx;

   camera = &context->cameras[cam_idx];

   output_type = camera->config.output_type;

   if (output_type == LPG_OUTPUT_TYPE_UFMF) {
      ufmf_params_initialize(&ufmf_params);

      ufmf_writer = ufmf_writer_create(camera->config.output_file,
         camera->config.width, camera->config.height, &ufmf_params);
      if (ufmf_writer == NULL) {
         gen_error_msg = "Unable to open output file";
         goto abort_capture;
      }
   }

   fc_error = fc2CreateImage(&raw_image);
   if (fc_error != FC2_ERROR_OK) {
      if (output_type == LPG_OUTPUT_TYPE_UFMF) {
         ufmf_writer_destroy(ufmf_writer);
      }

      fc_error_fname = "fc2CreateImage";
      fc_error_set = 1;
      goto abort_capture;
   }

   /* if requested, pause here */
   while (1) {
      lpg_ptgrey_mutex_lock(&context->mutex);

      /* if someone signaled halt, then break loop */
      if (context->halt) {
         lpg_ptgrey_mutex_unlock(&context->mutex);

         if (output_type == LPG_OUTPUT_TYPE_UFMF) {
            ufmf_writer_close(ufmf_writer);
            ufmf_writer_destroy(ufmf_writer);
         }

         fc_error = fc2DestroyImage(&raw_image);
         if (fc_error != FC2_ERROR_OK) {
            fc_error_fname = "fc2DestroyImage";
            fc_error_set = 1;
            goto abort_capture;
         }

         return;
      } else if (!camera->is_paused) {
         lpg_ptgrey_mutex_unlock(&context->mutex);

         break;
      }

      lpg_ptgrey_mutex_unlock(&context->mutex);
      lpg_ptgrey_spin();
   }

   /* pause complete start capture */
   fc_error = fc2StartCapture(camera->fc_context);
   if (fc_error != FC2_ERROR_OK) {
      if (output_type == LPG_OUTPUT_TYPE_UFMF) {
         ufmf_writer_destroy(ufmf_writer);
      }

      fc2DestroyImage(&raw_image);

      fc_error_fname = "fc2StartCapture";
      fc_error_set = 1;
      goto abort_capture;
   }

   /* main loop to record frames */
   frame_idx = 0;
   while (1) {
      if (camera->config.frames_per_capture != -1 &&
         frame_idx >= camera->config.frames_per_capture) {
         break;
      }

      fc_error = fc2RetrieveBuffer(camera->fc_context, &raw_image);
      if (fc_error != FC2_ERROR_OK) {
         if (output_type == LPG_OUTPUT_TYPE_UFMF) {
            ufmf_writer_destroy(ufmf_writer);
         }

         fc2DestroyImage(&raw_image);
         fc2StopCapture(camera->fc_context);

         fc_error_fname = "fc2RetrieveBuffer";
         fc_error_set = 1;
         goto abort_capture;
      }

      timestamp = fc2GetImageTimeStamp(&raw_image);

      if (frame_idx == 0) {
         lpg_ptgrey_mutex_lock(&context->mutex);

         if (context->has_start_time) {
            start_time = context->start_time;
         } else {
            context->start_time = start_time = timestamp;
			context->has_start_time = 1;
         } 

         lpg_ptgrey_mutex_unlock(&context->mutex);
      }

      if (output_type == LPG_OUTPUT_TYPE_UFMF) {
         unsigned char* raw_pixels;
         int gen_error;

         double time = (double)(timestamp.seconds - start_time.seconds) +
            ((double)timestamp.microSeconds - (double)start_time.microSeconds)*1e-6;

         fc_error = fc2GetImageData(&raw_image, &raw_pixels);
         if (fc_error != FC2_ERROR_OK) {
            ufmf_writer_destroy(ufmf_writer);
            fc2DestroyImage(&raw_image);
            fc2StopCapture(camera->fc_context);

            fc_error_fname = "fc2GetImageData";
            fc_error_set = 1;
            goto abort_capture;
         }

         gen_error = ufmf_writer_add(ufmf_writer, raw_pixels, time);
         if (gen_error) {
            ufmf_writer_destroy(ufmf_writer);
            fc2DestroyImage(&raw_image);
            fc2StopCapture(camera->fc_context);
            gen_error_msg = "Unable to add frame to UFMF movie";
            goto abort_capture;
         }
      }

      lpg_ptgrey_mutex_lock(&context->mutex);

      /* if someone signaled halt, then break loop */
      if (context->halt) {
         lpg_ptgrey_mutex_unlock(&context->mutex);
         break;
      }

      lpg_ptgrey_mutex_unlock(&context->mutex);

      /* append image to callback buffer list */
      if (camera->config.callback_fcn != NULL &&
         frame_idx % camera->config.callback_period == 0) {
         unsigned char* raw_pixels;
         int gen_error;

         fc_error = fc2GetImageData(&raw_image, &raw_pixels);
         if (fc_error != FC2_ERROR_OK) {
            if (output_type == LPG_OUTPUT_TYPE_UFMF) {
               ufmf_writer_destroy(ufmf_writer);
            }

            fc2DestroyImage(&raw_image);
            fc2StopCapture(camera->fc_context);

            fc_error_fname = "fc2GetImageData";
            fc_error_set = 1;
            goto abort_capture;
         }

         lpg_ptgrey_mutex_lock(&context->mutex);
         gen_error = lpg_ptgrey_append_to_callback_buffer(&context->callback_buffer,
            cam_idx, camera->config.width, camera->config.height,
            camera->config.pixel_format, raw_pixels);
         lpg_ptgrey_mutex_unlock(&context->mutex);

         if (gen_error) {
            if (output_type == LPG_OUTPUT_TYPE_UFMF) {
               ufmf_writer_destroy(ufmf_writer);
            }

            fc2DestroyImage(&raw_image);
            fc2StopCapture(camera->fc_context);

            fc_error_set = 0;
            goto abort_capture;
         }
      }

      frame_idx = frame_idx + 1;
   }

   fc_error = fc2DestroyImage(&raw_image);
   if (fc_error != FC2_ERROR_OK) {
      if (output_type == LPG_OUTPUT_TYPE_UFMF) {
         ufmf_writer_destroy(ufmf_writer);
      }

      fc2StopCapture(camera->fc_context);

      fc_error_fname = "fc2DestroyImage";
      fc_error_set = 1;
      goto abort_capture;
   }

   if (output_type == LPG_OUTPUT_TYPE_UFMF) {
      ufmf_writer_close(ufmf_writer);
      ufmf_writer_destroy(ufmf_writer);
   }

   fc_error = fc2StopCapture(camera->fc_context);
   if (fc_error != FC2_ERROR_OK) {
      fc_error_fname = "fc2StopCapture";
      fc_error_set = 1;
      goto abort_capture;
   }

   return;

abort_capture:
   /* signal other threads to give up */
   lpg_ptgrey_mutex_lock(&context->mutex);
   context->halt = 1;

   if (!context->has_error_msg) {

      if (fc_error_set) {
         size_t error_buflen = 1024;
         char error_buf[1024];

         snprintf(error_buf, error_buflen, "Error in %s: %s",
            fc_error_fname, lpg_ptgrey_fc2error_to_string(fc_error));

         context->has_error_msg = 1;
         context->error_msg = _strdup(error_buf);
         /* ignore strdup() returning NULL, nothing we can do anyway */
      } else if (gen_error_msg != NULL) {
         context->has_error_msg = 1;
         context->error_msg = _strdup(gen_error_msg);
      } else {
         context->has_error_msg = 1;
         context->error_msg = _strdup("Error in image capture");
      }
   }

   lpg_ptgrey_mutex_unlock(&context->mutex);
}


#if defined(_WIN32) || defined(_WIN64)
typedef struct {
   lpg_ptgrey_context_t* context;
   unsigned cam_idx;
} lpg_ptgrey_thread_wrapper_t;

/* This function is used to create a thread to begin 
 * capturing.
 */
DWORD WINAPI
lpg_ptgrey_start_capture_thread(LPVOID ptr)
{
   lpg_ptgrey_thread_wrapper_t* wrapper;
   lpg_ptgrey_context_t* context;
   unsigned cam_idx;

   wrapper = (lpg_ptgrey_thread_wrapper_t*)ptr;
   context = wrapper->context;
   cam_idx = wrapper->cam_idx;
   free(wrapper);

   lpg_ptgrey_start_camera_capture(context, cam_idx);

   return 0;
}
#else
#error "Unsupported operating system"
#endif

/* This function starts capturing for the passed context.
 *
 * Inputs:
 * context - Context to capture
 * ctxt_idx - Index of context
 */
void
lpg_ptgrey_start_capture(lpg_ptgrey_context_t* context, unsigned ctxt_idx)
{
#if defined(_WIN32) || defined(_WIN64)
   unsigned cam_idx;

   if (context->is_running) {
      mexErrMsgTxt("Context is already capturing");
   }

   context->threads = (lpg_ptgrey_thread_t*)malloc(context->num_cameras * sizeof(lpg_ptgrey_thread_t));
   if (context->threads == NULL) {
      lpg_ptgrey_destroy_context(context);
      lpg_ptgrey_global_contexts[ctxt_idx] = NULL;
      mexErrMsgTxt("Unable to allocate memory");
   }

   context->threads_is_done = (int*)malloc(context->num_cameras * sizeof(int));
   if (context->threads_is_done == NULL) {
      free(context->threads);
      context->threads = NULL;

      lpg_ptgrey_destroy_context(context);
      lpg_ptgrey_global_contexts[ctxt_idx] = NULL;
      mexErrMsgTxt("Unable to allocate memory");
   }

   context->is_running = 1;
   context->has_start_time = 0;

   for (cam_idx = 0; cam_idx < context->num_cameras; cam_idx++) {
      lpg_ptgrey_thread_wrapper_t* wrapper;

      wrapper = (lpg_ptgrey_thread_wrapper_t*)malloc(
         sizeof(lpg_ptgrey_thread_wrapper_t));
      if (wrapper == NULL) {
         unsigned thrd_idx;

         lpg_ptgrey_mutex_lock(&context->mutex);
         context->halt = 1;
         lpg_ptgrey_mutex_unlock(&context->mutex);

         if (cam_idx > 0) {
            WaitForMultipleObjects(cam_idx, context->threads, TRUE, INFINITE);
            for (thrd_idx = 0; thrd_idx < cam_idx; thrd_idx++) {
               CloseHandle(context->threads[thrd_idx]);
            }
         }

         free(context->threads);
         context->threads = NULL;
         free(context->threads_is_done);
         context->threads_is_done = NULL;
         lpg_ptgrey_destroy_context(context);
         lpg_ptgrey_global_contexts[ctxt_idx] = NULL;
         mexErrMsgTxt("Unable to create threads");
      }

      wrapper->context = context;
      wrapper->cam_idx = cam_idx;
 
      context->threads_is_done[cam_idx] = 0;
      context->threads[cam_idx] = CreateThread(NULL, 0,
         lpg_ptgrey_start_capture_thread, wrapper, 0, NULL);
      if (context->threads[cam_idx] == NULL) {
         unsigned thrd_idx;

         free(wrapper);

         lpg_ptgrey_mutex_lock(&context->mutex);
         context->halt = 1;
         lpg_ptgrey_mutex_unlock(&context->mutex);

         if (cam_idx > 0) {
            WaitForMultipleObjects(cam_idx, context->threads, TRUE, INFINITE);
            for (thrd_idx = 0; thrd_idx < cam_idx; thrd_idx++) {
               CloseHandle(context->threads[thrd_idx]);
            }
         }

         free(context->threads);
         context->threads = NULL;
         free(context->threads_is_done);
         context->threads_is_done = NULL;
         lpg_ptgrey_destroy_context(context);
         lpg_ptgrey_global_contexts[ctxt_idx] = NULL;
         mexErrMsgTxt("Unable to create threads");
      }
   }

#else
#error "Unsupported operating system"
#endif

}


/* This function stops capturing for the passed context.
 *
 * Inputs:
 * context - Context to capture
 */
void
lpg_ptgrey_stop_capture(lpg_ptgrey_context_t* context)
{
   unsigned thrd_idx;

   if (!context->is_running) {
      mexErrMsgTxt("Context is not currently capturing");
   }

   lpg_ptgrey_mutex_lock(&context->mutex);
   context->halt = 1;
   lpg_ptgrey_mutex_unlock(&context->mutex);

#if defined(_WIN32) || defined(_WIN64)
   WaitForMultipleObjects(context->num_cameras,
      context->threads, TRUE, INFINITE);
   for (thrd_idx = 0; thrd_idx < context->num_cameras; thrd_idx++) {
      CloseHandle(context->threads[thrd_idx]);
   }

#else
#error "Unsupported operating system"
#endif


   free(context->threads);
   context->threads = NULL;
   free(context->threads_is_done);
   context->threads_is_done = NULL;

   context->is_running = 0;
   context->halt = 0;

   if (context->has_error_msg) {
      char* return_msg;

      context->has_error_msg = 0;
      if (context->error_msg != NULL) {
         return_msg = (char*)mxMalloc(strlen(context->error_msg) + 1);
         strcpy(return_msg, context->error_msg);
         free(context->error_msg);
         context->error_msg = NULL;

         mexErrMsgTxt(return_msg);
      } else {
         mexErrMsgTxt("Error occurred during capture");
      }
   }
}


/* This function stops capturing for the passed context, but does
 * not wait for the capture threads to return. This can be useful
 * if the main threads needs to perform other tasks in order to
 * generate the signals needed to trigger the cameras so that they
 * return from the retrieve buffer function.
 *
 * Inputs:
 * context - Context to stop 
 */
void
lpg_ptgrey_start_slow_stop_capture(lpg_ptgrey_context_t* context)
{
   if (!context->is_running) {
      mexErrMsgTxt("Context is not currently capturing");
   }

   lpg_ptgrey_mutex_lock(&context->mutex);
   context->halt = 1;
   lpg_ptgrey_mutex_unlock(&context->mutex);
}


/* This function should only be used in conjunction with the 
 * lpg_ptgrey_start_slow_stop_capture function. It should be
 * invoked after that function, and will return a mxArray* 
 * scalar containing one when the slow stop has completed,
 * and zero otherwise.
 *
 * Inputs:
 * context - Context to stop 
 */
mxArray*
lpg_ptgrey_slow_stop_capture_is_done(lpg_ptgrey_context_t* context)
{
#if defined(_WIN32) || defined(_WIN64)
   unsigned thrd_idx;
   unsigned done_threads_count = 0;
   int status;
   mxArray* mx_status;

   for (thrd_idx = 0; thrd_idx < context->num_cameras; thrd_idx++) {
      if (!context->threads_is_done[thrd_idx]) {
         DWORD retval;

         retval = WaitForSingleObject(context->threads[thrd_idx], 0);
         if (retval == WAIT_OBJECT_0) {
            context->threads_is_done[thrd_idx] = 1;
            done_threads_count += 1;
         }
         
      } else {
         done_threads_count += 1;
      }
   }

   status = (done_threads_count == context->num_cameras);

   mx_status = mxCreateDoubleMatrix(1, 1, mxREAL);
   *(double*)mxGetPr(mx_status) = (double)status;

   return mx_status;
#else
#error "Unsupported operating system"
#endif
}


/* This function should only be used in conjunction with the 
 * lpg_ptgrey_start_slow_stop_capture function. It should be
 * invoked after that function, and will complete the stop
 * capture function. If any threads are not done, it will
 * wait for them to complete. It will also free additional
 * resources.
 *
 * Inputs:
 * context - Context to stop 
 */
void
lpg_ptgrey_finish_slow_stop_capture(lpg_ptgrey_context_t* context)
{
#if defined(_WIN32) || defined(_WIN64)
   unsigned thrd_idx;

   for (thrd_idx = 0; thrd_idx < context->num_cameras; thrd_idx++) {
      if (!context->threads_is_done[thrd_idx]) {
         WaitForSingleObject(context->threads[thrd_idx], INFINITE);
      }
   }

   for (thrd_idx = 0; thrd_idx < context->num_cameras; thrd_idx++) {
      CloseHandle(context->threads[thrd_idx]);
   }

#else
#error "Unsupported operating system"
#endif

   free(context->threads);
   context->threads = NULL;

   free(context->threads_is_done);
   context->threads_is_done = NULL;

   context->is_running = 0;
   context->halt = 0;

   if (context->has_error_msg) {
      char* return_msg;

      context->has_error_msg = 0;
      if (context->error_msg != NULL) {
         return_msg = (char*)mxMalloc(strlen(context->error_msg) + 1);
         strcpy(return_msg, context->error_msg);
         free(context->error_msg);
         context->error_msg = NULL;

         mexErrMsgTxt(return_msg);
      } else {
         mexErrMsgTxt("Error occurred during capture");
      }
   }
}


/* This function converts the passed callback image into a matlab array.
 * This function may jump back to matlab on error.
 *
 * Inputs:
 * im - Callback image structure
 *
 * Returns:
 * Pointer to matlab array, or null if error occurred
 */
mxArray*
lpg_ptgrey_convert_pixels_to_mxarray(const lpg_ptgrey_callback_image_t* im)
{
   mxArray* output;

   switch (im->pixel_format) {
   case FC2_PIXEL_FORMAT_MONO8:
   case FC2_PIXEL_FORMAT_RAW8: {
      mwSize dims[2];
      uint8_t* data;
      size_t row, col;

      dims[0] = (mwSize)im->height;
      dims[1] = (mwSize)im->width;

      output = mxCreateNumericArray(2, dims, mxUINT8_CLASS, 0);
      /* returns to matlab if allocation fails */

      data = (uint8_t*)mxGetData(output);

      /* matlab uses column major order */
      for (row = 0; row < im->height; row++) {
         for (col = 0; col < im->width; col++) {
            data[row + col * im->height] = im->pixels[row * im->width + col];
         }
      }

   } break;
   default:
      return NULL;
   }

   return output;
}


/* This function polls the callbacks for the passed context.
 *
 * Inputs:
 * context - Context to capture
 *
 * Outputs:
 * One if terminated, zero otherwise (double scalar mxArray*)
 */
mxArray*
lpg_ptgrey_poll_callbacks(lpg_ptgrey_context_t* context)
{
   lpg_ptgrey_callback_buffer_t* buffer;
   mxArray* mx_out_status = NULL;
   int gen_error = 0;

   if (!context->is_running) {
      mexErrMsgTxt("Context is not currently capturing");
   }

   lpg_ptgrey_mutex_lock(&context->mutex);

   /* if error occurred, then shut everything down */
   if (context->has_error_msg) {
      lpg_ptgrey_mutex_unlock(&context->mutex);

      lpg_ptgrey_stop_capture(context);

	  mx_out_status = mxCreateDoubleMatrix(1, 1, mxREAL);
	  *(double*)mxGetPr(mx_out_status) = 1;

	  return mx_out_status;
   }

   /* check the callback functions */
   buffer = &context->callback_buffer;
   if (buffer->num_readable_images > 0) {
      lpg_ptgrey_callback_image_t im;
      mxArray* callback_fcn;
      mxArray* status_ret;
      double status_value;
      mxArray* output[1];
      mxArray* input[2];

      gen_error = lpg_ptgrey_retrieve_from_callback_buffer(buffer, &im);
      if (gen_error) {
         lpg_ptgrey_mutex_unlock(&context->mutex);

         mexErrMsgTxt("Error retrieving image from callback buffer");
      }

      /* invoke callback function */
      callback_fcn = context->cameras[im.cam_idx].config.callback_fcn;
      input[0] = callback_fcn;

      input[1] = lpg_ptgrey_convert_pixels_to_mxarray(&im);

      lpg_ptgrey_mutex_unlock(&context->mutex);

      if (input[1] == NULL) {
         mexErrMsgTxt("Unable to convert image to mxArray");
      }

      /* note that this function could return to matlab immediately,
       * this code should be fine with that, but keep that in mind
       * when making changes.
       */
      mexCallMATLAB(1, output, 2, input, "feval");
      status_ret = output[0];

      /* check the output */
      if (mxGetClassID(status_ret) != mxDOUBLE_CLASS || mxGetImagData(status_ret) != NULL ||
         mxGetN(status_ret) != 1 || mxGetM(status_ret) != 1) {
         mexErrMsgTxt("Callback had invalid return value");
      }

      status_value = *(double*)mxGetPr(status_ret);
      if (status_value != 0) {
         lpg_ptgrey_stop_capture(context);
      }

	  mx_out_status = mxCreateDoubleMatrix(1, 1, mxREAL);
	  *(double*)mxGetPr(mx_out_status) = status_value;
   } else {
      lpg_ptgrey_mutex_unlock(&context->mutex);

	  mx_out_status = mxCreateDoubleMatrix(1, 1, mxREAL);
	  *(double*)mxGetPr(mx_out_status) = 0;
   }

   return mx_out_status;
}


/* This function unpauses the passed camera.
 *
 * Inputs:
 * context - Context to capture
 * cam_idx - Index of camera to unpause
 */
void
lpg_ptgrey_unpause(lpg_ptgrey_context_t* context, unsigned cam_idx)
{
   if (!context->is_running) {
      mexErrMsgTxt("Context is not currently capturing");
   }

   lpg_ptgrey_mutex_lock(&context->mutex);

   context->cameras[cam_idx].is_paused = 0;
   
   lpg_ptgrey_mutex_unlock(&context->mutex);

}


/* This function resets the interface.
 */
void
lpg_ptgrey_reset(void)
{
   unsigned int ctxt_idx;

   for (ctxt_idx = 0; ctxt_idx < LPG_PTGREY_MAX_CONTEXTS; ctxt_idx++) {
      lpg_ptgrey_context_t* ctxt_ptr = lpg_ptgrey_global_contexts[ctxt_idx];

      if (ctxt_ptr != NULL) {
         lpg_ptgrey_destroy_context(ctxt_ptr);
         lpg_ptgrey_global_contexts[ctxt_idx] = NULL;
      }
   }
}


/* This function retrieves a context from mex arguments, assuming that the
 * the second argument is the context identifier.
 *
 * Inputs:
 * nlhs - Number lefthand side arguments
 * plhs - Pointer to lefthand side arguments
 * nrhs - Number righthand side arguments
 * prhs - Pointer ot righthand side arguments
 * ctxt_idx_output - Pointer to which context id is written
 * ctxt_ptr_output - Pointer to which context pointer is written
 *
 * Returns:
 * Zero on success and one on error.
 */
int
lpg_ptgrey_get_context(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[],
   unsigned int* ctxt_idx_output, lpg_ptgrey_context_t** ctxt_ptr_output)
{
   const double eps_value = 1e-8;
   double* ctxt_arg_ptr;
   unsigned int ctxt_idx;
   const mxArray* mx_ctxt_ptr;
   lpg_ptgrey_context_t* ctxt_ptr;

   /* check type of input argument */
   mx_ctxt_ptr = prhs[1];

   if (mxGetClassID(mx_ctxt_ptr) != mxDOUBLE_CLASS || mxGetImagData(mx_ctxt_ptr) != NULL ||
      mxGetN(mx_ctxt_ptr) != 1 || mxGetM(mx_ctxt_ptr) != 1) {
      return 1;
   }

   /* check that input is integer */
   ctxt_arg_ptr = mxGetPr(mx_ctxt_ptr);
   if (*ctxt_arg_ptr < 0 || *ctxt_arg_ptr > LPG_PTGREY_MAX_CONTEXTS - 1 + eps_value ||
      fabs(*ctxt_arg_ptr - floor(*ctxt_arg_ptr + 0.5)) > eps_value) {
      return 1;
   }

   ctxt_idx = (unsigned int)floor(*ctxt_arg_ptr + 0.5);

   if (ctxt_idx >= LPG_PTGREY_MAX_CONTEXTS) {
      return 1;
   }

   ctxt_ptr = lpg_ptgrey_global_contexts[ctxt_idx];
   if (ctxt_ptr == NULL) {
      return 1;
   }

   if (ctxt_idx_output != NULL) {
      *ctxt_idx_output = ctxt_idx;
   }

   if (ctxt_ptr_output != NULL) {
      *ctxt_ptr_output = ctxt_ptr;
   }

   return 0;
}


/* The following function parses the passed matlab array as an boolean
 * value. On success, if set is non-NULL, then *set is assigned a value
 * of 1. On success, if out is non-NULL, then *out is assigned the parsed
 * value.
 *
 * Inputs:
 * mx_val - Array to parse
 * set - Pointer to set flag
 *  out - Pointer to out value
 * 
 * Returns:
 * Zero on success, one on error.
 */
int
lpg_ptgrey_parse_boolean(mxArray* mx_val, int* set, int* out)
{
   const double eps_value = 1e-8;
   double float_val;

   if (mxGetClassID(mx_val) != mxDOUBLE_CLASS ||
      mxGetImagData(mx_val) != NULL ||
      mxGetN(mx_val) != 1 || mxGetM(mx_val) != 1) {
      return 1;
   }

   float_val = *(double*)mxGetPr(mx_val);
   if (float_val < -eps_value || float_val > 1 + eps_value ||
      fabs(floor(float_val + 0.5) - float_val) > eps_value) {
      return 1;
   }

   if (set != NULL) {
      *set = 1;
   }

   if (out != NULL) {
      *out = (float_val > 0.5);
   }

   return 0;
}


/* The following function parses the passed matlab array as an signed
 * integer. On success, if set is non-NULL, then *set is assigned a value
 * of 1. On success, if out is non-NULL, then *out is assigned the parsed
 * value.
 *
 * Inputs:
 * mx_val - Array to parse
 * set - Pointer to set flag
 * out - Pointer to out value
 *
 * Returns:
 * Zero on success, one on error.
 */
int
lpg_ptgrey_parse_signed_int(mxArray* mx_val, int* set,  int* out)
{
   const double eps_value = 1e-8;
   double float_val;

   if (mxGetClassID(mx_val) != mxDOUBLE_CLASS ||
      mxGetImagData(mx_val) != NULL ||
      mxGetN(mx_val) != 1 || mxGetM(mx_val) != 1) {
      return 1;
   }

   float_val = *(double*)mxGetPr(mx_val);
   if (fabs(floor(float_val + 0.5) - float_val) > eps_value) {
      return 1;
   }

   if (set != NULL) {
      *set = 1;
   }

   if (out != NULL) {
      *out = (int)floor(float_val + 0.5);
   }

   return 0;
}


/* The following function parses the passed matlab array as an unsigned
 * integer. On success, if set is non-NULL, then *set is assigned a value
 * of 1. On success, if out is non-NULL, then *out is assigned the parsed
 * value.
 *
 * Inputs:
 * mx_val - Array to parse
 * set - Pointer to set flag
 * out - Pointer to out value
 *
 * Returns:
 * Zero on success, one on error.
 */
int
lpg_ptgrey_parse_unsigned_int(mxArray* mx_val, int* set, unsigned int* out)
{
   const double eps_value = 1e-8;
   double float_val;

   if (mxGetClassID(mx_val) != mxDOUBLE_CLASS ||
      mxGetImagData(mx_val) != NULL ||
      mxGetN(mx_val) != 1 || mxGetM(mx_val) != 1) {
      return 1;
   }

   float_val = *(double*)mxGetPr(mx_val);
   if (float_val < -eps_value || fabs(floor(float_val + 0.5) - float_val) > eps_value) {
      return 1;
   }

   if (set != NULL) {
      *set = 1;
   }

   if (out != NULL) {
      *out = (unsigned int)floor(float_val + 0.5);
   }

   return 0;
}


/* The following function parses the passed matlab array as an signed
 * long. On success, if set is non-NULL, then *set is assigned a value
 * of 1. On success, if out is non-NULL, then *out is assigned the parsed
 * value.
 *
 * Inputs:
 * mx_val - Array to parse
 * set - Pointer to set flag
 * out - Pointer to out value
 *
 * Returns:
 * Zero on success, one on error.
 */
int
lpg_ptgrey_parse_signed_long(mxArray* mx_val, int* set, long* out)
{
   const double eps_value = 1e-8;
   double float_val;

   if (mxGetClassID(mx_val) != mxDOUBLE_CLASS ||
      mxGetImagData(mx_val) != NULL ||
      mxGetN(mx_val) != 1 || mxGetM(mx_val) != 1) {
      return 1;
   }

   float_val = *(double*)mxGetPr(mx_val);
   if (fabs(floor(float_val + 0.5) - float_val) > eps_value) {
      return 1;
   }

   if (set != NULL) {
      *set = 1;
   }

   if (out != NULL) {
      *out = (long)floor(float_val + 0.5);
   }

   return 0;
}


/* The following function parses the passed matlab array as an unsigned
 * integer. On success, if set is non-NULL, then *set is assigned a value
 * of 1. On success, if out is non-NULL, then *out is assigned the parsed
 * value.
 *
 * Inputs:
 * mx_val - Array to parse
 * set - Pointer to set flag
 * out - Pointer to out value
 *
 * Returns:
 * Zero on success, one on error.
 */
int
lpg_ptgrey_parse_unsigned_long(mxArray* mx_val, int* set, unsigned long* out)
{
   const double eps_value = 1e-8;
   double float_val;

   if (mxGetClassID(mx_val) != mxDOUBLE_CLASS ||
      mxGetImagData(mx_val) != NULL ||
      mxGetN(mx_val) != 1 || mxGetM(mx_val) != 1) {
      return 1;
   }

   float_val = *(double*)mxGetPr(mx_val);
   if (float_val < -eps_value || fabs(floor(float_val + 0.5) - float_val) > eps_value) {
      return 1;
   }

   if (set != NULL) {
      *set = 1;
   }

   if (out != NULL) {
      *out = (unsigned long)floor(float_val + 0.5);
   }

   return 0;
}


/* The following function parses the passed matlab array as an unsigned
 * double. On success, if set is non-NULL, then *set is assigned a value
 * of 1. On success, if out is non-NULL, then *out is assigned the parsed
 * value.
 *
 * Inputs:
 * mx_val - Array to parse
 * set - Pointer to set flag
 * out - Pointer to out value
 *
 * Returns:
 * Zero on success, one on error.
 */
int
lpg_ptgrey_parse_unsigned_double(mxArray* mx_val, int* set, double* out)
{
   const double eps_value = 1e-8;
   double float_val;

   if (mxGetClassID(mx_val) != mxDOUBLE_CLASS ||
      mxGetImagData(mx_val) != NULL ||
      mxGetN(mx_val) != 1 || mxGetM(mx_val) != 1) {
      return 1;
   }

   float_val = *(double*)mxGetPr(mx_val);
   if (float_val < - eps_value || fabs(floor(float_val + 0.5) - float_val) > eps_value) {
      return 1;
   }

   if (float_val < 0) {
      float_val = 0.0;
   }

   if (set != NULL) {
      *set = 1;
   }

   if (out != NULL) {
      *out = float_val;
   }

   return 0;
}


/* This function parses an individual element from the cameras structure array
 * of the config input argument.
 *
 * Inputs:
 * mx_struct - mxArray containing structure of parameters
 * struct_idx - Index into the mx_struct structure array
 * config - structure to which configuration is written
 * field_str - On error, overwritten with value of field producing error,
 *          should be free()'d when no longer needed
 *
 * Returns:
 * Zero on success, one on error.
 */
int
lpg_ptgrey_parse_camera_config(const mxArray* mx_struct, unsigned struct_idx,
   lpg_ptgrey_camera_config_t* config, char** field_str)
{
   const double eps_value = 1e-8;
   int gen_error;
   int num_fields;
   int field_idx;

   num_fields = mxGetNumberOfFields(mx_struct);
   for (field_idx = 0; field_idx < num_fields; field_idx++) {
      const char* field_name;
      mxArray* mx_field_ptr;

      field_name = mxGetFieldNameByNumber(mx_struct, field_idx);
      mx_field_ptr = mxGetFieldByNumber(mx_struct, struct_idx, field_idx);

      if (mx_field_ptr == NULL || mxIsEmpty(mx_field_ptr)) {
         continue;
      }

      if (!strcmp(field_name, "dev_id")) {
         gen_error = lpg_ptgrey_parse_unsigned_int(mx_field_ptr,
            NULL, &config->dev_id);
         if (gen_error) {
            *field_str = _strdup("dev_id");
            return 1;
         }
      } else if (!strcmp(field_name, "mode")) {
         gen_error = lpg_ptgrey_parse_unsigned_int(mx_field_ptr,
            &config->set_mode, &config->mode);
         if (gen_error) {
            *field_str = _strdup("mode");
            return 1;
         }
      } else if (!strcmp(field_name, "offsetX")) {
         gen_error = lpg_ptgrey_parse_unsigned_int(mx_field_ptr,
            &config->set_offsetX, &config->offsetX);
         if (gen_error) {
            *field_str = _strdup("offsetX");
            return 1;
         }
      } else if (!strcmp(field_name, "offsetY")) {
         gen_error = lpg_ptgrey_parse_unsigned_int(mx_field_ptr,
            &config->set_offsetY, &config->offsetY);
         if (gen_error) {
            *field_str = _strdup("offsetY");
            return 1;
         }
      } else if (!strcmp(field_name, "width")) {
         gen_error = lpg_ptgrey_parse_unsigned_int(mx_field_ptr,
            &config->set_width, &config->width);
         if (gen_error) {
            *field_str = _strdup("width");
            return 1;
         }
      } else if (!strcmp(field_name, "height")) {
         gen_error = lpg_ptgrey_parse_unsigned_int(mx_field_ptr,
            &config->set_height, &config->height);
         if (gen_error) {
            *field_str = _strdup("height");
            return 1;
         }
      } else if (!strcmp(field_name, "framerate")) {
         gen_error = lpg_ptgrey_parse_unsigned_double(mx_field_ptr,
            &config->set_framerate, &config->framerate);
         if (gen_error) {
            *field_str = _strdup("framerate");
            return 1;
         }

         if (config->framerate > 100) {
            config->set_framerate = 0;
            config->framerate = 100;

            *field_str = _strdup("framerate");
            return 1;
         }
      } else if (!strcmp(field_name, "pixel_format")) {
         char* char_val;

         if (mxGetClassID(mx_field_ptr) != mxCHAR_CLASS ||
            mxGetN(mx_field_ptr)  < 1 || mxGetM(mx_field_ptr) != 1) {
            *field_str = _strdup("pixel_format");
            return 1;
         }

         char_val = mxArrayToString(mx_field_ptr);
         if (!strcmp(char_val, "mono8")) {
            config->pixel_format = FC2_PIXEL_FORMAT_MONO8;
         } else if (!strcmp(char_val, "raw8")) {
            config->pixel_format = FC2_PIXEL_FORMAT_RAW8;
         } else {
            mxFree(char_val);
            *field_str = _strdup("pixel_format");
            return 1;
         }

         mxFree(char_val);
         config->set_pixel_format = 1;
      } else if (!strcmp(field_name, "frames_per_capture")) {
         gen_error = lpg_ptgrey_parse_signed_long(mx_field_ptr,
            &config->set_frames_per_capture, &config->frames_per_capture);
         if (gen_error) {
            *field_str = _strdup("frames_per_capture");
            return 1;
         }

         if (config->frames_per_capture < -1 ||
             config->frames_per_capture == 0) {
            config->set_frames_per_capture = 1;
            config->frames_per_capture = 1;

            *field_str = _strdup("frames_per_capture");
            return 1;
         }
      } else if (!strcmp(field_name, "gain_auto")) {
         gen_error = lpg_ptgrey_parse_boolean(mx_field_ptr,
            &config->set_gain_auto, &config->gain_auto);
         if (gen_error) {
            *field_str = _strdup("gain_auto");
            return 1;
         }
      } else if (!strcmp(field_name, "gain_value")) {
         gen_error = lpg_ptgrey_parse_unsigned_double(mx_field_ptr,
            &config->set_gain_value, &config->gain_value);
         if (gen_error) {
            *field_str = _strdup("gain_value");
            return 1;
         }
      } else if (!strcmp(field_name, "shutter_auto")) {
         gen_error = lpg_ptgrey_parse_boolean(mx_field_ptr,
            &config->set_shutter_auto, &config->shutter_auto);
         if (gen_error) {
            *field_str = _strdup("shutter_auto");
            return 1;
         }
      } else if (!strcmp(field_name, "shutter_value")) {
         gen_error = lpg_ptgrey_parse_unsigned_double(mx_field_ptr,
            &config->set_shutter_value, &config->shutter_value);
         if (gen_error) {
            *field_str = _strdup("shutter_value");
            return 1;
         }
      } else if (!strcmp(field_name, "start_paused")) {
         gen_error = lpg_ptgrey_parse_boolean(mx_field_ptr,
            &config->set_start_paused, &config->start_paused);
         if (gen_error) {
            *field_str = _strdup("start_paused");
            return 1;
         }
      } else if (!strcmp(field_name, "trigger_enabled")) {
         gen_error = lpg_ptgrey_parse_boolean(mx_field_ptr,
            &config->set_trigger_enabled, &config->trigger_enabled);
         if (gen_error) {
            *field_str = _strdup("trigger_enabled");
            return 1;
         }
      } else if (!strcmp(field_name, "trigger_mode")) {
         gen_error = lpg_ptgrey_parse_unsigned_int(mx_field_ptr,
            &config->set_trigger_mode, &config->trigger_mode);
         if (gen_error) {
            *field_str = _strdup("trigger_mode");
            return 1;
         }
      } else if (!strcmp(field_name, "trigger_source")) {
         gen_error = lpg_ptgrey_parse_unsigned_int(mx_field_ptr,
            &config->set_trigger_source, &config->trigger_source);
         if (gen_error) {
            *field_str = _strdup("trigger_source");
            return 1;
         }
      } else if (!strcmp(field_name, "trigger_polarity")) {
         gen_error = lpg_ptgrey_parse_boolean(mx_field_ptr,
            &config->set_trigger_polarity, &config->trigger_polarity);
         if (gen_error) {
            *field_str = _strdup("trigger_polarity");
            return 1;
         }
      } else if (!strcmp(field_name, "strobe_enabled")) {
         gen_error = lpg_ptgrey_parse_boolean(mx_field_ptr,
            &config->set_strobe_enabled, &config->strobe_enabled);
         if (gen_error) {
            *field_str = _strdup("strobe_enabled");
            return 1;
         }
      } else if (!strcmp(field_name, "strobe_source")) {
         gen_error = lpg_ptgrey_parse_unsigned_int(mx_field_ptr,
            &config->set_strobe_source, &config->strobe_source);
         if (gen_error) {
            *field_str = _strdup("strobe_source");
            return 1;
         }
      } else if (!strcmp(field_name, "strobe_polarity")) {
         gen_error = lpg_ptgrey_parse_boolean(mx_field_ptr,
            &config->set_strobe_polarity, &config->strobe_polarity);
         if (gen_error) {
            *field_str = _strdup("strobe_polarity");
            return 1;
         }
      } else if (!strcmp(field_name, "output_file")) {
         char* char_val;

         if (mxGetClassID(mx_field_ptr) != mxCHAR_CLASS ||
            mxGetN(mx_field_ptr)  < 1 || mxGetM(mx_field_ptr) != 1) {
            *field_str = _strdup("output_file");
            return 1;
         }


         char_val = mxArrayToString(mx_field_ptr);
         if (char_val == NULL) {
            *field_str = _strdup("output_file");
            return 1;
         }

         config->output_file = _strdup(char_val);
         if (config->output_file == NULL) {
            mxFree(char_val);
            *field_str = _strdup("output_file");
            return 1;
         }

         mxFree(char_val);
         config->set_output_file = 1;
      } else if (!strcmp(field_name, "output_type")) {
         char* char_val;

         if (mxGetClassID(mx_field_ptr) != mxCHAR_CLASS ||
            mxGetN(mx_field_ptr)  < 1 || mxGetM(mx_field_ptr) != 1) {
            *field_str = _strdup("output_type");
            return 1;
         }

         char_val = mxArrayToString(mx_field_ptr);
         if (char_val == NULL) {
            *field_str = _strdup("output_type");
            return 1;
         }

         if (!strcmp(char_val, "ufmf")) {
            config->output_type = LPG_OUTPUT_TYPE_UFMF;
         } else {
            mxFree(char_val);
            *field_str = _strdup("output_type");
            return 1;
         }

         mxFree(char_val);
         config->set_output_type = 1;
      } else if (!strcmp(field_name, "callback_fcn")) {
         mxArray* new_array;

         if (mxGetClassID(mx_field_ptr) != mxFUNCTION_CLASS) {
            *field_str = _strdup("callback_fcn");
            return 1;
         }

         /* if mxDuplicateArray fails, then control returns
          * immediately to matlab without destroying context
          * this is a bug, but unfixable given matlab's poor
          * design in this regard
          */
         new_array = mxDuplicateArray(mx_field_ptr);
         mexMakeArrayPersistent(new_array);

         config->set_callback_fcn = 1;
         config->callback_fcn = new_array;
      } else if (!strcmp(field_name, "callback_period")) {
         gen_error = lpg_ptgrey_parse_unsigned_long(mx_field_ptr,
            &config->set_callback_period, &config->callback_period);
         if (gen_error) {
            *field_str = _strdup("callback_period");
            return 1;
         }

         if (config->callback_period < 1) {
            config->set_callback_period = 0;
            config->callback_period = 1;
 
            *field_str = _strdup("callback_period");
            return 1;
         }
      } else {
         *field_str = _strdup(field_name);
         return 1;
      }
   }

   return 0;
}


/* This function implements a matlab function that creates and
 * configures a context.
 *
 * Inputs:
 * nlhs - Number lefthand side arguments
 * plhs - Pointer to lefthand side arguments
 * nrhs - Number righthand side arguments
 * prhs - Pointer ot righthand side arguments
 *
 * Matlab Inputs:
 * config - Configuration struct
 *
 *  Matlab Outputs:
 *  Context identifier (double scalar)
 *
 *  The configuration structure 'config' has the following form:
 *    cameras - Nx1 array with fields:
 *       framerate - double scalar
 */
void
lpg_ptgrey_cmd_create_context(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
   unsigned int ctxt_idx;
   lpg_ptgrey_context_t* context;
   const mxArray* mx_config_ptr;
   unsigned int num_fields, field_idx;

   /* check arguments */
   if (nrhs != 2) {
      mexErrMsgTxt("This function accepts exactly two input arguments");
   }

   if (nlhs != 1) {
      mexErrMsgTxt("This function requires exactly one output argument");
   }

   /* find available context */
   for (ctxt_idx = 0; ctxt_idx < LPG_PTGREY_MAX_CONTEXTS; ctxt_idx++) {
      if (lpg_ptgrey_global_contexts[ctxt_idx] == NULL) {
         break;
      }
   }

   if (ctxt_idx == LPG_PTGREY_MAX_CONTEXTS) {
      mexErrMsgTxt("Limit for number of contexts has been reached");
   }

   /* initialize the context */
   context = (lpg_ptgrey_context_t*)malloc(sizeof(lpg_ptgrey_context_t));
   if (context == NULL) {
      mexErrMsgTxt("Unable to allocate memory");
   }

   lpg_ptgrey_initialize_context(context);

   /* check the config argument */
   mx_config_ptr = prhs[1];
   if (mxGetClassID(mx_config_ptr) != mxSTRUCT_CLASS ||
      mxGetN(mx_config_ptr) != 1 || mxGetM(mx_config_ptr) != 1) {
      lpg_ptgrey_destroy_context(context);
      mexErrMsgTxt("Argument 'config' must be a scalar structure");
   }

   /* parse each of the fields */
   num_fields = mxGetNumberOfFields(mx_config_ptr);
   for (field_idx = 0; field_idx < num_fields; field_idx++) {
      const char* field_name;
      mxArray* mx_field_ptr;

      field_name = mxGetFieldNameByNumber(mx_config_ptr, field_idx);
      mx_field_ptr = mxGetFieldByNumber(mx_config_ptr, 0, field_idx);

      /* parse the 'cameras' field */
      if (!strcmp(field_name, "cameras")) {
         unsigned int cam_idx;

         /* check the cameras field */
         if (mxGetClassID(mx_field_ptr) != mxSTRUCT_CLASS ||
            mxGetN(mx_field_ptr) < 1 || mxGetM(mx_field_ptr) != 1) {
            lpg_ptgrey_destroy_context(context);
            mexErrMsgTxt("Field 'cameras' of argument 'config' has invalid value");
         }

         context->num_cameras = (unsigned int)mxGetN(mx_field_ptr);
         context->cameras = (lpg_ptgrey_camera_t*)malloc(
            context->num_cameras * sizeof(lpg_ptgrey_camera_t));
         if (context->cameras == NULL) {
            lpg_ptgrey_destroy_context(context);
            mexErrMsgTxt("Unable to allocate memory");
         }

         for (cam_idx = 0; cam_idx < context->num_cameras; cam_idx++) {
            lpg_ptgrey_initialize_camera(context->cameras + cam_idx);

            context->cameras[cam_idx].config.dev_id = cam_idx;
         }

         for (cam_idx = 0; cam_idx < context->num_cameras; cam_idx++) {
            char* field_str;

            if (lpg_ptgrey_parse_camera_config(mx_field_ptr, cam_idx,
               &context->cameras[cam_idx].config, &field_str)) {
               const size_t error_buflen = 1024;
               char error_buf[1024];

               if (field_str != NULL) {
                  snprintf(error_buf, error_buflen, "Invalid configuration for camera %d, parameter %s",
                     cam_idx + 1, field_str);
                  free(field_str);
               } else {
                  snprintf(error_buf, error_buflen, "Invalid configuration for camera %d",
                     cam_idx + 1);
               }
               
               lpg_ptgrey_destroy_context(context);
               mexErrMsgTxt(error_buf);
            }
         }

      } else {
         lpg_ptgrey_destroy_context(context);
         mexErrMsgTxt("Unrecognized field name in argument 'config'");
      }
   }

   /* all configuration information is parsed, try to actually make it */
   lpg_ptgrey_realize_context(context);

   lpg_ptgrey_global_contexts[ctxt_idx] = context;

   /* return identifier for the context */
   plhs[0] = mxCreateDoubleScalar((double)ctxt_idx);
}


/* This function implements a matlab function that destoys a context.
 *
 * Inputs:
 * nlhs - Number lefthand side arguments
 * plhs - Pointer to lefthand side arguments
 * nrhs - Number righthand side arguments
 * prhs - Pointer ot righthand side arguments
 *
 * Matlab Inputs:
 * ctxt - Context identifier (double scalar)
 *
 * Matlab Outputs:
 * None
 */
void
lpg_ptgrey_cmd_destroy_context(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
   unsigned int ctxt_idx;
   lpg_ptgrey_context_t* ctxt_ptr;

   /* check arguments */
   if (nrhs != 2) {
      mexErrMsgTxt("This function accepts exactly two input arguments");
   }

   if (nlhs != 0) {
      mexErrMsgTxt("This function has no output arguments");
   }
   
   if (lpg_ptgrey_get_context(nlhs, plhs, nrhs, prhs, &ctxt_idx, &ctxt_ptr)) {
      mexErrMsgTxt("Argument 'ctxt' has an invalid value");
   }

   lpg_ptgrey_destroy_context(ctxt_ptr);
   lpg_ptgrey_global_contexts[ctxt_idx] = NULL;
}


/* This function implements a matlab function that starts capturing
 *
 * Inputs:
 * nlhs - Number lefthand side arguments
 * plhs - Pointer to lefthand side arguments
 * nrhs - Number righthand side arguments
 * prhs - Pointer ot righthand side arguments
 *
 * Matlab Inputs:
 * ctxt - Context identifier (double scalar)
 *
 * Matlab Outputs:
 * None
 */
void
lpg_ptgrey_cmd_start_capture(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
   unsigned int ctxt_idx;
   lpg_ptgrey_context_t* ctxt_ptr;

   /* check arguments */
   if (nrhs != 2) {
      mexErrMsgTxt("This function accepts exactly two input arguments");
   }

   if (nlhs != 0) {
      mexErrMsgTxt("This function has no output arguments");
   }

   if (lpg_ptgrey_get_context(nlhs, plhs, nrhs, prhs, &ctxt_idx, &ctxt_ptr)) {
      mexErrMsgTxt("Argument 'ctxt' has an invalid value");
   }

   lpg_ptgrey_start_capture(ctxt_ptr, ctxt_idx);
}


/* This function implements a matlab function that stops capturing
 *
 * Inputs:
 * nlhs - Number lefthand side arguments
 * plhs - Pointer to lefthand side arguments
 * nrhs - Number righthand side arguments
 * prhs - Pointer ot righthand side arguments
 *
 * Matlab Inputs:
 * ctxt - Context identifier (double scalar)
 *
 * Matlab Outputs:
 * None
 */
void
lpg_ptgrey_cmd_stop_capture(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
   unsigned int ctxt_idx;
   lpg_ptgrey_context_t* ctxt_ptr;

   /* check arguments */
   if (nrhs != 2) {
      mexErrMsgTxt("This function accepts exactly two input arguments");
   }

   if (nlhs != 0) {
      mexErrMsgTxt("This function has no output arguments");
   }

   if (lpg_ptgrey_get_context(nlhs, plhs, nrhs, prhs, &ctxt_idx, &ctxt_ptr)) {
      mexErrMsgTxt("Argument 'ctxt' has an invalid value");
   }

   lpg_ptgrey_stop_capture(ctxt_ptr);
}


/* This function implements a matlab function that starts a slow stop capture.
 * This initiates a stop capture but does not wait for the capture threads to
 * return. 
 *
 * Inputs:
 * nlhs - Number lefthand side arguments
 * plhs - Pointer to lefthand side arguments
 * nrhs - Number righthand side arguments
 * prhs - Pointer ot righthand side arguments
 *
 * Matlab Inputs:
 * ctxt - Context identifier (double scalar)
 *
 * Matlab Outputs:
 * None
 */
void
lpg_ptgrey_cmd_start_slow_stop_capture(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
   unsigned int ctxt_idx;
   lpg_ptgrey_context_t* ctxt_ptr;

   /* check arguments */
   if (nrhs != 2) {
      mexErrMsgTxt("This function accepts exactly two input arguments");
   }

   if (nlhs != 0) {
      mexErrMsgTxt("This function has no output arguments");
   }

   if (lpg_ptgrey_get_context(nlhs, plhs, nrhs, prhs, &ctxt_idx, &ctxt_ptr)) {
      mexErrMsgTxt("Argument 'ctxt' has an invalid value");
   }

   lpg_ptgrey_start_slow_stop_capture(ctxt_ptr);
}


/* This function implements a matlab function that checks if the slow stop
 * capture is complete. 
 *
 * Inputs:
 * nlhs - Number lefthand side arguments
 * plhs - Pointer to lefthand side arguments
 * nrhs - Number righthand side arguments
 * prhs - Pointer ot righthand side arguments
 *
 * Matlab Inputs:
 * ctxt - Context identifier (double scalar)
 *
 * Matlab Outputs:
 * is_done - True if slow stop is done (double scalar)
 */
void
lpg_ptgrey_cmd_slow_stop_capture_is_done(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
   unsigned int ctxt_idx;
   lpg_ptgrey_context_t* ctxt_ptr;

   /* check arguments */
   if (nrhs != 2) {
      mexErrMsgTxt("This function accepts exactly two input arguments");
   }

   if (nlhs != 1) {
      mexErrMsgTxt("This function one output argument");
   }

   if (lpg_ptgrey_get_context(nlhs, plhs, nrhs, prhs, &ctxt_idx, &ctxt_ptr)) {
      mexErrMsgTxt("Argument 'ctxt' has an invalid value");
   }

   plhs[0] = lpg_ptgrey_slow_stop_capture_is_done(ctxt_ptr);
}


/* This function implements a matlab function that finishes a slow stop capture.
 * This will wait for all capture threads to complete and free capture-associated
 * resources.
 *
 * Inputs:
 * nlhs - Number lefthand side arguments
 * plhs - Pointer to lefthand side arguments
 * nrhs - Number righthand side arguments
 * prhs - Pointer ot righthand side arguments
 *
 * Matlab Inputs:
 * ctxt - Context identifier (double scalar)
 *
 * Matlab Outputs:
 * None
 */
void
lpg_ptgrey_cmd_finish_slow_stop_capture(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
   unsigned int ctxt_idx;
   lpg_ptgrey_context_t* ctxt_ptr;

   /* check arguments */
   if (nrhs != 2) {
      mexErrMsgTxt("This function accepts exactly two input arguments");
   }

   if (nlhs != 0) {
      mexErrMsgTxt("This function has no output arguments");
   }

   if (lpg_ptgrey_get_context(nlhs, plhs, nrhs, prhs, &ctxt_idx, &ctxt_ptr)) {
      mexErrMsgTxt("Argument 'ctxt' has an invalid value");
   }

   lpg_ptgrey_finish_slow_stop_capture(ctxt_ptr);
}


/* This function implements a matlab function that polls the callback
 * functions. 
 *
 * Inputs:
 * nlhs - Number lefthand side arguments
 * plhs - Pointer to lefthand side arguments
 * nrhs - Number righthand side arguments
 * prhs - Pointer ot righthand side arguments
 *
 * Matlab Inputs:
 * ctxt - Context identifier (double scalar)
 *
 * Matlab Outputs:
 * is_term - One it terminated, zero otherwise (double scalar)
 */
void
lpg_ptgrey_cmd_poll_callbacks(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
   unsigned int ctxt_idx;
   lpg_ptgrey_context_t* ctxt_ptr;

   /* check arguments */
   if (nrhs != 2) {
      mexErrMsgTxt("This function accepts exactly two input arguments");
   }

   if (nlhs != 1) {
      mexErrMsgTxt("This function has exactly one output argument");
   }

   if (lpg_ptgrey_get_context(nlhs, plhs, nrhs, prhs, &ctxt_idx, &ctxt_ptr)) {
      mexErrMsgTxt("Argument 'ctxt' has an invalid value");
   }

   plhs[0] = lpg_ptgrey_poll_callbacks(ctxt_ptr);
}


/* This function implements a matlab function that unpauses a camera
 *
 * Inputs:
 * nlhs - Number lefthand side arguments
 * plhs - Pointer to lefthand side arguments
 * nrhs - Number righthand side arguments
 * prhs - Pointer ot righthand side arguments
 *
 * Matlab Inputs:
 * ctxt - Context identifier (double scalar)
 * cam_idx - Camera index (zero-based)
 * 
 * Matlab Outputs:
 * None
 */
void
lpg_ptgrey_cmd_unpause(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
   unsigned int ctxt_idx;
   lpg_ptgrey_context_t* ctxt_ptr;
   const double eps_value = 1e-8;
   const mxArray* mx_cam_idx;
   double* cam_arg_ptr;
   unsigned cam_idx;

   /* check arguments */
   if (nrhs != 3) {
      mexErrMsgTxt("This function accepts exactly three input arguments");
   }

   if (nlhs != 0) {
      mexErrMsgTxt("This function has no output arguments");
   }

   if (lpg_ptgrey_get_context(nlhs, plhs, nrhs, prhs, &ctxt_idx, &ctxt_ptr)) {
      mexErrMsgTxt("Argument 'ctxt' has an invalid value");
   }

   mx_cam_idx = prhs[1];

   if (mxGetClassID(mx_cam_idx) != mxDOUBLE_CLASS || mxGetImagData(mx_cam_idx) != NULL ||
      mxGetN(mx_cam_idx) != 1 || mxGetM(mx_cam_idx) != 1) {
      mexErrMsgTxt("Argument 'cam_idx' has an invalid value");
   }

   /* check that cam_idx is integer */
   cam_arg_ptr = mxGetPr(mx_cam_idx);
   if (*cam_arg_ptr < - eps_value || *cam_arg_ptr > ctxt_ptr->num_cameras - 1 + eps_value ||
      fabs(*cam_arg_ptr - floor(*cam_arg_ptr + 0.5)) > eps_value) {
      mexErrMsgTxt("Argument 'cam_idx' has an invalid value");
   }

   cam_idx = (unsigned int)floor(*cam_arg_ptr + 0.5);

   lpg_ptgrey_unpause(ctxt_ptr, cam_idx);
}


/* This function implements a matlab function that resets the interface.
 * Inputs:
 * nlhs - Number lefthand side arguments
 * plhs - Pointer to lefthand side arguments
 * nrhs - Number righthand side arguments
 * prhs - Pointer ot righthand side arguments
 *
 * Matlab Inputs:
 * None
 *
 * Matlab Outputs:
 * None
 */
void
lpg_ptgrey_cmd_reset(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
   /* check arguments */
   if (nrhs != 1) {
      mexErrMsgTxt("This function does not accept any arguments");
   }

   if (nlhs != 0) {
      mexErrMsgTxt("This function does not have any outputs");
   }

   lpg_ptgrey_reset();
}


/* The following function is the main entry point for matlab
 *
 * Inputs:
 * nlhs - Number lefthand side arguments
 * plhs - Pointer to lefthand side arguments
 * nrhs - Number righthand side arguments
 * prhs - Pointer ot righthand side arguments
 */
void
mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
   char* cmd_str;

   /* first thing, initialize contexts if not already done */
   if (!lpg_ptgrey_global_contexts_is_init) {
      unsigned int ctxt_idx;

      for (ctxt_idx = 0; ctxt_idx < LPG_PTGREY_MAX_CONTEXTS; ctxt_idx++) {
         lpg_ptgrey_global_contexts[ctxt_idx] = NULL;
      }

      lpg_ptgrey_global_contexts_is_init = 1;
   }

   /* log the exit function */
   mexAtExit(lpg_ptgrey_reset);

   /* read in the first argument, which is the command string */
   if (nrhs < 1) {
      mexErrMsgTxt("This function requires at least one input argument");
   }

   if (mxGetClassID(prhs[0]) != mxCHAR_CLASS || mxGetM(prhs[0]) != 1 || mxGetN(prhs[0]) < 1) {
      mexErrMsgTxt("First argument must be a string");
   }

   cmd_str = mxArrayToString(prhs[0]);

   if (!strcmp(cmd_str, "create_context")) {
      mxFree(cmd_str);
      lpg_ptgrey_cmd_create_context(nlhs, plhs, nrhs, prhs);
      return;
   } else if (!strcmp(cmd_str, "destroy_context")) {
      mxFree(cmd_str);
      lpg_ptgrey_cmd_destroy_context(nlhs, plhs, nrhs, prhs);
      return;
   } else if (!strcmp(cmd_str, "start_capture")) {
      mxFree(cmd_str);
      lpg_ptgrey_cmd_start_capture(nlhs, plhs, nrhs, prhs);
      return;
   } else if (!strcmp(cmd_str, "stop_capture")) {
      mxFree(cmd_str);
      lpg_ptgrey_cmd_stop_capture(nlhs, plhs, nrhs, prhs);
      return;
   } else if (!strcmp(cmd_str, "start_slow_stop_capture")) {
      mxFree(cmd_str);
      lpg_ptgrey_cmd_start_slow_stop_capture(nlhs, plhs, nrhs, prhs);
      return;
   } else if (!strcmp(cmd_str, "slow_stop_capture_is_done")) {
      mxFree(cmd_str);
      lpg_ptgrey_cmd_slow_stop_capture_is_done(nlhs, plhs, nrhs, prhs);
      return;
   } else if (!strcmp(cmd_str, "finish_slow_stop_capture")) {
      mxFree(cmd_str);
      lpg_ptgrey_cmd_finish_slow_stop_capture(nlhs, plhs, nrhs, prhs);
      return;
   } else if (!strcmp(cmd_str, "poll_callbacks")) {
      mxFree(cmd_str);
      lpg_ptgrey_cmd_poll_callbacks(nlhs, plhs, nrhs, prhs);
      return;
   } else if (!strcmp(cmd_str, "unpause")) {
      mxFree(cmd_str);
      lpg_ptgrey_cmd_unpause(nlhs, plhs, nrhs, prhs);
      return;
   } else if (!strcmp(cmd_str, "reset")) {
      mxFree(cmd_str);
      lpg_ptgrey_cmd_reset(nlhs, plhs, nrhs, prhs);
      return;
   } else {
      mxFree(cmd_str);
      mexErrMsgTxt("Unrecognized command string");
   }
}
