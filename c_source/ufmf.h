#ifndef __UFMF_H__
#define __UFMF_H__


/* Define types used for prototypes. We avoid standard
 * names to avoid polluting the namespace.
 */
#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#define UFMF_UINT8_T unsigned __int8
#define UFMF_UINT16_T unsigned __int16
#define UFMF_UINT32_T unsigned __int32
#else
#include <stdint.h>
#define UFMF_UINT8_T uint8_t
#define UFMF_UINT16_T uint16_t
#define UFMF_UINT32_T uint32_t
#endif


/**
 * The following struct holds the parameters values for the ufmf writer
 */
typedef struct
{
   /** Maximum number of seconds between background keyframes */
   double bg_update_period_ramp_max;

   /** Starting value for update period during ramp */
   double bg_update_period_ramp_min;

   /** Multiply update period by this factor during ramp, stop once max is reached */
   double bg_update_period_ramp_a;

   /** Add this factor to update period during ramp, stop once max is reached */
   double bg_update_period_ramp_b;

   /** Quantile value used for background value */
   double bg_quantile_value;

   /** Number of updates per generated key frame */
   UFMF_UINT32_T bg_updates_per_key_frame;

   /** Number of elements in the running windows used to calculate median of quantile values */
   UFMF_UINT32_T bg_median_window_length;

   /** Threshold for storing foreground pixels */
   int diff_threshold;

   /** Number of dilation operations to perform on foreground mask */
   int dilation_rounds;

   /** Maximum fraction of pixels that can be foreground in order for us to compress */
   double max_frac_foreground;

   /** Number of threads */
   int num_threads;

   /** Depth of the queue */
   int queue_depth;
} ufmf_params_t;

/* declare ufmf_writer_t type */
typedef struct _ufmf_writer_t ufmf_writer_t;

/* Function prototypes */
void ufmf_params_initialize(ufmf_params_t* params);

/* before calling ufmf_writer_create someone should call ufmf_log_init
 * from a single thread (this is not a thread safe function)
 */

ufmf_writer_t* ufmf_writer_create(const char *fileName, UFMF_UINT16_T width,
  UFMF_UINT16_T height, const ufmf_params_t* params);
void ufmf_writer_destroy(ufmf_writer_t* writer);
int ufmf_writer_close(ufmf_writer_t* writer);
int ufmf_writer_add(ufmf_writer_t* writer, UFMF_UINT8_T* frame, double timestamp);


#endif


