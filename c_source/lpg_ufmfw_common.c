/*
 * This file contains common functions used by multiple 
 * routines in the lpg_ufmfw suite.
 *
 * This function can be compiled with the following command
 * mex -v -largeArrayDims lpg_ufmfw_common.c ufmf.c 
 */

#include "ufmf.h"
#include "mex.h"
#include <math.h>
#include <stdio.h>
#include <string.h>


/* state information for writer */
typedef struct {
   /* output filename */
   char* filename;

   /* image size */
   UFMF_UINT16_T width;
   UFMF_UINT16_T height;

   /* conversion bufffer */
   UFMF_UINT8_T* im_buffer;

   /* writer */
   ufmf_params_t w_params;
   ufmf_writer_t* writer;
} lpg_ufmfw_context_t;


/* create a global contexts array */
#define LPG_UFMFW_MAX_CONTEXTS 128
static int lpg_ufmfw_global_contexts_is_init = 0;
static lpg_ufmfw_context_t* lpg_ufmfw_global_contexts[LPG_UFMFW_MAX_CONTEXTS];


/* function prototypes */
void lpg_ufmfw_initialize_context(lpg_ufmfw_context_t* context);
void lpg_ufmfw_close_context(lpg_ufmfw_context_t* context);
void lpg_ufmfw_reset(void);
int lpg_ufmwf_get_context(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[],
   unsigned int* ctxt_idx_output, lpg_ufmfw_context_t** ctxt_ptr_output);
void lpg_ufmfw_realize_context(lpg_ufmfw_context_t* context);
void lpg_ufmfw_add_frame(lpg_ufmfw_context_t* context, const mxArray* im_ptr,
   const mxArray* timestamp_ptr);

void lpg_ufmfw_cmd_create_context(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void lpg_ufmfw_cmd_close_context(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);
void lpg_ufmfw_cmd_add_frame(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]);


/* The following function initializes the passed context.
 * This function must be matched to lpg_ufmfw_close_context
 * to ensure that the context can be destroyed at any point.
 *
 * Inputs:
 * context - Pointer to context to initialize
 */
void
lpg_ufmfw_initialize_context(lpg_ufmfw_context_t* context)
{
   context->filename = NULL;
   context->im_buffer = NULL;
   context->writer = NULL;
}


/* The following function closes the passed context, and 
 * releases any internal memory associated with the context.
 *
 * Inputs:
 * context - Pointer to context to destroy
 */
void
lpg_ufmfw_close_context(lpg_ufmfw_context_t* context)
{
   if (context->filename != NULL) {
      free(context->filename);
   }

   if (context->im_buffer != NULL) {
      free(context->im_buffer);
   }

   if (context->writer != NULL) {
      ufmf_writer_close(context->writer);
      ufmf_writer_destroy(context->writer);
   }

   free(context);
}


/* This function resets the interface, closing all open contexts.
 */
void
lpg_ufmfw_reset(void)
{
   unsigned int ctxt_idx;

   for (ctxt_idx = 0; ctxt_idx < LPG_UFMFW_MAX_CONTEXTS; ctxt_idx++) {
      lpg_ufmfw_context_t* ctxt_ptr = lpg_ufmfw_global_contexts[ctxt_idx];

      if (ctxt_ptr != NULL) {
         lpg_ufmfw_close_context(ctxt_ptr);
         lpg_ufmfw_global_contexts[ctxt_idx] = NULL;
      }
   }
}


/* This function retrieves a context from mex arguments, assuming that the
 * the second argument is the context identifier.
 *
 * Inputs:
 * nlhs - Number lefthand side arguments
 * plhs - Pointer to lefthand side arguments
 * nrhs - Number righthand side arguments
 * prhs - Pointer ot righthand side arguments
 * ctxt_idx_output - Pointer to which context id is written
 * ctxt_ptr_output - Pointer to which context pointer is written
 *
 * Returns:
 * Zero on success and one on error.
 */
int
lpg_ufmfw_get_context(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[],
   unsigned int* ctxt_idx_output, lpg_ufmfw_context_t** ctxt_ptr_output)
{
   const double eps_value = 1e-8;
   double* ctxt_arg_ptr;
   unsigned int ctxt_idx;
   const mxArray* mx_ctxt_ptr;
   lpg_ufmfw_context_t* ctxt_ptr;

   /* check type of input argument */
   mx_ctxt_ptr = prhs[1];

   if (mxGetClassID(mx_ctxt_ptr) != mxDOUBLE_CLASS || mxGetImagData(mx_ctxt_ptr) != NULL ||
      mxGetN(mx_ctxt_ptr) != 1 || mxGetM(mx_ctxt_ptr) != 1) {
      return 1;
   }

   /* check that input is integer */
   ctxt_arg_ptr = mxGetPr(mx_ctxt_ptr);
   if (*ctxt_arg_ptr < 0 || *ctxt_arg_ptr > LPG_UFMFW_MAX_CONTEXTS - 1 + eps_value ||
      fabs(*ctxt_arg_ptr - floor(*ctxt_arg_ptr + 0.5)) > eps_value) {
      return 1;
   }

   ctxt_idx = (unsigned int)floor(*ctxt_arg_ptr + 0.5);

   if (ctxt_idx >= LPG_UFMFW_MAX_CONTEXTS) {
      return 1;
   }

   ctxt_ptr = lpg_ufmfw_global_contexts[ctxt_idx];
   if (ctxt_ptr == NULL) {
      return 1;
   }

   if (ctxt_idx_output != NULL) {
      *ctxt_idx_output = ctxt_idx;
   }

   if (ctxt_ptr_output != NULL) {
      *ctxt_ptr_output = ctxt_ptr;
   }

   return 0;
}


/* The following function configures the writer according
 * to the included configuration information.
 *
 * Inputs:
 * context - Context to configure
 */
void
lpg_ufmfw_realize_context(lpg_ufmfw_context_t* context)
{
   context->im_buffer = (UFMF_UINT8_T*)malloc(
     context->width * context->height * sizeof(UFMF_UINT8_T));
   if (context->im_buffer == NULL) {
      mexErrMsgTxt("Unable to allocate memory");
   }

   ufmf_params_initialize(&context->w_params);   

   context->writer = ufmf_writer_create(context->filename, 
     context->width, context->height, &context->w_params);
}


/* The following function adds the frame to the writer
 *
 * Inputs:
 * context - Context to write to
 * im_ptr - mxArray with image data
 * timestamp_ptr - mxArray with timestamp
 */
void
lpg_ufmfw_add_frame(lpg_ufmfw_context_t* context, const mxArray* im_ptr,
   const mxArray* timestamp_ptr)
{
   int status;
   UFMF_UINT8_T* mx_im, *c_buf;
   double timestamp;

   mx_im = mxGetData(im_ptr);
   c_buf = context->im_buffer;

   /* transpose the array */
   for (size_t col = 0; col < context->width; col++) {
      for (size_t row = 0; row < context->height; row++) {
         c_buf[row * context->width + col] = mx_im[col * context->height + row];
      }
   }

   /* retrieve the timestamp value */
   timestamp = *(double*)mxGetData(timestamp_ptr);

   status = ufmf_writer_add(context->writer, c_buf, timestamp);
   if (status) {
      mexErrMsgTxt("Error occurred while adding image to file");
   }
}


/* This function implements a matlab function that creates and
 * configures a context.
 *
 * Inputs:
 * nlhs - Number lefthand side arguments
 * plhs - Pointer to lefthand side arguments
 * nrhs - Number righthand side arguments
 * prhs - Pointer ot righthand side arguments
 *
 * Matlab Inputs:
 * config - Configuration struct
 *
 *  Matlab Outputs:
 *  Context identifier (double scalar)
 *
 *  The configuration structure 'config' has the following form:
 *       filename - string
 */
void
lpg_ufmfw_cmd_create_context(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
   unsigned int ctxt_idx;
   lpg_ufmfw_context_t* context;
   const mxArray* mx_config_ptr;
   unsigned int num_fields, field_idx;
   int has_filename, has_width, has_height;

   /* initialize req argument flags */
   has_filename = 0;
   has_width = 0;
   has_height = 0;

   /* check arguments */
   if (nrhs != 2) {
      mexErrMsgTxt("This function accepts exactly two input arguments");
   }

   if (nlhs != 1) {
      mexErrMsgTxt("This function requires exactly one output argument");
   }

   /* find available context */
   for (ctxt_idx = 0; ctxt_idx < LPG_UFMFW_MAX_CONTEXTS; ctxt_idx++) {
      if (lpg_ufmfw_global_contexts[ctxt_idx] == NULL) {
         break;
      }
   }

   if (ctxt_idx == LPG_UFMFW_MAX_CONTEXTS) {
      mexErrMsgTxt("Limit for number of contexts has been reached");
   }

   /* initialize the context */
   context = (lpg_ufmfw_context_t*)malloc(sizeof(lpg_ufmfw_context_t));
   if (context == NULL) {
      mexErrMsgTxt("Unable to allocate memory");
   }

   lpg_ufmfw_initialize_context(context);

   /* check the config argument */
   mx_config_ptr = prhs[1];
   if (mxGetClassID(mx_config_ptr) != mxSTRUCT_CLASS ||
      mxGetN(mx_config_ptr) != 1 || mxGetM(mx_config_ptr) != 1) {
      lpg_ufmfw_close_context(context);
      mexErrMsgTxt("Argument 'config' must be a scalar structure");
   }

   /* parse each of the fields */
   num_fields = mxGetNumberOfFields(mx_config_ptr);
   for (field_idx = 0; field_idx < num_fields; field_idx++) {
      const char* field_name;
      mxArray* mx_field_ptr;

      field_name = mxGetFieldNameByNumber(mx_config_ptr, field_idx);
      mx_field_ptr = mxGetFieldByNumber(mx_config_ptr, 0, field_idx);

      /* parse the 'filename' field */
      if (!strcmp(field_name, "filename")) {
         size_t slen;

         if (mxGetClassID(mx_field_ptr) != mxCHAR_CLASS ||
             mxGetM(mx_field_ptr) != 1 || mxGetN(mx_field_ptr) < 1) {
            lpg_ufmfw_close_context(context);
            mexErrMsgTxt("Argument 'config.filename' must be a string");
         }

         slen = mxGetN(mx_field_ptr) + 1;
         context->filename = (char*)malloc(slen*sizeof(char));
         if (context->filename == NULL) {
            mexErrMsgTxt("Unable to allocate memory");
         }

         mxGetString(mx_field_ptr, context->filename, slen);
         has_filename = 1;
      } else if (!strcmp(field_name, "width")) {
         double* tmp, val;

         if (mxGetClassID(mx_field_ptr) != mxDOUBLE_CLASS ||
             mxGetImagData(mx_field_ptr) != NULL ||
             mxGetM(mx_field_ptr) != 1 || mxGetN(mx_field_ptr) != 1) {
            lpg_ufmfw_close_context(context);
            mexErrMsgTxt("Argument 'config.width' must be a scalar double");
         }
     
         tmp = mxGetData(mx_field_ptr); 
         val = floor(*tmp + 0.5);

         if (val < 1 || val > (UFMF_UINT16_T)-1) {
            lpg_ufmfw_close_context(context);
            mexErrMsgTxt("Argument 'config.width' is out of bounds");
         }
 
         context->width = (UFMF_UINT16_T)val;
         has_width = 1;
      } else if (!strcmp(field_name, "height")) {
         double* tmp, val;

         if (mxGetClassID(mx_field_ptr) != mxDOUBLE_CLASS ||
             mxGetImagData(mx_field_ptr) != NULL ||
             mxGetM(mx_field_ptr) != 1 || mxGetN(mx_field_ptr) != 1) {
            lpg_ufmfw_close_context(context);
            mexErrMsgTxt("Argument 'config.height' must be a scalar double");
         }
     
         tmp = mxGetData(mx_field_ptr); 
         val = floor(*tmp + 0.5);

         if (val < 1 || val > (UFMF_UINT16_T)-1) {
            lpg_ufmfw_close_context(context);
            mexErrMsgTxt("Argument 'config.height' is out of bounds");
         }
 
         context->height = (UFMF_UINT16_T)val;
         has_height = 1;
       } else {
         lpg_ufmfw_close_context(context);
         mexErrMsgTxt("Unrecognized field name in argument 'config'");
      }
   }

   /* check that all required arguments exist */
   if (!has_filename || !has_width || !has_height) {
       lpg_ufmfw_close_context(context);
       mexErrMsgTxt("Argument config is missing required fields");
   }

   /* all configuration information is parsed, try to actually make it */
   lpg_ufmfw_realize_context(context);

   lpg_ufmfw_global_contexts[ctxt_idx] = context;

   /* return identifier for the context */
   plhs[0] = mxCreateDoubleScalar((double)ctxt_idx);
}


/* This function implements a matlab function that closes a context.
 *
 * Inputs:
 * nlhs - Number lefthand side arguments
 * plhs - Pointer to lefthand side arguments
 * nrhs - Number righthand side arguments
 * prhs - Pointer ot righthand side arguments
 *
 * Matlab Inputs:
 * ctxt - Context identifier (double scalar)
 *
 * Matlab Outputs:
 * None
 */
void
lpg_ufmfw_cmd_close_context(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
   unsigned int ctxt_idx;
   lpg_ufmfw_context_t* ctxt_ptr;

   /* check arguments */
   if (nrhs != 2) {
      mexErrMsgTxt("This function accepts exactly two input arguments");
   }

   if (nlhs != 0) {
      mexErrMsgTxt("This function has no output arguments");
   }
   
   if (lpg_ufmfw_get_context(nlhs, plhs, nrhs, prhs, &ctxt_idx, &ctxt_ptr)) {
      mexErrMsgTxt("Argument 'ctxt' has an invalid value");
   }

   lpg_ufmfw_close_context(ctxt_ptr);
   lpg_ufmfw_global_contexts[ctxt_idx] = NULL;
}


/* This function implements a matlab function that resets the interface.
 * Inputs:
 * nlhs - Number lefthand side arguments
 * plhs - Pointer to lefthand side arguments
 * nrhs - Number righthand side arguments
 * prhs - Pointer ot righthand side arguments
 *
 * Matlab Inputs:
 * None
 *
 * Matlab Outputs:
 * None
 */
void
lpg_ufmfw_cmd_reset(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
   /* check arguments */
   if (nrhs != 1) {
      mexErrMsgTxt("This function does not accept any arguments");
   }

   if (nlhs != 0) {
      mexErrMsgTxt("This function does not have any outputs");
   }

   lpg_ufmfw_reset();
}


/* This function implements a matlab function that adds a frame to the context.
 *
 * Inputs:
 * nlhs - Number lefthand side arguments
 * plhs - Pointer to lefthand side arguments
 * nrhs - Number righthand side arguments
 * prhs - Pointer ot righthand side arguments
 *
 * Matlab Inputs:
 * ctxt - Context identifier (double scalar)
 * im - Image data (uint8 matrix)
 * timestamp - Timestamp (double scalar)
 *
 * Matlab Outputs:
 * None
 */
void
lpg_ufmfw_cmd_add_frame(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
   unsigned int ctxt_idx;
   lpg_ufmfw_context_t* ctxt_ptr;
   const mxArray* im_ptr;
   const mxArray* timestamp_ptr;

   /* check arguments */
   if (nrhs != 4) {
      mexErrMsgTxt("This function accepts exactly four input arguments");
   }

   if (nlhs != 0) {
      mexErrMsgTxt("This function has no output arguments");
   }
   
   if (lpg_ufmfw_get_context(nlhs, plhs, nrhs, prhs, &ctxt_idx, &ctxt_ptr)) {
      mexErrMsgTxt("Argument 'ctxt' has an invalid value");
   }

   /* check arguments */
   im_ptr = prhs[2];
   if (mxGetClassID(im_ptr) != mxUINT8_CLASS ||
       mxGetN(im_ptr) != ctxt_ptr->width ||
       mxGetM(im_ptr) != ctxt_ptr->height) {
      mexErrMsgTxt("Argument 'im' has an invalid value");
   }

   timestamp_ptr = prhs[3];
   if (mxGetClassID(timestamp_ptr) != mxDOUBLE_CLASS ||
       mxGetImagData(timestamp_ptr) != NULL ||
       mxGetM(timestamp_ptr) != 1 || mxGetN(timestamp_ptr) != 1) {
      mexErrMsgTxt("Argument 'timestamp' has an invalid value");
   }

   lpg_ufmfw_add_frame(ctxt_ptr, im_ptr, timestamp_ptr);
}


/* The following function is the main entry point for matlab
 *
 * Inputs:
 * nlhs - Number lefthand side arguments
 * plhs - Pointer to lefthand side arguments
 * nrhs - Number righthand side arguments
 * prhs - Pointer ot righthand side arguments
 */
void
mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
   char* cmd_str;

   /* first thing, initialize contexts if not already done */
   if (!lpg_ufmfw_global_contexts_is_init) {
      unsigned int ctxt_idx;

      for (ctxt_idx = 0; ctxt_idx < LPG_UFMFW_MAX_CONTEXTS; ctxt_idx++) {
         lpg_ufmfw_global_contexts[ctxt_idx] = NULL;
      }

      lpg_ufmfw_global_contexts_is_init = 1;
   }

   /* log the exit function */
   mexAtExit(lpg_ufmfw_reset);

   /* read in the first argument, which is the command string */
   if (nrhs < 1) {
      mexErrMsgTxt("This function requires at least one input argument");
   }

   if (mxGetClassID(prhs[0]) != mxCHAR_CLASS ||
       mxGetM(prhs[0]) != 1 || mxGetN(prhs[0]) < 1) {
      mexErrMsgTxt("First argument must be a string");
   }

   cmd_str = mxArrayToString(prhs[0]);

   if (!strcmp(cmd_str, "create_context")) {
      mxFree(cmd_str);
      lpg_ufmfw_cmd_create_context(nlhs, plhs, nrhs, prhs);
      return;
   } else if (!strcmp(cmd_str, "close_context")) {
      mxFree(cmd_str);
      lpg_ufmfw_cmd_close_context(nlhs, plhs, nrhs, prhs);
      return;
   } else if (!strcmp(cmd_str, "reset")) {
      mxFree(cmd_str);
      lpg_ufmfw_cmd_reset(nlhs, plhs, nrhs, prhs);
      return;
    } else if (!strcmp(cmd_str, "add_frame")) {
      mxFree(cmd_str);
      lpg_ufmfw_cmd_add_frame(nlhs, plhs, nrhs, prhs);
      return;
   } else {
      mxFree(cmd_str);
      mexErrMsgTxt("Unrecognized command string");
   }
}

