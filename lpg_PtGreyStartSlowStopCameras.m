function cams = lpg_PtGreyStartSlowStopCameras(cams)
% LPG_PTGREYSTARTSLOWSTOPCAMERAS Stop the cameras recording
%   This function stops cameras that were started by calling the function
%   lpg_PtGreyStartCameras(), but does not wait for that stop to take
%   effect. The caller should poll the cameras using
%   lpg_PtGreySlowStopCamerasIsDone to see when the cameras have actually
%   stopped. At which point, the caller must invoke
%   lpg_PtGreyFinishSlowStopCameras. This function is useful when the
%   capture threads must be stopped without causing the main matlab thread
%   to enter a wait loop, which could negatively affect background
%   operations.
%
%   This function accepts a single argument 'cams', which is a structure
%   produced by the function lpg_PtGreyStartCameras. 

lpg_ptgrey_common('start_slow_stop_capture', cams.ptgrey_context);

end