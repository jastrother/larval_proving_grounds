function time_base = lpg_applyHeartBeat(time_base, ...
    heartbeat_type, heartbeat_exp, heartbeat)
% This function uses the heart beat calculated by lpg_computeHeartBeat
% in order to calculate a new time_base. It accepts the following
% arguments:
%   time_base - a Nx1 array that contains the current time base.
%       note that because frames may be averaged the time_base may
%       not have the same number of elements as the heartbeat. this
%       function should return the new time for each time given in
%       time_base.
%   heartbeat_type - a string indicating the heartbeat type
%   heartbeat_exp - the expected heartbeat, value depends on 
%      heartbeat_type. If heartbeat_type == 'period', then this
%       should be a scalar value indicating the expected heartbeat
%       period. If heartbeat_type == 'times', then this should be
%       a 1xN array with the rising edge times.
%   heartbeat - a 2xN array calculated using lpg_computeHeartBeat



% Find rising edges in heartbeat
redges_meas_idx = findBeatRisingEdges(heartbeat);
redges_meas_time = heartbeat(1, redges_meas_idx);

switch heartbeat_type
    case 'period'
        time_base = applyHeartBeatPeriod(time_base, heartbeat_exp, ...
            redges_meas_idx, redges_meas_time, heartbeat(1,:)');
    case 'times'
        time_base = applyHeartBeatTimes(time_base, heartbeat_exp, ...
            redges_meas_idx, redges_meas_time, heartbeat(1,:)');
    otherwise
        error 'unregonized heartbeat type';
end

end

function time_base = applyHeartBeatPeriod(time_base, exp_period, ...
    redges_meas_idx, redges_meas_time, old_time)
% This function computes the new time for a fixed period heartbeat.
% It accepts the following inputs:
%   time_base - current time base
%   exp_period - expected period
%   redges_meas_idx - measured heartbeat rising edge index (first before)
%   redges_meas_time - measured heartbeat rising edge time
%   old_time - uncorrected heartbeat time

allowed_timing_err = 0.15;
normalize_to_time_base = true;

% Check that time makes sense
if length(redges_meas_idx) < 2
    error 'not enough edges were found';
end

rel_error = abs(diff(redges_meas_time) - exp_period) / exp_period;
if max(rel_error) > allowed_timing_err
    error 'heartbeat was too far from expectation';
end

% If time_base is more accurate than the heartbeat, then
% adjust period to match time_base
if normalize_to_time_base
    exp_period = median(diff(redges_meas_time));
end

% Adjust timing for prelude
new_time = NaN * old_time;
time_idx = (1:length(old_time));
prelude_mask = time_idx < redges_meas_idx(1);
new_time(prelude_mask) = old_time(prelude_mask) - redges_meas_time(1);

% Adjust timing of each window
for win_idx=1:length(redges_meas_idx)-1
    win_mask = time_idx >= redges_meas_idx(win_idx) & ...
        time_idx < redges_meas_idx(win_idx+1);
    
    found_delta = redges_meas_time(win_idx+1) - redges_meas_time(win_idx);
    new_time(win_mask) = (old_time(win_mask) - redges_meas_time(win_idx)) ...
        * exp_period / found_delta + exp_period * (win_idx-1);
end

% Adjust timing for postlude
postlude_mask = time_idx >= redges_meas_idx(end);
new_time(postlude_mask) = old_time(postlude_mask) ...
    - redges_meas_time(end) + exp_period * (length(redges_meas_idx) - 1);

if nnz(isnan(new_time)) > 0
    error 'internal error';
end

% Recalculate time base using new clock
time_base = interp1(old_time, new_time, time_base);

end


function time_base = applyHeartBeatTimes(time_base, exp_times, ...
    redges_meas_idx, redges_meas_time, old_time)
% This function computes the new time for a fixed times hearteat.
% It accepts the following inputs:
%   time_base - current time base
%   exp_times - expected times
%   redges_meas_idx - measured heartbeat rising edge index
%   redges_meas_time - measured heartbeat rising edge time
%   old_time - uncorrected heartbeat time

allowed_timing_err = 0.15;

% Check that time makes sense
if nnz(size(redges_meas_time) - size(exp_times)) ~= 0
    error 'heartbeat does not match expectation';
end

if length(exp_times) > 1
    rel_error = abs(diff(redges_meas_time) - diff(exp_times)) ./ abs(diff(exp_times));
    if max(rel_error) > allowed_timing_err
        error 'heartbeat was too far from expectation';
    end
end

% Adjust timing for prelude (use first edge as zero)
new_time = NaN * old_time;
time_idx = (1:length(old_time));
prelude_mask = time_idx < redges_meas_idx(1);
new_time(prelude_mask) = old_time(prelude_mask) - redges_meas_time(1);

% Adjust timing of each window
for win_idx=1:length(redges_meas_idx)-1
    win_mask = time_idx >= redges_meas_idx(win_idx) & ...
        time_idx < redges_meas_idx(win_idx+1);
    
    found_delta = redges_meas_time(win_idx+1) - redges_meas_time(win_idx);
    exp_period = exp_times(win_idx+1) - exp_times(win_idx);
    
    new_time(win_mask) = (old_time(win_mask) - redges_meas_time(win_idx)) ...
        * exp_period / found_delta + exp_times(win_idx) - exp_times(1);
end

% Adjust timing for postlude
postlude_mask = time_idx >= redges_meas_idx(end);
new_time(postlude_mask) = old_time(postlude_mask) ...
    - redges_meas_time(end) + exp_times(end) - exp_times(1);

% Adjust timing to put first edge back in place
new_time = new_time + exp_times(1);

if nnz(isnan(new_time)) > 0
    error 'internal error';
end

% Recalculate time base using new clock
time_base = interp1(old_time, new_time, time_base);

end

function h_edges = findBeatRisingEdges(heartbeat)
% This functions scans through the heartbeat in order to find
% rising edges.

ht = heartbeat(1,:);
hv = heartbeat(2,:);

% Find cut values. First find middle of range, then find high
% value as median of values above middle of range, then find low
% value as median of values below middle of range, then take cutoffs
% around middle third.
hv_max = max(hv);
hv_min = min(hv);
hv_mid = 0.5*(hv_min + hv_max);

hv_high = median(hv(hv > hv_mid));
hv_low = median(hv(hv < hv_mid));

hv_low_cut = hv_low + (hv_high - hv_low)/3;
hv_high_cut = hv_high - (hv_high - hv_low)/3;

% Apply hysteretic cutoffs
state = zeros(1, length(hv));
cur_state = false;
for idx=1:length(hv)
    if ~cur_state && hv(idx) > hv_high_cut
        cur_state = true;
    elseif cur_state && hv(idx) < hv_low_cut
        cur_state = false;
    end
    
    state(idx) = cur_state;
end

% Find the edges
h_edges = find(~state(1:end-1) & state(2:end));

end

