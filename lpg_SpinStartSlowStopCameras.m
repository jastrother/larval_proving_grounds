function cams = lpg_SpinStartSlowStopCameras(cams)
% LPG_SPINSTARTSLOWSTOPCAMERAS Stop the cameras recording
%   This function stops cameras that were started by calling the function
%   lpg_SpinStartCameras(), but does not wait for that stop to take
%   effect. The caller should poll the cameras using
%   lpg_SpinSlowStopCamerasIsDone to see when the cameras have actually
%   stopped. At which point, the caller must invoke
%   lpg_SpinFinishSlowStopCameras. This function is useful when the
%   capture threads must be stopped without causing the main matlab thread
%   to enter a wait loop, which could negatively affect background
%   operations.
%
%   This function accepts a single argument 'cams', which is a structure
%   produced by the function lpg_SpinStartCameras. 

lpg_spin_common('start_slow_stop_capture', cams.spin_context);

end