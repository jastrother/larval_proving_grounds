function bounds = lpg_getMaskBounds(mask)
%LPG_GETMASKBOUNDS Calculate bounding box for the mask
%   This function processes a logical image, and returns the
%   bounding box. The bounding box is given as
%   [first_row, height, first_column, width]

    if nnz(mask) == 0
        bounds = [1, 0, 1, 0];
        return;
    end

    col_vals = sum(mask,1) > 0;
    row_vals = sum(mask,2) > 0;
    
    first_row = find(row_vals, 1,'first');
    first_col = find(col_vals, 1, 'first');
    
    width = find(col_vals,1,'last') - first_col + 1;
    height = find(row_vals,1,'last') - first_row + 1;
    

    bounds = [first_row, height, first_col, width];
end

