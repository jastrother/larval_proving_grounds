function stretches = lpg_findTrueStretches(y, min_size)
% This function finds contiguous stretches of true values in the passed
% array. The input vector y must be a row vector of logical values.
% stretches less than min_size elements are discarded. This function
% returns stretches, which is a [2xN] array where the first row is
% the start index of the stretch and the row column is the last
% index of the stretch.

if isempty(y)
    stretches = [];
    return;
end

if ~isrow(y) || ~islogical(y)
    error 'y has invalid dimensions or type';
end

% pad y with false values
y = [false, y, false];

% find locations of stretch start/finish
stretches = [...
    find(~y(1:end-2) & y(2:end-1)); ...
    find(y(2:end-1) & ~y(3:end)) ];

if ~isempty(stretches)
    % removal small stretches
    stretch_len = stretches(2,:) - stretches(1,:) + 1;
    stretches(:,stretch_len < min_size) = [];
end

end

