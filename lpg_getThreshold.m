function best_level = lpg_getThreshold(cfg, header, bkg_im, max_value, num_frames, masks)
% Calculate the image threshold for the image. This function accepts
% the following arguments:
%   cfg - configuration parameters
%       thresh_skip - number of frames in median group
%       thresh_size - number of dark pixels in larva
%       thresh_tol - tolerance for threshold finding
%   header - ufmf header file
%   bkg_im - background iamge
%   max_value - maximum pixel value (e.g., 255 for uint8)
%   num_frames - number of frames to process
%   masks - masks to use (empty for no mask)

if isempty(masks)
    masks = ones(size(bkg_im,1), size(bkg_im,2));
end

% solve (num_samples-1)*skip + 1 <= num_frames
num_samples = floor((num_frames - 1)/cfg.thresh_skip) + 1;
levels = zeros(numel(masks), num_samples);

for iter_idx= 0:num_samples-1
    frame_idx = iter_idx * cfg.thresh_skip + 1;
    
    % read frame
    im = ufmf_read_frame(header, frame_idx);

    % subtract background and shift to positive values
    im = double(im)/max_value - bkg_im;
    im = 0.5*(im + 1);

    % threshold image
    for mask_idx=1:numel(masks)
        im_mask = masks(mask_idx).im;
        
        %levels(mask_idx, iter_idx+1) = graythresh(im(im_mask));
        levels(mask_idx, iter_idx+1) = calc_thresh(im(im_mask), ...
            cfg.thresh_size, cfg.thresh_tol);
    end
    
end

best_level = median(levels,2);

end


function test_thresh = calc_thresh(vals, expected_num, thresh_tol)
% This function finds the threshold by finding the threshold
% that produces the "expected_num" of dark pixels. This works
% for cases where we are searching for an object of approximately
% known size.

low_thresh = min(vals);
high_thresh = max(vals);
test_thresh = 0.5*(low_thresh + high_thresh);

while true
    if high_thresh - low_thresh < thresh_tol
        break;
    end
    
    if nnz(vals < test_thresh) < expected_num
        low_thresh = test_thresh;
    else
        high_thresh = test_thresh;
    end

    test_thresh = 0.5*(low_thresh + high_thresh);
end

end