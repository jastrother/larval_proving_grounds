function lpg_getLarvaTrajectory3d(file1, file2, output)
%LPG_GETLARVATRAJECTORY3D Get 3D trajectory from two camera views
%   This function reads in two ufmf files and then saves and output
%   file that contains the position of the larva in 3D space.

error 'update this code to use background movie';

cfg.show_progress = true;
cfg.prog_num_bars = 30;
cfg.bkg_quantile = 0.9;
cfg.bkg_group_skip = 50;
cfg.bkg_group_size = 500;
cfg.thresh_skip = 500;
cfg.thresh_size = 800;
cfg.thresh_tol = 0.01;
cfg.parallel_info.show_progress = true;
cfg.parallel_info.in_parallel = true;
cfg.parallel_info.block_size = 50000;
cfg.parallel_info.chunk_size = 500;

% check if output already exists
if exist(output, 'file')
    error 'output file already exists';
end

% read in movie headers
header1 = ufmf_read_header(file1);
header2 = ufmf_read_header(file2);

num_frames = min([header1.nframes, header2.nframes]);

% find the image type
switch header1.dataclass
    case 'uint8'
        max_value = 255;
    otherwise
        error 'unrecognized data class';
end

if ~strcmp(header1.dataclass, header2.dataclass)
    error 'data class for images must match';
end

% calulate the background images
if cfg.show_progress
    fprintf(1, 'Computing background images\n');
end

bkg_im1 = lpg_calculateBackground(cfg, header1, max_value, num_frames);
bkg_im2 = lpg_calculateBackground(cfg, header2, max_value, num_frames);

% Calculate the image threshold
if cfg.show_progress
    fprintf(1, 'Computing image thresholds\n');
end

thresh_value1 = lpg_getThreshold(cfg, header1, bkg_im1, max_value, num_frames);
thresh_value2 = lpg_getThreshold(cfg, header2, bkg_im2, max_value, num_frames);

% Find larva in each frame
if cfg.show_progress
    fprintf(1, 'Calculating larva positions\n');
end

if ~cfg.parallel_info.in_parallel
    udata.header1 = header1;
    udata.header2 = header2;
else
    udata.header1 = file1;
    udata.header2 = file2;
end
    
udata.bkg_im1 = bkg_im1;
udata.bkg_im2 = bkg_im2;
udata.thresh_value1 = thresh_value1;
udata.thresh_value2 = thresh_value2;
udata.max_value = max_value;
    
raw_output = lpg_doForEach(cfg.parallel_info, ...
    @(start, num) processFrames(cfg, udata, start, num), ...
    1, num_frames);
raw_pos1 = raw_output(1:2,:);
raw_pos2 = raw_output(3:4,:);
area1 = raw_output(5,:);
area2 = raw_output(6,:);
    
save(output, 'bkg_im1', 'bkg_im2', 'raw_pos1', 'raw_pos2', 'area1', 'area2');

end

function raw_output = processFrames(cfg, udata, start_frame, num_frames)
% This function processes the passed group of frames

have_fid1 = false;
if ischar(udata.header1)
    udata.header1 = ufmf_read_header(udata.header1);
    have_fid1 = true;
end

have_fid2 = false;
if ischar(udata.header2)
    udata.header2 = ufmf_read_header(udata.header2);
    have_fid2 = true;
end

raw_output = zeros(6, num_frames);

for frame_idx=start_frame:start_frame+num_frames-1
    
    % read frame
    im1 = ufmf_read_frame(header1, frame_idx);
    im2 = ufmf_read_frame(header2, frame_idx);

    % subtract background and shift to positive values
    im1 = double(im1)/max_value - udata.bkg_im1;
    im1 = 0.5*(im1 + 1);
    
    im2 = double(im2)/max_value - udata.bkg_im2;
    im2 = 0.5*(im2 + 1);
    
    local_idx = frame_idx-start_frame+1;
    
    [raw_output(1:2,local_idx), raw_output(5,local_idx)] = ...
        lpg_findLarvaCentroid(cfg, im1, udata.thresh_value1, []);
    
    [raw_output(3:4,local_idx), raw_output(6,local_idx)] = ...
        lpg_findLarvaCentroid(cfg, im2, udata.thresh_value2, []);

end

if have_fid1
    fclose(udata.header1.fid);
end

if have_fid2
    fclose(udata.header2.fid);
end

end



