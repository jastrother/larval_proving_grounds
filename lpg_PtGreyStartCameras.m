function cameras = lpg_PtGreyStartCameras(cfg)
% LPG_PTGREYSTARTCAMERAS Start the cameras recording
%   This function starts the two cameras running and shows a preview
%   of the feed in a figure window.
%
%   This function accepts an optional argument cfg that provides
%   configuration information. Most of these are derived directly
%   from the FlyCapture SDK, so more information on each can be
%   found there. The following fields are recognized, and unrecognized
%   fields produce errors. Unless otherwise noted, fields can be left
%   empty and default values will be used:
%      cameras - Mandatory 1xN structure array with the following fields:
%         dev_id - Device ID (integer)
%         mode - Camera capture mode (integer)
%         offsetX - ROI X offset (integer)
%         offsetY - ROI Y offset (integer)
%         width - ROI width (integer)
%         height - ROI height (integer)
%         framerate - Percent of maximum (0 to 100, floating-point)
%         pixel_format - Pixel format ('mono8', or 'raw8')
%         frames_per_trigger - Frames per trigger (integer)
%         start_paused - Start camera in paused mode (0 or 1)
%         trigger_enabled - Set trigger enabled (0 or 1)
%         trigger_mode - Trigger mode (integer)
%         trigger_source - Trigger source (gpio pin, integer)
%         trigger_polarity - Trigger polarity (0 for low or 1 for high)
%         strobe_enabled - Set strobe enabled (0 or 1)
%         strobe_source - Strobe output (gpio pin, integer)
%         strobe_polarity - Strobe polarity (0 for low or 1 for high)
%         output_type - Output file type ('ufmf' or 'avi')
%         output_file - Output file name to save data
%         callback_period - Callback period (integer)
%
%      preview_figure - Preview figure created by lpg_createDisplay
%
%   The cell array cam_device_udata contains a struct array with
%   the following fields:
%
%   This functions a structure with the following fields:
%      cfg - Configuration information
%      ptgrey_context - Point grey context identifier
%      preview_figure - Preview figure (see lpg_createDisplay)
%   

% Check the input arguments and assign defaults
if nargin >= 1
    if ~isstruct(cfg) || numel(cfg) ~= 1
        error 'cfg has invalid type';
    end
else
    cfg = [];
end

% Create the display
if ~isfield(cfg, 'preview_figure')
   disp_cfg.num_images = length(cfg.cameras);
   preview_figure = lpg_createDisplay(disp_cfg);
else
   preview_figure = cfg.preview_figure;
   
   % Cleanse non-lpg_ptgrey parameters from config
    cfg = rmfield(cfg, 'preview_figure');
end

% Configure callback
for cam_idx=1:length(cfg.cameras)
    cfg.cameras(cam_idx).callback_fcn = ...
        @(im) local_callback(preview_figure, cam_idx, im);
end

% Configure the cameras
ptgrey_context = lpg_ptgrey_common('create_context', cfg);

% Start capturing
lpg_ptgrey_common('start_capture', ptgrey_context);

% Push video objects to the output
cameras.cfg = cfg;
cameras.ptgrey_context = ptgrey_context;
cameras.preview_figure = preview_figure;

end

function stop_flag = local_callback(preview_fig, cam_idx, im)
set(preview_fig.UserData.win_images{cam_idx}, 'CData', im);
caxis(preview_fig.UserData.win_axes{cam_idx}, [0, 255]);
preview_fig.UserData.updateImageDims();

if preview_fig.UserData.term_activated > 0
    stop_flag = 1;
else
    stop_flag = 0;
end

end


