function mask_array = lpg_selectCircularMaskArray(im, nrows, ncols)
% This function selects an array of circular mask using the input image
% as a guide. This function accepts one parameter:
%
%   im - input image
%   nrows - number of rows
%   ncols - number of columns
%
% This function has one output:
%
%   mask_array - output masks

im_modified = repmat(im, [1, 1, 3]);

fig = figure;
ax = axes('Parent', fig);

% preallocate struct
mask_array(nrows,ncols).im = [];
mask_array(nrows,ncols).params = [];

while true
    hold(ax, 'off');
    imshow(im_modified, 'Parent', ax);

    corner_info(4).origin = [];
    corner_info(4).radius = [];

    if nrows == 1 && ncols == 1
        title(ax, 'Select 3pts on well');
        
        while true
            [pts_x, pts_y] = getpts(fig);
            if length(pts_x) ~= 3
                continue;
            else
                break;
            end
        end
        
        pts = [pts_x'; pts_y'];
        
        [circ_origin,circ_radius] = solve_circle_from_3pts(...
            pts(:,1), pts(:,2), pts(:,3));
        
        for corner_idx=1:4
            corner_info(corner_idx).origin = circ_origin;
            corner_info(corner_idx).radius = circ_radius;
        end
    elseif nrows == 1
        for col_idx=1:2
            switch col_idx
                case 1
                    title(ax, 'Select 3pts on leftmost well');
                case 2
                    title(ax, 'Select 3pts on rightmost well');
            end

            while true
                [pts_x, pts_y] = getpts(fig);
                if length(pts_x) ~= 3
                    continue;
                else
                    break;
                end
            end

            pts = [pts_x'; pts_y'];

            [circ_origin,circ_radius] = solve_circle_from_3pts(...
                pts(:,1), pts(:,2), pts(:,3));

            corner_info(col_idx).origin = circ_origin;
            corner_info(col_idx).radius = circ_radius;
        end
        
        for corner_idx=3:4
            corner_info(corner_idx).origin = corner_info(corner_idx-2).origin;
            corner_info(corner_idx).radius = corner_info(corner_idx-2).radius;
        end
     elseif ncols == 1
        for row_idx=1:2
            switch row_idx
                case 1
                    title(ax, 'Select 3pts on topmost well');
                case 2
                    title(ax, 'Select 3pts on bottommost well');
            end

            while true
                [pts_x, pts_y] = getpts(fig);
                if length(pts_x) ~= 3
                    continue;
                else
                    break;
                end
            end

            pts = [pts_x'; pts_y'];

            [circ_origin,circ_radius] = solve_circle_from_3pts(...
                pts(:,1), pts(:,2), pts(:,3));

            corner_info(2*(row_idx-1)+1).origin = circ_origin;
            corner_info(2*(row_idx-1)+1).radius = circ_radius;
        end
        
        for corner_idx=2:2:4
            corner_info(corner_idx).origin = corner_info(corner_idx-1).origin;
            corner_info(corner_idx).radius = corner_info(corner_idx-1).radius;
        end
    else
        for corner_idx=1:4
            switch corner_idx
                case 1
                    title(ax, 'Select 3pts on well of top-left corner');
                case 2
                    title(ax, 'Select 3pts on well of top-right corner');
                case 3
                    title(ax, 'Select 3pts on well of bottom-left corner');
                case 4
                    title(ax, 'Select 3pts on well of bottom-right corner');
            end

            while true
                [pts_x, pts_y] = getpts(fig);
                if length(pts_x) ~= 3
                    continue;
                else
                    break;
                end
            end

            pts = [pts_x'; pts_y'];

            [circ_origin,circ_radius] = solve_circle_from_3pts(...
                pts(:,1), pts(:,2), pts(:,3));

            corner_info(corner_idx).origin = circ_origin;
            corner_info(corner_idx).radius = circ_radius;
        end
    end
        
    total_origin = corner_info(1).origin;
    basis1a = corner_info(2).origin - corner_info(1).origin;
    basis1b = corner_info(4).origin - corner_info(3).origin;
    basis2 = corner_info(3).origin - corner_info(1).origin;

    radius_base = corner_info(1).radius;
    radius_diff1a = corner_info(2).radius - corner_info(1).radius;
    radius_diff1b = corner_info(4).radius - corner_info(3).radius;
    radius_diff2 = corner_info(3).radius - corner_info(1).radius;

    row_pos = linspace(0, 1, nrows);
    col_pos = linspace(0, 1, ncols);
    
    for row_idx=1:nrows
        for col_idx=1:ncols

            x_pos = col_pos(col_idx);
            y_pos = row_pos(row_idx);

            well_pos = total_origin + basis2 * y_pos + ...
                (basis1a * (1-y_pos) + basis1b * y_pos) * x_pos;

            well_radius = radius_base + radius_diff2 * y_pos + ...
                (radius_diff1a * (1-y_pos) + radius_diff1b * y_pos) * x_pos;

            params.pos = well_pos;
            params.radius = well_radius;
            
            mask_array(row_idx, col_idx).params = params;
            
            hold(ax, 'on');
            draw_circle(ax, well_pos, well_radius);
        end
    end
    
    title(ax, 'Click Enter to accept or Esc to re-try');
    while true
        wait_btn = waitforbuttonpress;
        if wait_btn == 0
            continue;
        end
        
        wait_btn = uint8(get(fig, 'CurrentCharacter'));
        break;
    end

    switch wait_btn
        case 13
            % Enter
            break;
        otherwise
            continue;
    end
        
end


close(fig);

% construct the image masks
[X,Y] = meshgrid(1:size(im,2), 1:size(im,1));

for row_idx=1:nrows
    for col_idx=1:ncols
        
        params = mask_array(row_idx,col_idx).params;
        im_mask = (X-params.pos(1)).^2 + (Y-params.pos(2)).^2 < params.radius.^2;
        
        mask_array(row_idx,col_idx).im = im_mask;
        
    end
end

end

function [origin,r] = solve_circle_from_3pts(pt1, pt2, pt3)
% This function finds the radius and center of a circle
% that goes from pt1 to pt2 through pt3. 

% Find 2D basis
origin = (pt1 + pt2 + pt3)/3;

basis1 = pt1 - origin;
basis1 = basis1 / norm(basis1);

basis2 = pt2 - origin;
basis2 = basis2 - dot(basis1,basis2)*basis1;
basis2 = basis2 / norm(basis2);

% Solve for center and radius
system_A = zeros(3);
system_b = zeros(3,1);

for pt_idx=1:3
    switch pt_idx
        case 1
            pt = pt1;
        case 2
            pt = pt2;
        case 3
            pt = pt3;
    end
    
    x = dot(pt - origin, basis1);
    y = dot(pt - origin, basis2);

    system_A(pt_idx,1) = x;
    system_A(pt_idx,2) = y;
    system_A(pt_idx,3) = 1;
    system_b(pt_idx) = x.^2 + y.^2;
end

coefs = linsolve(system_A, system_b);
x0 = 0.5 * coefs(1);
y0 = 0.5 * coefs(2);
r = sqrt(coefs(3) + x0.^2 + y0.^2);

new_origin = origin + x0 * basis1 + y0 * basis2;

origin = new_origin;

end

function draw_circle(ax, pos, radius)

theta = linspace(0, 2*pi, 100);
x = pos(1) + radius * cos(theta);
y = pos(2) + radius * sin(theta);

h = plot(ax, x, y, 'r-', 'LineWidth', 2);

end




