function [kinematics,time_base] = lpg_analyzeLarvaTrajectoriesTimeDiv(traj_fname, ...
    mask_fname, subr_fname, outlier_masks, cfg)
%LPG_ANALYZELARVATRAJECTORIESTIMEDIV Calculate properties of trajectory
%   This function calculates the mean translational and angular velocity
%   within windows for the passed trajectory file and mask file. The mask
%   is used only to find the distance to the wall of the chamber. If
%   subregions exist, then the quantities are calculated for each
%   subregion. If subr_fname is empty, then the quantities are calculated
%   without subregions. Any mask numbers listed in outlier_wells will be
%   omitted from the analysis, and NaN arrays will be output instead. 
%   In the output, the rows represent the different masks and the columns
%   represent the the different subregions.

% The configuration has the following properties.
%   cutoff_freq - frequency to use for filtering x/y coordinates
%   min_time - minimum time in subregion for data to count toward it
%   min_stretch - larval disappearances can leave gaps in the data,
%       only stretches with min_stretch points are analyzed
%   snr_high - minimum signal to noise ratio for larva coordinates
%       to be considered high confidence
%   snr_med - minimum signal to noise ratio for larva coordinates
%       to be considered medium confidence
%   allow_dist_error - allowable distance error to be  used when
%       determining if medium confidence points are usable
%   switch_hysteresis - larvae must spend a minimum amount of time
%       in a subregion for a switch to a new subregion to count
%   window_type - string specifying window type, can be 'period' of
%       'times'
%   window_period - number of seconds in each window, or empty to use
%       single window, used only if window_type is 'period'
%   window_times - Nx2 array of windows, first column is the start of
%       the window, second column is the duration of the window, only
%       used if window_type is 'times'.
%   partial_window_thresh - scalar value between 0 and 1, indicating
%       the fraction of the window for which data must be present for
%       the data to be averaged and included in the output.
%   allowed_missing_fraction - fraction of frames allowed to drop
%   heartbeat_type - type of heartbeat signal
%   heartbeat_expect - expected times for heartbeat
%   heartbeat_vals - found signal for heartbeat

if isempty(cfg)
    cfg.cutoff_freq = 5;
    cfg.min_time = 5;
    cfg.min_stretch = 15;
    cfg.snr_high = 0.5;
    cfg.snr_med = 0.4;
    cfg.allow_dist_error = 10; % pixels
    cfg.switch_hysteresis = 5;
    cfg.window_type = 'period';
    cfg.window_period = 10;
    cfg.window_times = [];
    cfg.partial_window_thresh = 0.90;
    cfg.allowed_missing_fraction = 0.05;
    cfg.heartbeat_type = 'none';
    cfg.heartbeat_expect = [];
    cfg.heartbeat_vals = [];
end

load(traj_fname, 'time');
load(traj_fname, 'raw_pos');
load(traj_fname, 'xcorr_signal');

load(mask_fname, 'masks');

if isempty(subr_fname)
    num_subr = 1;
else
    load(subr_fname, 'subregions');
    
    switch subregions(1).type
        case 'linear'
            num_subr = 2;
        otherwise
            error 'unrecognized subregion type';
    end
end

num_tpoints = size(raw_pos, 3);
num_masks = size(raw_pos, 2);

kinematics(num_masks, num_subr).trans_vel = [];
kinematics(num_masks, num_subr).ang_vel = [];
kinematics(num_masks, num_subr).total_time = [];
kinematics(num_masks, num_subr).wall_dist = [];
kinematics(num_masks, num_subr).area_use = [];
kinematics(num_masks, num_subr).switch_rate = [];
kinematics(num_masks, num_subr).explor_index = [];

% apply heartbeat if requested
if ~strcmp(cfg.heartbeat_type, 'none')
    time = lpg_applyHeartBeat(time, cfg.heartbeat_type, ...
        cfg.heartbeat_expect, cfg.heartbeat_vals);
end


% find duration of each time step
time_step = diff(time);
time_step = [time_step; median(time_step)];   

% check if time overlaps with windows, otherwise return empty arrays
if strcmp(cfg.window_type, 'times')
    trimmed_window_times = [];
    
    for window_idx=1:size(cfg.window_times,1)
        time_mask = time >= cfg.window_times(window_idx, 1) & ...
            time <= cfg.window_times(window_idx, 1) + ...
            cfg.window_times(window_idx, 2);
        
        if nnz(time_mask) > 0
            trimmed_window_times = [trimmed_window_times; ...
                cfg.window_times(window_idx, :)];
        end
    end
    
    if isempty(trimmed_window_times)
        % if no windows match, return empty kinematics and time base
        time_base = [];
        return;
    end
end

% step through each mask
for area_idx=1:num_masks
    
    switch cfg.window_type
        case 'period'
            if ~isempty(cfg.window_period)
                total_time = max(time) - min(time);                
                full_windows = floor(total_time/cfg.window_period);
                partial_window_frac = (total_time - ...
                    full_windows*cfg.window_period)/cfg.window_period;
                
                num_windows = full_windows;
                
                if partial_window_frac > cfg.partial_window_thresh
                    num_windows = num_windows + 1;
                end
            else
                num_windows = 1;
            end
        case 'times'
            num_windows = size(cfg.window_times, 1);
        otherwise
            error 'unrecognized window_type';
    end
    
    % output NaN if area is flagged as outlier
    if ismember(area_idx, outlier_masks)
        for subr_idx=1:num_subr
            kinematics(area_idx,subr_idx).trans_vel = NaN * zeros(1, num_windows);
            kinematics(area_idx,subr_idx).ang_vel = NaN * zeros(1, num_windows);
            kinematics(area_idx,subr_idx).total_time = NaN * zeros(1, num_windows);
            kinematics(area_idx,subr_idx).wall_dist = NaN * zeros(1, num_windows);
            kinematics(area_idx,subr_idx).area_use = NaN * zeros(1, num_windows);
            kinematics(area_idx,subr_idx).switch_rate = NaN * zeros(1, num_windows);
            kinematics(area_idx,subr_idx).explor_index = NaN * zeros(1, num_windows);
        end
        continue;
    end
    
    % find wall distance function
    im_mask = masks(area_idx).im;
    wall_dist_map = bwdist(edge(im_mask));
    wall_dist_fun = griddedInterpolant({1:size(im_mask,1), ...
        1:size(im_mask,2)}, wall_dist_map, 'nearest');
    
    % retrieve data for well
    x_well = squeeze(raw_pos(1,area_idx,:));
    y_well = squeeze(raw_pos(2,area_idx,:));
    snr_well = xcorr_signal(area_idx,:)';
    
    % pre-allocated cleaned values
    x_clean = NaN * zeros(num_tpoints, 1);
    y_clean = NaN * zeros(num_tpoints, 1);
    trans_vel_clean = NaN * zeros(num_tpoints, 1);
    ang_vel_clean = NaN * zeros(num_tpoints, 1);
    wall_dist_clean = NaN * zeros(num_tpoints, 1);
    
    % check for poor coverage
    usable_mask = lpg_findUsableTrajectoryPoints(time, x_well, y_well, ...
        snr_well, cfg);
    
    missing_frac = 1 - nnz(usable_mask)/length(snr_well);
    if missing_frac > cfg.allowed_missing_fraction
        warning('trajectory %s area %d is missing %f %% of frames', ...
            traj_fname, area_idx, missing_frac*100);
    end
    
    % find contiguous stretches
    stretches = lpg_findTrueStretches(usable_mask', cfg.min_stretch);
    
    % analyze each contiguous stretch
    for stretch_idx=1:size(stretches,2)
        stretch_first = stretches(1,stretch_idx);
        stretch_last = stretches(2,stretch_idx);
        
        x_stretch = x_well(stretch_first:stretch_last);
        y_stretch = y_well(stretch_first:stretch_last);
        time_stretch = time(stretch_first:stretch_last);
        
        % filter raw x and y values
        delta_t = mean(diff(time_stretch));
        x_filt = lpg_filterTimeSeries(delta_t, x_stretch', cfg.cutoff_freq);
        y_filt = lpg_filterTimeSeries(delta_t, y_stretch', cfg.cutoff_freq);
        
        % create splines from filtered x and y positions
        x_sp = csapi(time_stretch, x_filt);
        y_sp = csapi(time_stretch, y_filt);
        
        % take derivative of spline for velocity
        x_sp_vel = fnder(x_sp);
        y_sp_vel = fnder(y_sp);
        
        % output into a vector instead of a struct
        x_vel = fnval(time_stretch, x_sp_vel);
        y_vel = fnval(time_stretch, y_sp_vel);
        trans_vel = sqrt(x_vel.^2 + y_vel.^2);
        
        % calculate angular veloctiy
        heading = unwrap(atan2(y_vel, x_vel));
        heading_sp = csapi(time_stretch, heading);
        ang_vel_sp = fnder(heading_sp);
        ang_vel = fnval(time_stretch, ang_vel_sp);
        
        % calculate the wall distance
        wall_dist = wall_dist_fun(y_filt, x_filt);

        x_clean(stretch_first:stretch_last) = x_filt;
        y_clean(stretch_first:stretch_last) = y_filt;
        trans_vel_clean(stretch_first:stretch_last) = trans_vel;
        ang_vel_clean(stretch_first:stretch_last) = ang_vel;
        wall_dist_clean(stretch_first:stretch_last) = wall_dist;
    end
    
    averaged_trans_vel = zeros(num_subr, num_windows);
    averaged_ang_vel = zeros(num_subr, num_windows);
    averaged_total_time = zeros(num_subr, num_windows);
    averaged_wall_dist = zeros(num_subr, num_windows);
    averaged_area_use = zeros(num_subr, num_windows); 
    averaged_switch_rate = zeros(num_subr, num_windows);
    averaged_explor_index = zeros(num_subr, num_windows);
    
    % create logical indicating if larvae is on the left or the right
    if isempty(subr_fname)
        switch_state = zeros(size(time));
    else
        switch subregions(area_idx).type
            case 'linear'
                line_n = subregions(area_idx).params(1:2);
                line_b = subregions(area_idx).params(3);
                xy_dot = (x_clean * line_n(1)) + (y_clean * line_n(2));
                divider_pos = xy_dot - line_b;
                on_left = divider_pos < 0; % false for NaN
                on_right = divider_pos > 0; % false for NaN

                switch_state = countSwitches(time, double(on_left), cfg.switch_hysteresis);
            otherwise
                error 'unrecognized subregion type';
        end
    end
    
    % step through each window
    for window_idx=1:num_windows
        
        switch cfg.window_type
            case 'period'
                if ~isempty(cfg.window_period)
                    start_time = min(time) + (window_idx-1) * cfg.window_period;
                    end_time = start_time + cfg.window_period;
                    time_mask = time >= start_time & time < end_time;
                else
                    start_time = min(time);
                    end_time = max(time);
                    time_mask = true(size(time));
                end
            case 'times'
                start_time = cfg.window_times(window_idx, 1);
                end_time = start_time + cfg.window_times(window_idx, 2);
                time_mask = time >= start_time & time < end_time;
            otherwise
                error 'unrecognized window_type';
        end
        
        if isempty(subr_fname)
            averaged_trans_vel(window_idx) = median(trans_vel_clean(time_mask), 'omitNaN');
            averaged_ang_vel(window_idx) = median(abs(ang_vel_clean(time_mask)), 'omitNaN');
            averaged_total_time(window_idx) = end_time - start_time;
            averaged_wall_dist(window_idx) = median(wall_dist_clean(time_mask), 'omitNaN');
            averaged_area_use(window_idx) = calculateAreaUse(x_clean(time_mask), ...
                y_clean(time_mask)) / (end_time - start_time);
            averaged_switch_rate(window_idx) = 0;
            averaged_explor_index(window_idx) = 0;
        else
            switch subregions(area_idx).type
                case 'linear'
                    div_total_time_left = sum(time_step(time_mask & on_left));
                    div_total_time_right = sum(time_step(time_mask & on_right));
                    
                    if div_total_time_left < cfg.min_time
                        div_trans_vel_left = NaN;
                        div_ang_vel_left = NaN;
                        div_wall_dist_left = NaN;
                        div_area_use_left = NaN;
                    else
                        div_trans_vel_left = median(...
                            trans_vel_clean(time_mask & on_left), 'omitnan');
                        
                        %div_trans_vel_left = max(...
                        %    trans_vel_clean(time_mask & on_left), [], 'omitnan');
                        
                        div_ang_vel_left = median(...
                            abs(ang_vel_clean(time_mask & on_left)), 'omitnan');
                        
                        div_wall_dist_left = median(...
                            wall_dist_clean(time_mask & on_left), 'omitnan');
                        
                        div_area_use_left = calculateAreaUse(...
                            x_clean(time_mask & on_left), ...
                            y_clean(time_mask & on_left)) / ...
                            div_total_time_left;
                    end
                    
                    if div_total_time_right < cfg.min_time
                        div_trans_vel_right = NaN;
                        div_ang_vel_right = NaN;
                        div_wall_dist_right = NaN;
                        div_area_use_right = NaN;  
                    else
                        div_trans_vel_right = median(...
                            trans_vel_clean(time_mask & on_right), 'omitnan');
                        
                        %div_trans_vel_right = max(...
                        %    trans_vel_clean(time_mask & on_right), [], 'omitnan');
                        
                        div_ang_vel_right = median(...
                            abs(ang_vel_clean(time_mask & on_right)), 'omitnan');
                        
                        div_wall_dist_right = median(...
                            wall_dist_clean(time_mask & on_right), 'omitnan');
                        
                        div_area_use_right = calculateAreaUse(...
                            x_clean(time_mask & on_right), ...
                            y_clean(time_mask & on_right)) / ...
                            div_total_time_right;
                    end
                    
                    averaged_trans_vel(1,window_idx) = div_trans_vel_left;
                    averaged_trans_vel(2,window_idx) = div_trans_vel_right;
                    
                    averaged_ang_vel(1,window_idx) = div_ang_vel_left;
                    averaged_ang_vel(2,window_idx) = div_ang_vel_right;
                    
                    averaged_total_time(1,window_idx) = div_total_time_left;
                    averaged_total_time(2,window_idx) = div_total_time_right;
                    
                    averaged_wall_dist(1,window_idx) = div_wall_dist_left;
                    averaged_wall_dist(2,window_idx) = div_wall_dist_right;
                    
                    averaged_area_use(1,window_idx) = div_area_use_left;
                    averaged_area_use(2,window_idx) = div_area_use_right;
                    
                    averaged_switch_rate(1,window_idx) = nnz(switch_state(time_mask)) / ...
                        sum(time_step(time_mask));
                    averaged_switch_rate(2,window_idx) = 0;
                    
                    p_on_left = div_total_time_left / ...
                        (div_total_time_left + div_total_time_right);
  
                    % avoid probabilities less than 1e-4 so log is not -Inf
                    p_on_left = max(p_on_left, 1e-4);
                    p_on_left = min(p_on_left, 1-1e-4);
                    
                    averaged_explor_index(1,window_idx) = ...
                        1 - abs(1 - 2*p_on_left);
                    averaged_explor_index(2,window_idx) = 0;
                    
                otherwise
                    error 'unrecognized subregion type'
                    
            end
        end
    end
    
    % transfer data into main struct array
    for subr_idx=1:num_subr
        kinematics(area_idx,subr_idx).trans_vel = averaged_trans_vel(subr_idx,:);
        kinematics(area_idx,subr_idx).ang_vel = averaged_ang_vel(subr_idx,:);
        kinematics(area_idx,subr_idx).total_time = averaged_total_time(subr_idx,:);
        kinematics(area_idx,subr_idx).wall_dist = averaged_wall_dist(subr_idx,:);
        kinematics(area_idx,subr_idx).area_use = averaged_area_use(subr_idx,:);
        kinematics(area_idx,subr_idx).switch_rate = averaged_switch_rate(subr_idx,:);
        kinematics(area_idx,subr_idx).explor_index = averaged_explor_index(subr_idx,:);
    end
end

start_time = min(time);

switch cfg.window_type
    case 'period'
        if ~isempty(cfg.window_period)
            time_base = start_time + ...
                ((0:num_windows-1) + 0.5)* cfg.window_period;
        else
            time_base = start_time;
        end
    case 'times'
        time_base = cfg.window_times(1:end, 1)' + ...
            0.5 * cfg.window_times(1:end,2)';
    otherwise
        error 'unrecognized window_type';
end

end

function area_use = calculateAreaUse(x, y)
% This function calculates the area that is contained by the
% convex hull of the passed points. It ignores NaN values
% in the data set.

not_nan_mask = ~isnan(x) & ~isnan(y);
x = x(not_nan_mask);
y = y(not_nan_mask);

if length(x) < 3
    area_use = NaN;
    return;
end

if max(x - mean(x)) * max(y - mean(y)) < 1e-8
    area_use = NaN;
    return;
end

k = convhull(x, y);
x_hull = x(k);
y_hull = y(k);

area_use = polyarea(x_hull, y_hull);

end

function switch_state = countSwitches(t, y, min_time)
% This function calculates if a switch between subregions occurred
% at a given time point. It accepts three arguments:
%
%   t - Nx1 array of time values (approx fixed interval)
%   y - Nx1 array of subregion id values
%   min_time - scalar value indicating minimum time in which
%       larva must be in a different subregion for a switch to
%       be registered (prevents rapid flipping at boundary)
%
% This function returns an Nx1 array that contains a 1 value if
% a switch occurred and zero otherwise.

if length(t) ~= length(y)
    error 'invalid input';
end

if length(t) <= 2
    switch_state = false(length(t), 1);
    return;
end

y_prev_state = y(1);
switch_state = zeros(length(t), 1);

% for efficiency we convert min_time into a fixed
% number of indices, this assumed that the time is
% roughly fixed interval
min_time_indices = ceil(1 + min_time / mean(diff(t)));

for idx=2:length(y)-min_time_indices+1
    if all(y(idx:idx+min_time_indices-1) ~= y_prev_state)        
        switch_state(idx) = 1;
        y_prev_state = y(idx);
    else
        switch_state(idx) = 0;
    end
end

end

