function lpg_getLarvaMovement(mov_file, masks_file, output)
%LPG_GETLARVA_MOVEMENT Calculate number of movements
%   This function reads in one ufmf file and then saves an output
%   file that contains the number of times the larva moved, and
%   the cross-correlation as a function of time.

% skip this many frames between calculating movement
frame_skipping = 25;

% ignore changes in pixel intensity less than this fraction
% of the background intensity
noise_scale = 0.1;

% set the intensity signal produced by a larva. This should be the
% sum of the difference between the background intensity and the
% larva intensity integrated over the body of the larva, normalized
% by the background intensity. This is not exact, just an estimate
% for identifying movement.
larva_total_signal = 40;

% set the threshold below which the larva is considered to
% have moved, given as fraction of total signal.
diff_threshold = 0.3;

% set the movement filter duration in seconds
boxcar_filt_period = 30;

% set the size of the background smoothing filter
smooth_filt_size = 30;

% check if output already exists
if exist(output, 'file')
    error 'output file already exists';
end

% read in movie headers
ufmf_header = ufmf_read_header(mov_file);
num_frames = ufmf_header.nframes;

% read in the masks file
load(masks_file, 'masks');

% find the image type
switch ufmf_header.dataclass
    case 'uint8'
        max_value = 255;
    otherwise
        error 'unrecognized data class';
end

% calculate the bounding box for each mask
mask_bounds = zeros(4,numel(masks));
mask_cropped = cell(1,numel(masks));

for mask_idx=1:numel(masks)
    mask_im = masks(mask_idx).im;
    
    bnds = lpg_getMaskBounds(masks(mask_idx).im);
    
    mask_cropped{mask_idx} = mask_im(...
        bnds(1):bnds(1)+bnds(2)-1, bnds(3):bnds(3)+bnds(4)-1);
    mask_bounds(:,mask_idx) = bnds;
end

% traverse frames for relative difference
prev_im = ufmf_read_frame(ufmf_header, 1);
prev_im = double(prev_im)/max_value;
prev_im = prev_im ./ imgaussfilt(prev_im, repmat(smooth_filt_size,1,2));

frames_to_proc = 2:frame_skipping:num_frames;
rel_diff = zeros(numel(masks), length(frames_to_proc),1);

for frame_idx=1:length(frames_to_proc)
    % read frame
    this_im = ufmf_read_frame(ufmf_header, frames_to_proc(frame_idx));
    this_im = double(this_im)/max_value;
    this_im = this_im ./ imgaussfilt(this_im, repmat(smooth_filt_size,1,2));

    % zero out small changes in intensity
    diff_im = abs(this_im - prev_im);
    diff_im(diff_im < noise_scale) = 0;
    
    % calculate relative difference for each mask
    for mask_idx=1:numel(masks)
        bnds = mask_bounds(:, mask_idx);
        cur_mask = mask_cropped{mask_idx};

        diff_cropped = diff_im(bnds(1):bnds(1)+bnds(2)-1, bnds(3):bnds(3)+bnds(4)-1);
        diff_cropped = diff_cropped(cur_mask);
        
        rel_diff(mask_idx, frame_idx) = sum(diff_cropped) / ...
            larva_total_signal;
    end
    
    prev_im = this_im;
end

% find places where relative difference is greater than threshold
is_moving = double(rel_diff > diff_threshold);

% apply boxcar filter to the movement
delta_t = mean(diff(ufmf_header.timestamps)) * frame_skipping;
filt_len = ceil(boxcar_filt_period / delta_t);

filt_a = 1.0;
filt_b = (1/filt_len)*ones(1,filt_len);

max_frac = 0.5;
filt_pad = min(4*filt_len, floor(max_frac * size(is_moving,2)));

filt_is_moving = zeros(size(is_moving,1), size(is_moving,2));

for mask_idx=1:numel(masks)
    padded_array = padarray(is_moving(mask_idx,:), [0,filt_pad], 'symmetric', 'both');
    padded_array = filtfilt(filt_b, filt_a, padded_array);
    filt_is_moving(mask_idx,:) = padded_array(filt_pad+1:end-filt_pad);
end

is_moving = filt_is_moving;
time = (0:size(is_moving,2)-1)*delta_t;

save(output, 'is_moving', 'time');

fclose(ufmf_header.fid);

end


