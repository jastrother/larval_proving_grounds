function cams = lpg_PtGreyStopCameras(cams)
% LPG_PTGREYSTOPCAMERAS Stop the cameras recording
%   This function stops cameras that were started by calling the function
%   lpg_PtGreyStartCameras().
%
%   This function accepts a single argument 'cams', which is a structure
%   produced by the function lpg_PtGreyStartCameras. After this function
%   has been invoked, the 'ptgrey_context' in 'cams' is invalid. But the
%   'cfg' and 'preview_figure' fields of 'cams' are unaltered.

lpg_ptgrey_common('stop_capture', cams.ptgrey_context);
lpg_ptgrey_common('destroy_context', cams.ptgrey_context);

end